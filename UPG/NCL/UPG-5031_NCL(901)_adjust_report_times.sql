-- SELECT id FROM reports.ScheduledReportRuntime WHERE scheduled_report_id = (SELECT id FROM reports.ScheduledReport WHERE partner_id = 901 AND schedule_name = 'NCL - Daily Ticketing Queue Offer Detail Report');
-- Returned:
--	2637
--	2642
START TRANSACTION;
UPDATE reports.ScheduledReportRuntime SET next_runtime = next_runtime + INTERVAL 1 HOUR WHERE id = 2637;
UPDATE reports.ScheduledReportRuntime SET next_runtime = next_runtime + INTERVAL 1 HOUR WHERE id = 2642;
COMMIT;