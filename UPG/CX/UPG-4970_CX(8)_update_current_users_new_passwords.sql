
-- SELECT * FROM users.Users WHERE email IN ('CPPSWTB@cathaypacific.com',
-- 'CPPSWB@cathaypacific.com',
-- 'PG.CPPPB@cathaypacific.com',
-- 'CPPPB@cathaypacific.com',
-- 'CPPNG@cathaypacific.com',
-- 'CPPNGIO@cathaypacific.com',
-- 'CPPSVK@cathaypacific.com',
-- 'PG.CPPSVK@cathaypacific.com',
-- 'CPPSB@cathaypacific.com',
-- 'PG.CPPSB@cathaypacific.com',
-- 'CPPNB@cathaypacific.com',
-- 'PG.CPPNB@cathaypacific.com',
-- 'CPPSHNA@cathaypacific.com',
-- 'PG.CPPSHNA@cathaypacific.com',
-- 'CPPPN@cathaypacific.com',
-- 'PG.CPPPN@cathaypacific.com',
-- 'CPPZP@cathaypacific.com',
-- 'PG.CPPZP@cathaypacific.com',
-- 'CPPSR@cathaypacific.com',
-- 'PG.CPPSR@cathaypacific.com',
-- 'CPPAMS@cathaypacific.com',
-- 'PG.CPPAMS@cathaypacific.com',
-- 'CPPMT@cathaypacific.com',
-- 'PG.CPPMT@cathaypacific.com',
-- 'CPPTW@cathaypacific.com',
-- 'PG.CPPTW@cathaypacific.com',
-- 'PG.CPPSARR@cathaypacific.com',
-- 'PG.CPPAJAM@cathaypacific.com',
-- 'PG.CPPRHKY@cathaypacific.com',
-- 'PG.CPPRERR@cathaypacific.com');


START TRANSACTION;

-- Activate all users (old)
-- REPLACE INTO `users`.`Users`  (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (17970, 'Rohan', 'Sadarjoshi', 'CPPSARR@cathaypacific.com', NULL,NULL,NULL, '9b9574ee31670d84f053313db68d28e79edc7adf937ea9edea6ddb8f7dbdc7ad', '7', '8', NULL, '11', 'ACTIVE', '1');
-- REPLACE INTO `users`.`Users`  (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (17971, 'Ajay', 'Mundekar', 'CPPAJAM@cathaypacific.com', NULL,NULL,NULL, '470f5f3374d25ad71ffce66abeda41d130dfa7c3d4d2bc1bc25b666a06129c65', '7', '8', NULL, '11', 'ACTIVE', '1');
-- REPLACE INTO `users`.`Users`  (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (17972, 'Ralph', 'Kinny', 'CPPRHKY@cathaypacific.com', NULL,NULL,NULL, 'bd6f248100138f96bec1d701ce448aae6a51ef6e1a82fdcb490170e53b3cda1e', '7', '8', NULL, '11', 'ACTIVE', '1');
-- REPLACE INTO `users`.`Users`  (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (17973, 'Reena', 'Rakshe', 'CPPRERR@cathaypacific.com', NULL,NULL,NULL, '79a0d6f84d7a77763c38774149c0c75e0856a04fcac5156ddb0d66f23034b4d0', '7', '8', NULL, '11', 'ACTIVE', '1');
-- REPLACE INTO `users`.`Users`  (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (1310,'Ameya','Saraf','CPPAMS@cathaypacific.com',NULL,NULL,NULL,'26124d584438882048f3637b82927faf6f138ad9fd446c0f4f904e7806a9e56d',7,8,NULL,11,'ACTIVE',1);
-- REPLACE INTO `users`.`Users`  (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (1313,'Monica','Teli','CPPMT@cathaypacific.com',NULL,NULL,NULL,'3cc7e5a9a4af92c11160419b91febb457baa579e12e8476230828589c69b843c',7,8,NULL,11,'ACTIVE',1);
-- REPLACE INTO `users`.`Users`  (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (7124,'Niteen','Bhosale','CPPNB@cathaypacific.com',NULL,NULL,NULL,'97c358a1e25fdfcf852cb0c824ade8ebacf63135b92226b5b4530b187c50e93b',7,8,NULL,11,'ACTIVE',1);
-- REPLACE INTO `users`.`Users`  (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (1314,'Nikita','Godambe','CPPNG@cathaypacific.com',NULL,NULL,NULL,'7c8a73406da403208a92af0dbb1e2d570764f1e47da85e20b52bd39ee1bf016d',7,8,NULL,11,'ACTIVE',1);
-- REPLACE INTO `users`.`Users`  (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (1315,'Pradnya','Bhavar','CPPPB@cathaypacific.com',NULL,NULL,NULL,'ab7035eb07c0509d91f86c6cce5569c3a21c177b2dce452c249896dc63d6b51d',7,8,NULL,11,'ACTIVE',1);
-- REPLACE INTO `users`.`Users`  (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (1316,'Priyanka','Nijap','CPPPN@cathaypacific.com',NULL,NULL,NULL,'0ddf77af58018574a2485f52cb6dd35b7c4c97eddb3ef605ad3fbbe0cc340c7e',7,8,NULL,11,'ACTIVE',1);
-- REPLACE INTO `users`.`Users`  (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (7123,'Sagar','Bhoir','CPPSB@cathaypacific.com',NULL,NULL,NULL,'9177df10939217e81e47c67c95d44e405f74d8ca330698f53f446986d6817ee7',7,8,NULL,11,'ACTIVE',1);
-- REPLACE INTO `users`.`Users`  (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (1317,'Shrunal','Nangare','CPPSHNA@cathaypacific.com',NULL,NULL,NULL,'5308d9b86e428345d685f9161db1ed5cd4df2c026166fecbe9c672641b1d9ece',7,8,NULL,11,'ACTIVE',1);
-- REPLACE INTO `users`.`Users`  (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (1319,'Shagufta','Retiwala','CPPSR@cathaypacific.com',NULL,NULL,NULL,'1a16d944f12fb48304911b4b73e840c035ba519f2405782bb8b80c5a76c2475c',7,8,NULL,11,'ACTIVE',1);
-- REPLACE INTO `users`.`Users`  (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (1320,'Sanket','Kasle','CPPSVK@cathaypacific.com',NULL,NULL,NULL,'357fc6cdfeb099d5db23fe580326018d9e7efa82ca9300fcc006c402d0df888a',7,8,NULL,11,'ACTIVE',1);
-- REPLACE INTO `users`.`Users`  (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (1321,'Sweetan','Bhandu','CPPSWB@cathaypacific.com',NULL,NULL,NULL,'d787d5bd8ac6e86b5b52da7da819a90c6842a24cddbc97e8fda03f7ca31d34ec',7,8,NULL,11,'ACTIVE',1);
-- REPLACE INTO `users`.`Users`  (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (1322,'Tejashree','Warghade','CPPTW@cathaypacific.com',NULL,NULL,NULL,'07ef50d7a3ecfa4923144614ecfed9b67a6195170d616290180ca2ec068e82a6',7,8,NULL,11,'ACTIVE',1);
-- REPLACE INTO `users`.`Users`  (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (1323,'Zaheer','Patel','CPPZP@cathaypacific.com',NULL,NULL,NULL,'b8291ffa6984ed7224372d6fcd08ad64c39103d83beb304b7d985b2493f5b98e',7,8,NULL,11,'ACTIVE',1);

-- Set new passwords (new users)
REPLACE INTO `users`.`Users` (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (17461,'Ajay','Mundekar','PG.CPPAJAM@cathaypacific.com',NULL,NULL,NULL,'470f5f3374d25ad71ffce66abeda41d130dfa7c3d4d2bc1bc25b666a06129c65',7,8,NULL,11,'ACTIVE',1);
REPLACE INTO `users`.`Users` (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (17463,'Reena','Rakshe','PG.CPPRERR@cathaypacific.com',NULL,NULL,NULL,'79a0d6f84d7a77763c38774149c0c75e0856a04fcac5156ddb0d66f23034b4d0',7,8,NULL,11,'ACTIVE',1);
REPLACE INTO `users`.`Users` (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (17462,'Kinny','Ralph','PG.CPPRHKY@cathaypacific.com',NULL,NULL,NULL,'bd6f248100138f96bec1d701ce448aae6a51ef6e1a82fdcb490170e53b3cda1e',7,8,NULL,11,'ACTIVE',1);
REPLACE INTO `users`.`Users` (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (17460,'Rohan','Sadarjoshi','PG.CPPSARR@cathaypacific.com',NULL,NULL,NULL,'9b9574ee31670d84f053313db68d28e79edc7adf937ea9edea6ddb8f7dbdc7ad',7,8,NULL,11,'ACTIVE',1);
REPLACE INTO `users`.`Users` (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (17497,'Ameya','Saraf','PG.CPPAMS@cathaypacific.com',NULL,NULL,NULL,'b9fbe86a162a5fc482c2c5d3b5b5a22e761f4c319e78dfba1d3631d999268f81',7,8,NULL,11,'ACTIVE',1);
REPLACE INTO `users`.`Users` (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (17498,'Monica','Teli','PG.CPPMT@cathaypacific.com',NULL,NULL,NULL,'bc2aaad12603759c3ad7faf9d2f12f00e3eaadc2cb7dcef8155869bec30fce09',7,8,NULL,11,'ACTIVE',1);
REPLACE INTO `users`.`Users` (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (17459,'Bhosale','Niteen','PG.CPPNB@cathaypacific.com',NULL,NULL,NULL,'97c358a1e25fdfcf852cb0c824ade8ebacf63135b92226b5b4530b187c50e93b',7,8,NULL,11,'ACTIVE',1);
REPLACE INTO `users`.`Users` (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (17488,'Pradnya','Bhavar','PG.CPPPB@cathaypacific.com',NULL,NULL,NULL,'d25a124e51c433c2c070ec501016a72fa2fd4445afb916acbb23bda7f1d3126c',7,8,NULL,11,'ACTIVE',1);
REPLACE INTO `users`.`Users` (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (17494,'Priyanka','Nijap','PG.CPPPN@cathaypacific.com',NULL,NULL,NULL,'b02612f3280ea9230d3b6717a923b104b350b96e5b9ec0c83bfb2f6415079fa2',7,8,NULL,11,'ACTIVE',1);
REPLACE INTO `users`.`Users` (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (17458,'Sagar','Bhoir','PG.CPPSB@cathaypacific.com',NULL,NULL,NULL,'9177df10939217e81e47c67c95d44e405f74d8ca330698f53f446986d6817ee7',7,8,NULL,11,'ACTIVE',1);
REPLACE INTO `users`.`Users` (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (17493,'Shrunal','Nangare','PG.CPPSHNA@cathaypacific.com',NULL,NULL,NULL,'7a47a5b753b5da69dcc55e9563256b148968140b1ee9aaa55fa120a6831f84a5',7,8,NULL,11,'ACTIVE',1);
REPLACE INTO `users`.`Users` (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (17496,'Shagufta','Retiwala','PG.CPPSR@cathaypacific.com',NULL,NULL,NULL,'8908031e8c77a46c18a035f3a495fe47aef39475fa44a1ddae4f430efad11911',7,8,NULL,11,'ACTIVE',1);
REPLACE INTO `users`.`Users` (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (17490,'Sanket','Kasle','PG.CPPSVK@cathaypacific.com',NULL,NULL,NULL,'f7406123facdf583615c8fc0d712dbcfa66de4421f7f89f59313876dd46d97cb',7,8,NULL,11,'ACTIVE',1);
REPLACE INTO `users`.`Users` (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (17499,'Tejashree','Warghade','PG.CPPTW@cathaypacific.com',NULL,NULL,NULL,'0ab4a08f4b292f951ca06f6082b28972ca1fe6c0e3a15b62167ea6e8958b2108',7,8,NULL,11,'ACTIVE',1);
REPLACE INTO `users`.`Users` (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (17495,'Zaheer','Patel','PG.CPPZP@cathaypacific.com',NULL,NULL,NULL,'d1d03f3ab958c7257764e6f003443d4ad15b40c16ba5caff5cf07e42b9782ae7',7,8,NULL,11,'ACTIVE',1);
REPLACE INTO `users`.`Users` (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (17489,'Nikita','Godambe','CPPNGIO@cathaypacific.com',NULL,NULL,NULL,'7ef9e0679c4228ccde1a0a2022a5d2051f4f6d17afb50b1161e7506136d75366',7,8,NULL,11,'ACTIVE',1);
REPLACE INTO `users`.`Users` (`id`,`firstname`,`lastname`,`email`,`lang`,`airport`,`currency`,`password`,`role_id`,`partner_id`,`employee_id`,`security_img_id`,`account_status`,`subprocessors_notification`) VALUES (17487,'Sweetan','Bhandu','CPPSWTB@cathaypacific.com',NULL,NULL,NULL,'89ead0d1573797bf11f4e5d84e4854e9cd81ef87884e4a70e4ef33298f6c1f2a',7,8,NULL,11,'ACTIVE',1);

COMMIT;
