START TRANSACTION;

INSERT INTO `offers`.`HoldSettings` (`partner_id`, `origin`, `dest`, `product_id`, `enabled`, `upgrade_type`, `auto_hold_threshold`, `auto_hold_ranklist_selected`) VALUES
( '424', 'KBP', '*', '0', '1', 'BUSINESS', '0', '0'),
( '424', 'KBP', '*', '0', '1', 'PREMIUM_ECONOMY', '0', '0');

COMMIT;