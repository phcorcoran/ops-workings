START TRANSACTION;

-- Set Holdback to 0 for GEO-JFK
INSERT INTO offers.RuleData (partner_id, rule_type, value, value_type) VALUES (218, 'PRODUCT_ALLOCATION', 0, 'REMAINDER');
SET @ruleId = (SELECT LAST_INSERT_ID());
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'ORIGIN', 'GEO');
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'DEST', 'JFK');

-- Set Holdback to 0 for GEO-MIA
INSERT INTO offers.RuleData (partner_id, rule_type, value, value_type) VALUES (218, 'PRODUCT_ALLOCATION', 0, 'REMAINDER');
SET @ruleId = (SELECT LAST_INSERT_ID());
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'ORIGIN', 'GEO');
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'DEST', 'MIA');

-- Set Holdback to 0 for GEO-POS
INSERT INTO offers.RuleData (partner_id, rule_type, value, value_type) VALUES (218, 'PRODUCT_ALLOCATION', 0, 'REMAINDER');
SET @ruleId = (SELECT LAST_INSERT_ID());
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'ORIGIN', 'GEO');
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'DEST', 'POS');

-- Set Holdback to 0 for GND-JFK
INSERT INTO offers.RuleData (partner_id, rule_type, value, value_type) VALUES (218, 'PRODUCT_ALLOCATION', 0, 'REMAINDER');
SET @ruleId = (SELECT LAST_INSERT_ID());
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'ORIGIN', 'GND');
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'DEST', 'JFK');

-- Set Holdback to 0 for JFK-GEO
INSERT INTO offers.RuleData (partner_id, rule_type, value, value_type) VALUES (218, 'PRODUCT_ALLOCATION', 0, 'REMAINDER');
SET @ruleId = (SELECT LAST_INSERT_ID());
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'ORIGIN', 'JFK');
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'DEST', 'GEO');

-- Set Holdback to 0 for JFK-GND
INSERT INTO offers.RuleData (partner_id, rule_type, value, value_type) VALUES (218, 'PRODUCT_ALLOCATION', 0, 'REMAINDER');
SET @ruleId = (SELECT LAST_INSERT_ID());
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'ORIGIN', 'JFK');
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'DEST', 'GND');

-- Set Holdback to 0 for JFK-POS
INSERT INTO offers.RuleData (partner_id, rule_type, value, value_type) VALUES (218, 'PRODUCT_ALLOCATION', 0, 'REMAINDER');
SET @ruleId = (SELECT LAST_INSERT_ID());
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'ORIGIN', 'JFK');
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'DEST', 'POS');

-- Set Holdback to 0 for JFK-SVD
INSERT INTO offers.RuleData (partner_id, rule_type, value, value_type) VALUES (218, 'PRODUCT_ALLOCATION', 0, 'REMAINDER');
SET @ruleId = (SELECT LAST_INSERT_ID());
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'ORIGIN', 'JFK');
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'DEST', 'SVD');

-- Set Holdback to 0 for MCO-POS
INSERT INTO offers.RuleData (partner_id, rule_type, value, value_type) VALUES (218, 'PRODUCT_ALLOCATION', 0, 'REMAINDER');
SET @ruleId = (SELECT LAST_INSERT_ID());
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'ORIGIN', 'MCO');
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'DEST', 'POS');

-- Set Holdback to 0 for MIA-GEO
INSERT INTO offers.RuleData (partner_id, rule_type, value, value_type) VALUES (218, 'PRODUCT_ALLOCATION', 0, 'REMAINDER');
SET @ruleId = (SELECT LAST_INSERT_ID());
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'ORIGIN', 'MIA');
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'DEST', 'GEO');

-- Set Holdback to 0 for MIA-POS
INSERT INTO offers.RuleData (partner_id, rule_type, value, value_type) VALUES (218, 'PRODUCT_ALLOCATION', 0, 'REMAINDER');
SET @ruleId = (SELECT LAST_INSERT_ID());
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'ORIGIN', 'MIA');
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'DEST', 'POS');

-- Set Holdback to 0 for POS-GEO
INSERT INTO offers.RuleData (partner_id, rule_type, value, value_type) VALUES (218, 'PRODUCT_ALLOCATION', 0, 'REMAINDER');
SET @ruleId = (SELECT LAST_INSERT_ID());
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'ORIGIN', 'POS');
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'DEST', 'GEO');

-- Set Holdback to 0 for POS-JFK
INSERT INTO offers.RuleData (partner_id, rule_type, value, value_type) VALUES (218, 'PRODUCT_ALLOCATION', 0, 'REMAINDER');
SET @ruleId = (SELECT LAST_INSERT_ID());
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'ORIGIN', 'POS');
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'DEST', 'JFK');

-- Set Holdback to 0 for POS-MCO
INSERT INTO offers.RuleData (partner_id, rule_type, value, value_type) VALUES (218, 'PRODUCT_ALLOCATION', 0, 'REMAINDER');
SET @ruleId = (SELECT LAST_INSERT_ID());
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'ORIGIN', 'POS');
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'DEST', 'MCO');

-- Set Holdback to 0 for POS-MIA
INSERT INTO offers.RuleData (partner_id, rule_type, value, value_type) VALUES (218, 'PRODUCT_ALLOCATION', 0, 'REMAINDER');
SET @ruleId = (SELECT LAST_INSERT_ID());
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'ORIGIN', 'POS');
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'DEST', 'MIA');

-- Set Holdback to 0 for POS-SVD
INSERT INTO offers.RuleData (partner_id, rule_type, value, value_type) VALUES (218, 'PRODUCT_ALLOCATION', 0, 'REMAINDER');
SET @ruleId = (SELECT LAST_INSERT_ID());
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'ORIGIN', 'POS');
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'DEST', 'SVD');

-- Set Holdback to 0 for SVD-JFK
INSERT INTO offers.RuleData (partner_id, rule_type, value, value_type) VALUES (218, 'PRODUCT_ALLOCATION', 0, 'REMAINDER');
SET @ruleId = (SELECT LAST_INSERT_ID());
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'ORIGIN', 'SVD');
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'DEST', 'JFK');

-- Set Holdback to 0 for SVD-POS
INSERT INTO offers.RuleData (partner_id, rule_type, value, value_type) VALUES (218, 'PRODUCT_ALLOCATION', 0, 'REMAINDER');
SET @ruleId = (SELECT LAST_INSERT_ID());
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'ORIGIN', 'SVD');
INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'DEST', 'POS');
COMMIT;
