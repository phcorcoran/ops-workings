#!/usr/bin/env python3

import csv

r = csv.reader(open('UPG-5145.csv'))

print('START TRANSACTION;')
for row in r:
	print("\n-- Set Holdback to 0 for %s-%s" % (row[0], row[1]))
	print("INSERT INTO offers.RuleData (partner_id, rule_type, value, value_type) VALUES (218, 'PRODUCT_ALLOCATION', 0, 'REMAINDER');")
	print("SET @ruleId = (SELECT LAST_INSERT_ID());")
	print("INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'ORIGIN', %s);" % repr(row[0]))
	print("INSERT INTO offers.RuleAttribute (rule_id, attr_type, attr_value) VALUES (@ruleId, 'DEST', %s);" % repr(row[1]))

print('COMMIT;')


