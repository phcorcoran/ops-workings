#!/usr/bin/env python3

import csv
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--db', '-d', help='Database to use')
parser.add_argument('--table', '-t', help='Table to use')
parser.add_argument('--input', '-i', help='File to use as input (CSV, first row SQL fields)')
parser.add_argument('--output', '-o', help='File to output to')
args = parser.parse_args()

db = args.db
table = args.table
input_file = args.input
output_file = args.output

if not args.db:
	db = input('Enter database to use? ').strip()
if not args.table:
	table = input('Enter table to use? ').strip()
if not args.input:
	input_file = input('Enter file to use (CSV, first row SQL fields)? ').strip()
if not args.output:
	output_file = input('Enter file to output to? ').strip()

r = csv.reader(open(input_file))
headers = None
out = []
for row in r:
	if not headers:
		headers = row
		continue
	out.append("(%s)" % ', '.join([repr(x) for x in row]))

f = open(output_file, 'w')
f.write("START TRANSACTION;\n")
f.write("REPLACE INTO `%s`.`%s` (%s) VALUES \n" % (db, table, ', '.join(headers)))
f.write(",\n".join(out) + ";\n")
f.write("COMMIT;\n")
f.close()
