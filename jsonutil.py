#!/usr/bin/env python3
import os, sys

#pragma mark Version Handling

FLAG_VERSION = '0.1.2'

def register_version_args(parser):
	parser.add_argument('--version', help='Output script version', action='store_true')
	
def handle_version_args(args):
	global FLAG_VERSION
	try:
		if args.version:
			print('%s Ver' % os.path.basename(__file__), FLAG_VERSION)
			exit()
	except AttributeError:
		pass

#pragma mark -

#pragma mark Import Handling

from pprint import pprint
import argparse
import datetime
from random import randint
import json
from io import StringIO
from getpass import getpass
import curses

#pragma mark -

#pragma mark Verbose Handling

FLAG_VERBOSE = 2

def verbose(*args, level = 'INFO'):
	global FLAG_VERBOSE
	levels = [
		['FATAL', '\033[31m'],
		['ERROR', '\033[31m'],
		['WARNING', '\033[33m'],
		['INFO', '\033[32m'],
		['DEBUG', '\033[36m'],
		['TRACE', '\033[90m'],
	]
	color_end = '\033[0m'
	for i in range(FLAG_VERBOSE+1):
		if levels[i][0] == level:
			print("[%s%s%s]" % (levels[i][1], level, color_end), *args)

def debug(*args):
	verbose(*args, level='DEBUG')

def register_verbose_args(parser):
	group = parser.add_argument_group('Verbose')
	group.add_argument('--verbose', '-v', help='Output more information (-vv debug, -vvv trace)', action='count', default=0)
	group.add_argument('--quiet', '-q', help='Suppress warnings (-qq errors, -qqq fatal)', action='count', default=0)

def trace_calls(frame, event, arg):
	if event != 'call' or frame.f_code.co_name == 'write': return
	try:
		print('[%sTRACE%s] Called `%s` (line %s of `%s`) from line %s of `%s`' %  ('\033[90m', '\033[0m', frame.f_code.co_name, frame.f_lineno, frame.f_code.co_filename, frame.f_back.f_lineno, frame.f_back.f_code.co_filename))
	except AttributeError:
		pass
	return

def handle_verbose_args(args):
	global FLAG_VERBOSE
	try:
		FLAG_VERBOSE += args.verbose - args.quiet
	except AttributeError:
		pass
	if FLAG_VERBOSE >= 5:
		try:
			sys.settrace(trace_calls)
		except NameError:
			pass

#pragma mark -

#pragma mark Environment Handling

FLAG_ENVIRONMENT = None

def register_environment_args(parser, include_prod_readonly = False, include_prod_confirm = True):
	verbose('Environment: Setting up argument group', level='DEBUG')
	group = parser.add_argument_group('Environment')
	verbose('Environment: Adding STAGE argument', level='DEBUG')
	group.add_argument('--stage', help='Select STAGE database', action='store_true')
	verbose('Environment: Adding PRODUCTION argument', level='DEBUG')
	group.add_argument('--prod', help='Select PRODUCTION database', action='store_true')
	if include_prod_readonly:
		verbose('Environment: Adding PRODUCTION READ-ONLY argument', level='DEBUG')
		parser.add_argument('--prod-readonly', help='Select PRODUCTION READ-ONLY database', action='store_true')
	if include_prod_confirm:
		verbose('Environment: Adding skip-prod-confirm argument', level='DEBUG')
		parser.add_argument('--skip-prod-confirm', help='Disable PRODUCTION confirmation prompt', action='store_true')
	
def handle_environment_args(args, default = None):
	global FLAG_ENVIRONMENT
	include_prod_readonly = True
	include_prod_confirm = True
	
	try: 
		if args.skip_prod_confirm: include_prod_confirm = False
	except AttributeError: include_prod_confirm = False
	
	verbose('Environment: include_prod_confirm set to %s' % str(include_prod_confirm), level='DEBUG')
	
	env_select = None
	
	if default == 'STAGE':
		env_select = 1
	elif default == 'PRODUCTION':
		env_select = 2
	elif default == 'PRODUCTION READ-ONLY':
		env_select = 3
	
	try:
		if args.prod: env_select = 2
	except AttributeError: pass
	
	try:
		if args.prod_readonly: env_select = 3
	except AttributeError: include_prod_readonly = False
	
	try:
		if args.stage: env_select = 1
	except AttributeError: pass
	
	verbose('Environment: env_select set to %s' % str(env_select), level='DEBUG')
	
	if not env_select:
		if include_prod_readonly:
			print("1. STAGE\n2. PRODUCTION\n3. PRODUCTION READ-ONLY")
		else:
			print("1. STAGE\n2. PRODUCTION")
	
		max_option = 2
		if include_prod_readonly: max_option = 3
	
		while not env_select:
			try:
				env_select = int(input("Select the desired environment? "))
				if env_select < 1 or env_select > max_option:
					env_select = None
					raise ValueError
			except ValueError:
				print("Invalid selection")
	
	verbose('Environment: env_select set to %s' % str(env_select), level='DEBUG')
	
	if env_select == 1:
		FLAG_ENVIRONMENT = 'STAGE'
	elif env_select == 2:
		FLAG_ENVIRONMENT = 'PRODUCTION'
		if include_prod_confirm:
			print("===== CONFIRM CHANGES TO PRODUCTION =====")
			x = input("Confirm (y/n)? ").strip()
			if x != 'y':
				exit()
	elif env_select == 3:
		FLAG_ENVIRONMENT = 'PRODUCTION READ-ONLY'
	
	verbose('Environment: FLAG_ENVIRONMENT set to %s' % str(FLAG_ENVIRONMENT), level='DEBUG')

#pragma mark -

#pragma mark Global Variables

RULEDATA_VALID_FIELDS = ['rule_type', 'hours_before', 'value', 'value_type', 'min', 'max', 'attr_type']
RULEATTRIBUTE_VALID_TYPES = ['ORIGIN', 'DEST', 'FROM_CABIN', 'UPGRADE_TYPE', 'HOURS_BEFORE', 'EQUIPMENT', 'DAY_OF_WEEK']

#pragma mark -

#pragma mark -

#pragma mark DatabaseController

def register_database_args(parser, include_superuser = False):
	group = parser.add_argument_group('Database')
	group.add_argument('--db-user', help='Database user')
	group.add_argument('--db-password', help='Database password')
	group.add_argument('--db-email', help='Database user email')
	group.add_argument('--db-capture-credentials', help='Force capture of database credentials for encrypted local storage', action='store_true')
	if include_superuser:
		parser.add_argument('--db-superuser', help='Use superuser credentials', action='store_true')

def handle_database_args(args):
	DatabaseController.override_user = args.db_user
	DatabaseController.override_password = args.db_password
	DatabaseController.override_email = args.db_email
	DatabaseController.override_capture_credentials = args.db_capture_credentials
	try:
		DatabaseController.superuser = args.db_superuser
	except AttributeError:
		pass

class DatabaseController:
	version = '0.3.0'
	override_user = ''
	override_password = ''
	override_email = ''
	override_capture_credentials = False
	superuser = False
	
	def __init__(self):
		try:
			import mysql.connector
		except ModuleNotFoundError as e:
			verbose(e, level='ERROR')
			verbose('Run `brew ls --versions mysql && brew upgrade mysql || brew install mysql`', level='ERROR')
			verbose('Run `brew cask install mysql-connector-python`', level='ERROR')
			verbose('Run `sudo pip3 install mysql-connector-python`', level='ERROR')
			exit()
		
		self.mysql_connector = mysql.connector
		
		self.db = None
		self.dry_run = False
		self.override_user = DatabaseController.override_user
		self.override_password = DatabaseController.override_password
		self.override_email = DatabaseController.override_email
		self.override_credentials_capture = DatabaseController.override_capture_credentials
		if not self.override_credentials_capture:
			if self.override_user or self.override_password:
				if not self.override_user: self.override_user = 'plusgrade'
				self.credentials = self.minimal_credentials()
				return
		self.permissions = {}

		self.load_gpg_credentials()
	
	def load_gpg_credentials(self):
		try:
			import gnupg
		except ModuleNotFoundError as e:
			verbose('DB credentials can be speficied as command line arguments (--db-user & --db-password), or can be stored locally using GnuPG encryption', level='ERROR')
			verbose(e, level='ERROR')
			verbose('For the latter, follow these instructions:', level='ERROR')
			verbose('Run `brew install gnupg`', level='ERROR')
			verbose('Run `sudo pip3 install python-gnupg`', level='ERROR')
			verbose('Adjust the next command with your own shell profile location (could be .bash_profile, .zshrc, .bashrc, etc)', level='ERROR')
			verbose('Run `echo -e "\nGPG_TTY=\$(tty);\nexport GPG_TTY;\ngpgconf --launch gpg-agent;\necho UPDATESTARTUPTTY | gpg-connect-agent &> /dev/null;\n" >> ~/.profile`')
			verbose('Run `gpg --full-generate-key`', level='ERROR')
			exit()
		
		self.gpgpath = os.path.expanduser("~/.gnupg/")
		verbose('DatabaseController -- GPG path is set to %s' % self.gpgpath, level='DEBUG')
		if DatabaseController.superuser:
			self.credentials_path = os.path.expanduser("~/.gnupg/credentials-superuser.json.gpg")
		else:
			self.credentials_path = os.path.expanduser("~/.gnupg/credentials.json.gpg")
		verbose('DatabaseController -- GPG credentials path is set to %s' % self.credentials_path, level='DEBUG')
		self.update_start_tty()
		self.gpg = gnupg.GPG(gnupghome=self.gpgpath)
		self.gpg.encoding = 'utf-8'
		self.retrieve_credentials()
	
	def retrieve_email(self):
		if self.override_email:
			return self.override_email
		email = ''
		try:
			gpg_identity = self.gpg.list_keys()[0]['uids'][0]
			email = gpg_identity.split('@')[0].split('<')[-1]
		except (IndexError, KeyError) as e:
			pass
		return email
	
	def update_start_tty(self):
		from subprocess import run, DEVNULL
		run(['gpg-connect-agent', 'UPDATESTARTUPTTY', '/bye'], stdout=DEVNULL, stderr=DEVNULL)
	
	def minimal_credentials(self):
		return {
			'STAGE': {
				'host': 'db.stage.plusgrade.com',
				'port': '3306',
				'user': self.override_user,
				'passwd': self.override_password
			},
			'PRODUCTION READ-ONLY': {
				'host': 'db-rr.plusgrade.com',
				'port': '3306',
				'user': self.override_user,
				'passwd': self.override_password
			},
			'PRODUCTION': {
				'host': 'db.plusgrade.com',
				'port': '3306',
				'user': self.override_user,
				'passwd': self.override_password
			}
		}
			
	def generate_db_credentials(self):
		from getpass import getpass
		verbose('DB credentials can be speficied as command line arguments (--db-user & --db-password), or can be stored locally using GnuPG encryption', level='WARNING')
		user = input('Enter DB username (e.g. john.D) ? ')
		stage_passwd = getpass(prompt='Enter stage password? ')
		prod_passwd = getpass(prompt='Enter prod password? ')
		
		credentials = self.minimal_credentials()
		credentials['STAGE']['user'] = user
		credentials['PRODUCTION READ-ONLY']['user'] = user
		credentials['PRODUCTION']['user'] = user
		credentials['STAGE']['passwd'] = stage_passwd
		credentials['PRODUCTION READ-ONLY']['passwd'] = prod_passwd
		credentials['PRODUCTION']['passwd'] = prod_passwd
		
		verbose('DatabaseController -- Creating credentials file')
		recipient = self.gpg.list_keys()[0]['keyid']
		verbose('DatabaseController -- GPG Recipient: %s' % recipient, level='DEBUG')
		s = self.gpg.encrypt(json.dumps(credentials), [recipient])
		verbose('DatabaseController -- GPG Response: %s' % s, level='DEBUG')
		if s.ok:
			with open(self.credentials_path, 'w') as f:
				f.write(str(s))
		else:
			verbose('DatabaseController -- GPG Status: %s' % s.status, level='ERROR')
			verbose('DatabaseController -- GPG Error: %s' % s.stderr, level='ERROR')
			exit()
	
	def retrieve_credentials(self):
		if not os.path.exists(self.credentials_path):
			verbose('DatabaseController -- Credentials file not found.', level='DEBUG')
			self.generate_db_credentials()
		if self.override_credentials_capture:
			verbose('DatabaseController -- Forcing capture of credentials', level='DEBUG')
			self.override_credentials_capture = False
			self.generate_db_credentials()
		with open(self.credentials_path, 'rb') as f:
			s = self.gpg.decrypt_file(f)
		if s.ok:
			verbose('Credentials file loaded correctly.', level='DEBUG')
			self.credentials = json.loads(str(s))
		else:
			verbose('DatabaseController -- GPG Status: %s' % s.status)
			verbose('DatabaseController -- GPG Error: %s' % s.stderr)
			verbose('DatabaseController -- Could not retrieve DB credentials', level='WARNING')
			self.generate_db_credentials()
			return self.retrieve_credentials()
			
			
	
	def enable_dry_run(self):
		self.dry_run = True
	def disable_dry_run(self):
		self.dry_run = False

	def select_stage(self):
		if self.db: self.close()
		self.load_db("STAGE")
	def select_prod_readonly(self):
		if self.db: self.close()
		self.load_db("PRODUCTION READ-ONLY")
	def select_prod(self):
		if self.db: self.close()
		self.load_db("PRODUCTION")
	
	def load_db(self, name):
		if self.dry_run:
			self.db = self.credentials[name]
			self.permissions = {'*': {'*': 'ALL PRIVILEGES'}}
			return
		verbose('DatabaseController -- Opening MySQL connection', level='DEBUG')
		self.db = self.mysql_connector.connect(
			host=self.credentials[name]['host'],
			port=self.credentials[name]['port'],
			user=self.credentials[name]['user'],
			passwd=self.credentials[name]['passwd']
		)
		self.retrieve_all_permissions()
	
	def cursor(self):
		if not self.db:
			verbose("DatabaseController -- No database selected.", level='ERROR')
			return
		if self.dry_run:
			return None
		return self.db.cursor()
	
	def commit(self):
		if not self.db: return
		self.db.commit()
	
	def close(self):
		if not self.db: return
		self.db.close()
		self.db = None
	
	def retrieve_all_permissions(self):
		if not self.db:
			verbose("DatabaseController -- No database selected.", level='ERROR')
			return
		grant_query = 'SHOW GRANTS;'
		response = self.execute(grant_query)[0]['result']
		verbose('DatabaseController -- Executed permissions query', level='DEBUG')
		self.permissions = {}
		for row in response:
			p1, p2 = row[0].split(' ON ')
			p1 = p1.split('GRANT ')[-1]
			p2 = p2.split(' TO ')[0]
			row_permission = p1.split(', ')
			database, table = p2.split('.')
			database = database.replace('`', '').strip().lower()
			table = table.replace('`', '').strip().lower()
			
			if database not in self.permissions:
				self.permissions[database] = {}
			self.permissions[database][table] = row_permission
		verbose('DatabaseController -- Loaded permissions: %s' % self.permissions, level='DEBUG')
	
	def check_permission_list(self, permission_list):
		if not self.db:
			verbose("DatabaseController -- No database selected.", level='ERROR')
			return False
		verbose('DatabaseController -- Checking permission list: %s' % permission_list, level='DEBUG')
		for line in permission_list:
			try:
				database, table, action = line.split('.')
			except ValueError:
				verbose('DatabaseController -- Invalid permission list format (format is `database.table.action`)', level='ERROR')
				return False
			database = database.replace('`', '')
			table = table.replace('`', '')
			if not self.check_permission(database, table, action):
				verbose('DatabaseController -- Permission check failed for `%s`' % line, level='ERROR')
				return False
		verbose('DatabaseController -- All permission checks succeeded', level='DEBUG')
		return True

	def check_permission(self, database, table, action):
		if not self.db:
			verbose("DatabaseController -- No database selected.", level='ERROR')
			return False
		database = database.replace('`', '').strip().lower()
		table = table.replace('`', '').strip().lower()

		key1 = database
		if database not in self.permissions.keys():
			if '*' in self.permissions.keys():
				key1 = '*'
			else:
				return False
		key2 = table
		if table not in self.permissions[key1].keys():
			if '*' in self.permissions[key1].keys():
				key2 = '*'
			else:
				return False
		if action in self.permissions[key1][key2] or 'ALL PRIVILEGES' in self.permissions[key1][key2]:
			return True
		return False
	
	def execute(self, queries):
		if self.dry_run:
			verbose('DatabaseController -- Dry-Run MySQL query: %s' % queries, level='WARNING')
			return [{
				'statement': queries,
				'result': [],
				'rowcount': 0
			}]
		out = []
		cursor = self.cursor()
		try:
			verbose('DatabaseController -- Executing MySQL query: %s' % queries, level='DEBUG')
			for result in cursor.execute(queries, multi=True):
				if result.with_rows:
					verbose('DatabaseController -- MySQL result with rows: %s' % result, level='DEBUG')
					out.append({
						'statement': result.statement,
						'result': result.fetchall()
					})
				else:
					verbose('DatabaseController -- MySQL result: %s' % result, level='DEBUG')
					out.append({
						'statement': result.statement,
						'rowcount': result.rowcount
					})
		except (RuntimeError, StopIteration) as e:
			pass
		
		self.commit()
		verbose('DatabaseController -- Execute value out: %s' % str(out), level='DEBUG')
		return out

#pragma mark -

def process_filter(s):
	verbose('Processing filter %s' % s)
	try:
		f = [x.strip() for x in s.split(',')]
	except (AttributeError, TypeError) as e:
		debug('Filter processing failed %s' % e)
		f = []
	if len(f) < 1:
		f = ['*']
		debug('No filter found, setting catch-all')
	verbose('Filter returned as %s' % str(f))
	return f

def retrieve_parameters(env, args):
	global RULEDATA_VALID_FIELDS, RULEATTRIBUTE_VALID_TYPES
		
	while not env['partner_id']:
		try:
			env['partner_id'] = int(args.partner_id)
		except (TypeError, ValueError):
			pass
		if isinstance(args.partner_id, str) and args.partner_id.upper() in env['partners'].keys():
			env['partner_id'] = env['partners'][args.partner_id.upper()]
		found_partner_id = False
		for partner in env['partners']:
			if env['partners'][partner] == env['partner_id']:
				found_partner_id = True
		if not found_partner_id:
			env['partner_id'] = None
		if not env['partner_id']:
			args.partner_id = input("Enter the partner id or code? ")
		
			
	verbose("Partner id set to: %d" % env['partner_id'])
	
	env['filter'] = {}
	if args.filter:
		for f in args.filter:
			field = f[0]
			if field not in RULEDATA_VALID_FIELDS and field not in RULEATTRIBUTE_VALID_TYPES:
				verbose('filter must be one of %s or %s' % (RULEDATA_VALID_FIELDS, RULEATTRIBUTE_VALID_TYPES), level='WARNING')
				continue
			values = process_filter(f[1])
			env['filter'][field] = values

	verbose("Filter set to: %s" % env['filter'])
	
	env['filter-action'] = args.filter_action
	if not env['filter-action']:
		env['filter-action'] = 'AND'
	
	env['group-by'] = args.group_by
	if not env['group-by']:
		env['group-by'] = 'rule_type'
	if env['group-by'] not in RULEDATA_VALID_FIELDS:
		env['group-by'] = 'rule_type'
		verbose('group-by must be one of %s' % RULEDATA_VALID_FIELDS, level='WARNING')

def show_rule_engine(env):
	global RULEDATA_VALID_FIELDS, RULEATTRIBUTE_VALID_TYPES
	rule_query = 'SELECT rd.id, partner_id, rule_type, hours_before, value, value_type, min, max, attr_type, attr_value FROM offers.RuleData AS rd LEFT JOIN offers.RuleAttribute AS ra ON rd.id = ra.rule_id WHERE partner_id = %s;'
	
	rules = env['db'].execute(rule_query % env['partner_id'])[0]['result']
	verbose("Executed Rule Engine query")
	
	val = {}
	
	for row in rules:
		if row[0] not in val.keys():
			rule = {'attributes': {}}
			if row[2]: rule['rule_type'] = row[2]
			if row[3]: rule['hours_before'] = float(row[3])
			if row[4] is not None: rule['value'] = float(row[4])
			if row[5]: rule['value_type'] = row[5]
			if row[6]: rule['min'] = float(row[6])
			if row[7]: rule['max'] = float(row[7])
			val[row[0]] = rule
		val[row[0]]['attributes'][row[8]] = row[9]

	filtered_rows = {}
	for row_id in val:
		should_include = True
		row = val[row_id]
		for field in env['filter']:
			if field in RULEATTRIBUTE_VALID_TYPES:
				if field not in row['attributes'] or ('*' not in env['filter'][field] and row['attributes'][field] not in env['filter'][field]):
					should_include = False
					if env['filter-action'] == 'AND':
						break
			else:
				if field not in row or ('*' not in env['filter'][field] and row[field] not in env['filter'][field]):
					should_include = False
					if env['filter-action'] == 'AND':
						break
		if should_include:
			filtered_rows[row_id] = row
	
	out = {}
	for row_id in filtered_rows:
		rule = dict(val[row_id])
		t = rule[env['group-by']]
		del rule[env['group-by']]
		if t not in out.keys():
			out[t] = {row_id: rule}
		else:
			out[t][row_id] = rule
	print(json.dumps(out, sort_keys=True, indent=2))
	return out



def show_reports(env):
	fields = [
		'r.id',
		'schedule_name',
		'report_name',
		'user_id',
		'delivery_method',
		'delivery_subdir',
		'mail_to',
		'mail_subject',
		'mail_message',
		'alt_filename',
		'filename_dateformat',
		'output_formats',
		'skip_empty_reports',
		'repeat_every',
		'repeat_time_unit',
		'next_runtime'
	]
	
	report_query = 'SELECT %s, param_name, param_value FROM reports.ScheduledReport AS r LEFT JOIN reports.ScheduledReportParam AS p ON r.id = p.schedule_id LEFT JOIN reports.ScheduledReportRuntime AS s ON r.id = s.scheduled_report_id WHERE partner_id = %s;'
	
	
	reports = env['db'].execute(report_query % (','.join(fields), env['partner_id']))[0]['result']
	verbose("Executed Reports query")
	
	val = {}
	
	for row in reports:
		if row[0] not in val.keys():
			rule = {'attributes': {}}
			for i in range(len(fields)):
				if row[i]: rule[fields[i]] = row[i]
			if 'next_runtime' in rule: rule['next_runtime'] = str(rule['next_runtime'])
			if 'r.id' in rule: del rule['r.id']
			val[row[0]] = rule
		t = row[len(fields)]
		v = row[len(fields)+1]
		out = val[row[0]]['attributes']
		if t not in val[row[0]]['attributes'].keys():
			val[row[0]]['attributes'][t] = v
		elif not isinstance(out[t], list) and v != val[row[0]]['attributes'][t]:
			val[row[0]]['attributes'][t] = [val[row[0]]['attributes'][t], v]
		else:
			if v not in val[row[0]]['attributes'][t]:
				val[row[0]]['attributes'][t].append(v)
	print(json.dumps(val, sort_keys=True, indent=2))
	return val

def show_rosey(env):
	rule_query = 'SELECT * FROM offers.AutoUpgradeSchedule AS aus LEFT JOIN offers.AutoUpgradeConfig AS auc ON aus.partner_id = auc.partner_id WHERE partner_id = %s;'
	
	return # TODO continue here
	rules = env['db'].execute(rule_query % env['partner_id'])[0]['result']
	verbose("Executed Rosey query")
	
	val = {}
	
	for row in rules:
		if row[0] not in val.keys():
			rule = {'attributes': {}}
			if row[2]: rule['type'] = row[2]
			if row[3]: rule['hours_before'] = float(row[3])
			if row[4] is not None: rule['value'] = float(row[4])
			if row[5]: rule['value_type'] = row[5]
			if row[6]: rule['min'] = float(row[6])
			if row[7]: rule['max'] = float(row[7])
			val[row[0]] = rule
		val[row[0]]['attributes'][row[10]] = row[11]
	
	out = {}
	
	for k in val:
		rule = dict(val[k])
		t = rule['type']
		del rule['type']
		if t not in out.keys():
			out[t] = {k: rule}
		else:
			out[t][k] = rule
	print(json.dumps(out, sort_keys=True, indent=2))
	return out		
	

def retrieve_partners(env):
	# Partners restrictions
	partner_query = "SELECT id, code FROM offers.Partners;"
	partners = env['db'].execute(partner_query)[0]['result']
	verbose("Executed Partners query")
	
	out = {}
	for row in partners:
		out[row[1]] = row[0]
	env['partners'] = out


def main():
	global FLAG_ENVIRONMENT
	
	parser = argparse.ArgumentParser('An assistant for convert complex DB relationships to simple JSON', usage=argparse.SUPPRESS)
	# Required or prompted options
	parser.add_argument('--partner-id', '-p', help='The partner id or code', metavar="ID")
	
	# Actions
	group = parser.add_argument_group('Actions')
	group.add_argument('--rule-engine', help='Show rule engine configuration', action='store_true')
	group.add_argument('--reports', help='Show reports configuration', action='store_true')
	group.add_argument('--rosey', help='Show Rosey configuration', action='store_true')
	
	# Filter for rule engine
	group = parser.add_argument_group('Rule engine')
	group.add_argument('--filter', '-f',  action='append', help='Filter rule engine for a specified attribute (e.g. `ORIGIN YUL,YYZ` or `ORIGIN *`)',  metavar=("KEY", "VALUE"), nargs=2)
	group.add_argument('--filter-action', '-a', help='Action to use when many filters are specified (OR, AND), default `AND`',  metavar="OR|AND")
	group.add_argument('--group-by', '-g', help='Group rule engine rows by specified rule_type/attr_type  (default `rule_type`)', metavar="PARAM")
	
	register_environment_args(parser, include_prod_confirm = False)
	register_verbose_args(parser)
	register_version_args(parser)
	register_database_args(parser)
	
	args = parser.parse_args()
	
	handle_database_args(args)
	handle_version_args(args)
	handle_verbose_args(args)
	handle_environment_args(args)
	
	env = {
		'partner_id': None,
	}
	
	db = DatabaseController()
	
	if FLAG_ENVIRONMENT == 'STAGE':
		db.select_stage()
	elif FLAG_ENVIRONMENT == 'PRODUCTION':
		db.select_prod()
	env['db'] = db
	
	if not db.check_permission_list([
		'offers.AutoUpgradeConfig.SELECT',
		'offers.AutoUpgradeSchedule.SELECT',
		'offers.Partners.SELECT',
		'offers.RuleAttribute.SELECT',
		'offers.RuleData.SELECT',
		'reports.ScheduledReport.SELECT',
		'reports.ScheduledReportParam.SELECT',
	]):
		return
	
	retrieve_partners(env)
	retrieve_parameters(env, args)
	
	if args.rule_engine:
		show_rule_engine(env)
	elif args.reports:
		show_reports(env)
	else:
		verbose('No flag was specified (Menu to be implemented in the future...)', level='WARNING')
		
	
if __name__ == '__main__':
	main()
