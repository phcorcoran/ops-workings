# OPS-WORKINGS

Learn about the workings of a ticket

Archive for ancillary work done for tickets, recorded to assist others for future similar tickets

## Guidelines

* Place your material in a hierarchy /PROJECT/PARTNER/
  * For example, work for "UPG-1234 WY(687) Do some work for stuff" would be stored in /UPG/WY/UPG-1234_some_work.sql

* You can push directly to master, or create a pull request if you would like to request code reviews

* You can store csv file, scripts, SQL queries, or any other related material that could be of interest to someone else taking on another similar ticket

## Python3

All utilities in this repository use Python3.

Here is how to install it on your computer:

```
brew install python3
```

## GnuPG

Various utilities in this repository use GnuPG to store securely your credentials on your computer.

Here is how to install GnuPG (change ~/.profile for your preferred file; other choices are  ~/.bashrc, ~/.zshrc, ~/.bash_profile, etc.)

```
brew install gnupg
echo -e "\nGPG_TTY=\$(tty);\nexport GPG_TTY;\ngpgconf --launch gpg-agent;\necho UPDATESTARTUPTTY | gpg-connect-agent &> /dev/null;\n" >> ~/.profile
*open a new shell*
gpg --full-generate-key
```

The options to use for GnuPG are:

1. (1) RSA and RSA (default)
2. Key Size 2048 is fine
3. Expiration 0 is fine
4. Your full name "Firstname Lastname"
5. Your plusgrade email (firstname.lastname@plusgrade.com)
6. Empty comment is fine


## offerutil.py
 
Here are the requirements for running offerutil.py:
 
```
brew ls --versions mysql && brew upgrade mysql || brew install mysql
brew cask install mysql-connector-python
sudo pip3 install python-gnupg requests mysql-connector-python npyscreen
```

Alternatively, you can run offerutil.py using docker, assuming you've set up GnuPG correctly:

```
make     # To build the docker image
make run # To run, then you can use offerutil.py inside the container
```

Here is the help menu for offerutil.py version 0.3.0

```
$ offerutil.py -h
optional arguments:
  -h, --help            show this help message and exit
  --partner-id ID, -p ID
                        The partner id or code
  --version             Output script version

Actions:
  --list, -l            List the eligible routes with flight numbers and fare
                        classes
  --generate-token, --instant-token, -i
                        Generate a random instant token
  --generate-bidding-token, --token, -t
                        Ignore instant upgrade options, generate bidding token
  --troubleshoot        Ignore all options, troubleshoot instant upgrade
                        tables

Filters:
  --fare-class FILTER   Filter for FROM fare classes (Ex. Y,PY or *)
  --cabin-type TYPE     Filter for FROM cabin type
  --equipment EQUIP     Filter for equipment type (e.g. 338,388 or ~738 or *)
  --origin FILTER, -o FILTER
                        Filter for origin (e.g. YUL,YYZ or *)
  --destination FILTER, -d FILTER
                        Filter for destination (e.g. LGA,YVR or *)

Token generator options:
  --hours-before HOURS  Select hours before departure
  --extra-seat          Further check for extra seat eligibility
  --show-raw-token      Show raw generated token
  --suppress-raw-payload
                        Suppress raw generated payload
  --segment-count COUNT
                        Specify the number of segments
  --pax COUNT           Specify the number of pax
  --email EMAIL         Specify the email
  --departure-date DEPARTURE_DATE
                        Specify the departure date (Bidding tokens only)

Cruise options:
  --duration DAYS       Select duration in days for cruise
  --from-cabin CABIN    Select from cabin for cruise
  --after DATE          Restrict to cruises after date (e.g. 2030-01-01)
  --show-all            Show all cruises instead of one per duration

Email options:
  --show-email-links, -a
                        Show all email links

Override generator options:
  --suppress-key KEY, -s KEY
                        Strip out key from payload, regardless of other
                        options
  --force-key KEY VALUE, -f KEY VALUE
                        Set key value pair in payload, regardless of other
                        options
  --edit-payload        Gives a chance to edit the payload before posting

Environment:
  --stage               Select STAGE database
  --prod                Select PRODUCTION database

Verbose:
  --verbose, -v         Output more information (-vv debug, -vvv trace)
  --quiet, -q           Suppress warnings (-qq errors, -qqq fatal)
```

 