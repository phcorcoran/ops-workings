#!/usr/bin/env python3
import os, sys

#pragma mark Version Handling

FLAG_VERSION = '0.4.0'

def register_version_args(parser):
	parser.add_argument('--version', help='Output script version', action='store_true')
	
def handle_version_args(args):
	global FLAG_VERSION
	try:
		if args.version:
			print('%s Ver' % os.path.basename(__file__), FLAG_VERSION)
			exit()
	except AttributeError:
		pass

#pragma mark -

#pragma mark Import Handling

try:
	import requests
except ModuleNotFoundError as e:
	print('[ERROR] %s' % e)
	print('[ERROR] Run `sudo pip3 install requests`')
	exit()

from pprint import pprint
import argparse
import datetime
from random import randint, choice
import json
from io import StringIO
from getpass import getpass
from subprocess import run, DEVNULL, check_output, CalledProcessError
from webbrowser import open_new_tab

#pragma mark -

#pragma mark Verbose Handling

FLAG_VERBOSE = 2
FLAG_VERBOSE_LEVELS = [
	['CRITICAL', '\033[31m'],
	['ERROR', '\033[31m'],
	['WARNING', '\033[33m'],
	['INFO', '\033[32m'],
	['DEBUG', '\033[36m'],
	['TRACE', '\033[90m'],
]

def _verbose(*args, level):
	global FLAG_VERBOSE, FLAG_VERBOSE_LEVELS
	color_end = '\033[0m'
	for i in range(FLAG_VERBOSE+1):
		if FLAG_VERBOSE_LEVELS[i][0] == level:
			print("[%s%s%s]" % (FLAG_VERBOSE_LEVELS[i][1], level, color_end), *args)

def critical(*args):	_verbose(*args, level='CRITICAL')
def error(*args):	_verbose(*args, level='ERROR')
def warning(*args):	_verbose(*args, level='WARNING')
def info(*args):	_verbose(*args, level='INFO')
def debug(*args):	_verbose(*args, level='DEBUG')
def trace(*args):	_verbose(*args, level='TRACE')

def verbose_level():
	global FLAG_VERBOSE, FLAG_VERBOSE_LEVELS
	return FLAG_VERBOSE_LEVELS[FLAG_VERBOSE][0]

def set_verbose_level(level):
	global FLAG_VERBOSE, FLAG_VERBOSE_LEVELS
	for i in range(len(FLAG_VERBOSE_LEVELS)):
		if FLAG_VERBOSE_LEVELS[i][0] == level:
			FLAG_VERBOSE = i

def register_verbose_args(parser):
	group = parser.add_argument_group('Verbose')
	group.add_argument('--verbose', '-v', help='Output more information (-vv debug, -vvv trace)', action='count', default=0)
	group.add_argument('--quiet', '-q', help='Suppress warnings (-qq errors, -qqq fatal)', action='count', default=0)

def trace_calls(frame, event, arg):
	if event != 'call' or frame.f_code.co_name == 'write': return
	try:
		print('[%sTRACE%s] Called `%s` (line %s of `%s`) from line %s of `%s`' %  ('\033[90m', '\033[0m', frame.f_code.co_name, frame.f_lineno, frame.f_code.co_filename, frame.f_back.f_lineno, frame.f_back.f_code.co_filename))
	except AttributeError:
		pass
	return

def handle_verbose_args(args):
	global FLAG_VERBOSE
	try:
		FLAG_VERBOSE += args.verbose - args.quiet
	except AttributeError:
		pass
	if FLAG_VERBOSE >= 5:
		try:
			sys.settrace(trace_calls)
		except NameError:
			pass

#pragma mark -

#pragma mark Environment Handling

FLAG_ENVIRONMENT = None

def register_environment_args(parser, include_prod_readonly = False, include_prod_confirm = True):
	debug('Environment: Setting up argument group')
	group = parser.add_argument_group('Environment')
	debug('Environment: Adding STAGE argument')
	group.add_argument('--stage', help='Select STAGE database', action='store_true')
	debug('Environment: Adding PRODUCTION argument')
	group.add_argument('--prod', help='Select PRODUCTION database', action='store_true')
	if include_prod_readonly:
		debug('Environment: Adding PRODUCTION READ-ONLY argument')
		parser.add_argument('--prod-readonly', help='Select PRODUCTION READ-ONLY database', action='store_true')
	if include_prod_confirm:
		debug('Environment: Adding skip-prod-confirm argument')
		parser.add_argument('--skip-prod-confirm', help='Disable PRODUCTION confirmation prompt', action='store_true')
	
def handle_environment_args(args, default = None):
	global FLAG_ENVIRONMENT
	include_prod_readonly = True
	include_prod_confirm = True
	
	try: 
		if args.skip_prod_confirm: include_prod_confirm = False
	except AttributeError: include_prod_confirm = False
	
	debug('Environment: include_prod_confirm set to %s' % str(include_prod_confirm))
	
	env_select = None
	
	if default == 'STAGE':
		env_select = 1
	elif default == 'PRODUCTION':
		env_select = 2
	elif default == 'PRODUCTION READ-ONLY':
		env_select = 3
	
	try:
		if args.prod: env_select = 2
	except AttributeError: pass
	
	try:
		if args.prod_readonly: env_select = 3
	except AttributeError: include_prod_readonly = False
	
	try:
		if args.stage: env_select = 1
	except AttributeError: pass
	
	debug('Environment: env_select set to %s' % str(env_select))
	
	if not env_select:
		if include_prod_readonly:
			print("1. STAGE\n2. PRODUCTION\n3. PRODUCTION READ-ONLY")
		else:
			print("1. STAGE\n2. PRODUCTION")
	
		max_option = 2
		if include_prod_readonly: max_option = 3
	
		while not env_select:
			try:
				env_select = int(input("Select the desired environment? "))
				if env_select < 1 or env_select > max_option:
					env_select = None
					raise ValueError
			except ValueError:
				print("Invalid selection")
	
	debug('Environment: env_select set to %s' % str(env_select))
	
	if env_select == 1:
		FLAG_ENVIRONMENT = 'STAGE'
	elif env_select == 2:
		FLAG_ENVIRONMENT = 'PRODUCTION'
		if include_prod_confirm:
			print("===== CONFIRM CHANGES TO PRODUCTION =====")
			x = input("Confirm (y/n)? ").strip()
			if x != 'y':
				exit()
	elif env_select == 3:
		FLAG_ENVIRONMENT = 'PRODUCTION READ-ONLY'
	
	debug('Environment: FLAG_ENVIRONMENT set to %s' % str(FLAG_ENVIRONMENT))

#pragma mark -

#pragma mark -

#pragma mark DatabaseController

def register_database_args(parser, include_superuser = False):
	group = parser.add_argument_group('Database')
	group.add_argument('--db-user', help='Database user')
	group.add_argument('--db-password', help='Database password')
	group.add_argument('--db-email', help='Database user email')
	group.add_argument('--db-capture-credentials', help='Force capture of database credentials for encrypted local storage', action='store_true')
	if include_superuser:
		parser.add_argument('--db-superuser', help='Use superuser credentials', action='store_true')

def handle_database_args(args):
	DatabaseController.override_user = args.db_user
	DatabaseController.override_password = args.db_password
	DatabaseController.override_email = args.db_email
	DatabaseController.override_capture_credentials = args.db_capture_credentials
	try:
		DatabaseController.superuser = args.db_superuser
	except AttributeError:
		pass

class DatabaseController:
	version = '0.3.0'
	override_user = ''
	override_password = ''
	override_email = ''
	override_capture_credentials = False
	superuser = False
	
	def __init__(self):
		try:
			import mysql.connector
		except ModuleNotFoundError as e:
			error(e)
			error('Run `brew ls --versions mysql && brew upgrade mysql || brew install mysql`')
			error('Run `brew cask install mysql-connector-python`')
			error('Run `sudo pip3 install mysql-connector-python`')
			exit()
		
		self.mysql_connector = mysql.connector
		
		self.db = None
		self.dry_run = False
		self.override_user = DatabaseController.override_user
		self.override_password = DatabaseController.override_password
		self.override_email = DatabaseController.override_email
		self.override_credentials_capture = DatabaseController.override_capture_credentials
		if not self.override_credentials_capture:
			if self.override_user or self.override_password:
				if not self.override_user: self.override_user = 'plusgrade'
				self.credentials = self.minimal_credentials()
				return
		self.permissions = {}

		self.load_gpg_credentials()
	
	def load_gpg_credentials(self):
		try:
			import gnupg
		except ModuleNotFoundError as e:
			error('DB credentials can be speficied as command line arguments (--db-user & --db-password), or can be stored locally using GnuPG encryption')
			error(e)
			error('For the latter, follow these instructions:')
			error('Run `brew install gnupg`')
			error('Run `sudo pip3 install python-gnupg`')
			error('Adjust the next command with your own shell profile location (could be .bash_profile, .zshrc, .bashrc, etc)')
			error('Run `echo -e "\nGPG_TTY=\$(tty);\nexport GPG_TTY;\ngpgconf --launch gpg-agent;\necho UPDATESTARTUPTTY | gpg-connect-agent &> /dev/null;\n" >> ~/.profile`')
			error('Run `gpg --full-generate-key`')
			exit()
		
		self.gpgpath = os.path.expanduser("~/.gnupg/")
		debug('DatabaseController -- GPG path is set to %s' % self.gpgpath)
		if DatabaseController.superuser:
			self.credentials_path = os.path.expanduser("~/.gnupg/credentials-superuser.json.gpg")
		else:
			self.credentials_path = os.path.expanduser("~/.gnupg/credentials.json.gpg")
		debug('DatabaseController -- GPG credentials path is set to %s' % self.credentials_path)
		self.update_start_tty()
		self.gpg = gnupg.GPG(gnupghome=self.gpgpath)
		self.gpg.encoding = 'utf-8'
		self.retrieve_credentials()
	
	def retrieve_email(self):
		if self.override_email:
			return self.override_email
		email = ''
		try:
			gpg_identity = self.gpg.list_keys()[0]['uids'][0]
			email = gpg_identity.split('@')[0].split('<')[-1]
		except (IndexError, KeyError) as e:
			pass
		return email
	
	def update_start_tty(self):
		from subprocess import run, DEVNULL
		run(['gpg-connect-agent', 'UPDATESTARTUPTTY', '/bye'], stdout=DEVNULL, stderr=DEVNULL)
	
	def minimal_credentials(self):
		return {
			'STAGE': {
				'host': 'db.stage.plusgrade.com',
				'port': '3306',
				'user': self.override_user,
				'passwd': self.override_password
			},
			'PRODUCTION READ-ONLY': {
				'host': 'db-rr.plusgrade.com',
				'port': '3306',
				'user': self.override_user,
				'passwd': self.override_password
			},
			'PRODUCTION': {
				'host': 'db.plusgrade.com',
				'port': '3306',
				'user': self.override_user,
				'passwd': self.override_password
			}
		}
			
	def generate_db_credentials(self):
		from getpass import getpass
		warning('DB credentials can be speficied as command line arguments (--db-user & --db-password), or can be stored locally using GnuPG encryption')
		user = input('Enter DB username (e.g. john.D) ? ')
		stage_passwd = getpass(prompt='Enter stage password? ')
		prod_passwd = getpass(prompt='Enter prod password? ')
		
		credentials = self.minimal_credentials()
		credentials['STAGE']['user'] = user
		credentials['PRODUCTION READ-ONLY']['user'] = user
		credentials['PRODUCTION']['user'] = user
		credentials['STAGE']['passwd'] = stage_passwd
		credentials['PRODUCTION READ-ONLY']['passwd'] = prod_passwd
		credentials['PRODUCTION']['passwd'] = prod_passwd
		
		info('DatabaseController -- Creating credentials file')
		recipient = self.gpg.list_keys()[0]['keyid']
		debug('DatabaseController -- GPG Recipient: %s' % recipient)
		s = self.gpg.encrypt(json.dumps(credentials), [recipient])
		debug('DatabaseController -- GPG Response: %s' % s)
		if s.ok:
			with open(self.credentials_path, 'w') as f:
				f.write(str(s))
		else:
			error('DatabaseController -- GPG Status: %s' % s.status)
			error('DatabaseController -- GPG Error: %s' % s.stderr)
			exit()
	
	def retrieve_credentials(self):
		if not os.path.exists(self.credentials_path):
			debug('DatabaseController -- Credentials file not found.')
			self.generate_db_credentials()
		if self.override_credentials_capture:
			debug('DatabaseController -- Forcing capture of credentials')
			self.override_credentials_capture = False
			self.generate_db_credentials()
		with open(self.credentials_path, 'rb') as f:
			s = self.gpg.decrypt_file(f)
		if s.ok:
			debug('Credentials file loaded correctly.')
			self.credentials = json.loads(str(s))
		else:
			info('DatabaseController -- GPG Status: %s' % s.status)
			info('DatabaseController -- GPG Error: %s' % s.stderr)
			warning('DatabaseController -- Could not retrieve DB credentials')
			self.generate_db_credentials()
			return self.retrieve_credentials()
			
			
	
	def enable_dry_run(self):
		self.dry_run = True
	def disable_dry_run(self):
		self.dry_run = False

	def select_stage(self):
		if self.db: self.close()
		self.load_db("STAGE")
	def select_prod_readonly(self):
		if self.db: self.close()
		self.load_db("PRODUCTION READ-ONLY")
	def select_prod(self):
		if self.db: self.close()
		self.load_db("PRODUCTION")
	
	def load_db(self, name):
		if self.dry_run:
			self.db = self.credentials[name]
			self.permissions = {'*': {'*': 'ALL PRIVILEGES'}}
			return
		debug('DatabaseController -- Opening MySQL connection')
		self.db = self.mysql_connector.connect(
			host=self.credentials[name]['host'],
			port=self.credentials[name]['port'],
			user=self.credentials[name]['user'],
			passwd=self.credentials[name]['passwd']
		)
		self.retrieve_all_permissions()
	
	def cursor(self):
		if not self.db:
			error("DatabaseController -- No database selected.")
			return
		if self.dry_run:
			return None
		return self.db.cursor()
	
	def commit(self):
		if not self.db: return
		self.db.commit()
	
	def close(self):
		if not self.db: return
		self.db.close()
		self.db = None
	
	def retrieve_all_permissions(self):
		if not self.db:
			error("DatabaseController -- No database selected.")
			return
		grant_query = 'SHOW GRANTS;'
		response = self.execute(grant_query)[0]['result']
		debug('DatabaseController -- Executed permissions query')
		self.permissions = {}
		for row in response:
			p1, p2 = row[0].split(' ON ')
			p1 = p1.split('GRANT ')[-1]
			p2 = p2.split(' TO ')[0]
			row_permission = p1.split(', ')
			database, table = p2.split('.')
			database = database.replace('`', '').strip().lower()
			table = table.replace('`', '').strip().lower()
			
			if database not in self.permissions:
				self.permissions[database] = {}
			self.permissions[database][table] = row_permission
		debug('DatabaseController -- Loaded permissions: %s' % self.permissions)
	
	def check_permission_list(self, permission_list):
		if not self.db:
			error("DatabaseController -- No database selected.")
			return False
		debug('DatabaseController -- Checking permission list: %s' % permission_list)
		for line in permission_list:
			try:
				database, table, action = line.split('.')
			except ValueError:
				error('DatabaseController -- Invalid permission list format (format is `database.table.action`)')
				return False
			database = database.replace('`', '')
			table = table.replace('`', '')
			if not self.check_permission(database, table, action):
				error('DatabaseController -- Permission check failed for `%s`' % line)
				return False
		debug('DatabaseController -- All permission checks succeeded')
		return True

	def check_permission(self, database, table, action):
		if not self.db:
			error("DatabaseController -- No database selected.")
			return False
		database = database.replace('`', '').strip().lower()
		table = table.replace('`', '').strip().lower()

		key1 = database
		if database not in self.permissions.keys():
			if '*' in self.permissions.keys():
				key1 = '*'
			else:
				return False
		key2 = table
		if table not in self.permissions[key1].keys():
			if '*' in self.permissions[key1].keys():
				key2 = '*'
			else:
				return False
		if action in self.permissions[key1][key2] or 'ALL PRIVILEGES' in self.permissions[key1][key2]:
			return True
		return False
	
	def execute(self, queries):
		if self.dry_run:
			warning('DatabaseController -- Dry-Run MySQL query: %s' % queries)
			return [{
				'statement': queries,
				'result': [],
				'rowcount': 0
			}]
		out = []
		cursor = self.cursor()
		try:
			debug('DatabaseController -- Executing MySQL query: %s' % queries)
			for result in cursor.execute(queries, multi=True):
				if result.with_rows:
					debug('DatabaseController -- MySQL result with rows: %s' % result)
					out.append({
						'statement': result.statement,
						'result': result.fetchall()
					})
				else:
					debug('DatabaseController -- MySQL result: %s' % result)
					out.append({
						'statement': result.statement,
						'rowcount': result.rowcount
					})
		except (RuntimeError, StopIteration) as e:
			pass
		
		self.commit()
		debug('DatabaseController -- Execute value out: %s' % str(out))
		return out

#pragma mark -


#pragma mark Global Variables


REQUESTS_ENV = {
 'PRODUCTION': {'COMMUNICATION_SERVICE': 'https://ws-communication.prd.internal.plusgrade.com',
          'CONFIGURATION_SERVICE': ['https://configuration.prd.internal.plusgrade.com/configuration-service',
          'https://configuration.prdb.internal.plusgrade.com/configuration-service'],
          'ELASTIC_SEARCH': 'https://search-prod.plusgrade.com',
          'ELASTIC_SEARCH_AUTH_KEY': 'Basic '
                                     'ZXNfdXNlcjpIc2IwdGdDdlhNcklTNzhNNFV2aVJDVmY',
          'HOST_GATEWAY': 'https://services.plusgrade.com/host-gateway',
          'INVENTORY_APP': 'https://services.plusgrade.com/inventory-app',
          'INVENTORY_SERVICE': 'https://services.plusgrade.com',
          'PARTNER_GATEWAY': 'https://partner-gateway.plusgrade.com',
          'REQUEST_ROUTER': 'https://ws-request-router.plusgrade.com',
          'REQUEST_SERVICE': 'https://request.plusgrade.com',
          'REQUEST_SERVICE_AUTH_KEY': 'e8sRg75754218a6uuJ52vv7W',
          'TOKEN_GENERATOR': 'https://ws-testing.mgmt.internal.plusgrade.com/ws-testing/',
          'TOKEN_GENERATOR_ENV': 'PRODUCTION'},
 'STAGE': {'COMMUNICATION_SERVICE': 'https://ws-communication.stg.internal.plusgrade.com',
           'CONFIGURATION_SERVICE': ['https://configuration.stg.internal.plusgrade.com/configuration-service'],
           'ELASTIC_SEARCH': 'https://vpc-stg-app-logs-wxrn7m4tkdnvbpagpqawy56fpa.us-east-1.es.amazonaws.com',
           'ELASTIC_SEARCH_AUTH_KEY': 'Basic '
                                      'ZXNfdXNlcjpIc2IwdGdDdlhNcklTNzhNNFV2aVJDVmY',
           'HOST_GATEWAY': 'https://host-gateway.stg.internal.plusgrade.com/host-gateway',
           'INVENTORY_APP': 'https://ws-inventory.stg.internal.plusgrade.com/inventory-app/',
           'MARKETING_ENGINE': 'https://ws-marketing-engine.stg.internal.plusgrade.com',
           'METRICS_ES': 'https://vpc-stage-metrics-60-akspb7s3vk76x5xk7wfi55o37u.us-east-1.es.amazonaws.com',
           'OFFERS_SERVICE': 'https://offers.stg.internal.plusgrade.com/partner-offers/service',
           'PARTNER_APP': '\x01',
           'PARTNER_GATEWAY': 'https://partner-gateway.stage.plusgrade.com',
           'PAYMENT_GATEWAY_PROFILE': 'https://payment-gateway.stg.internal.plusgrade.com/payment-gateway',
           'REQUEST_ROUTER': 'https://ws-request-router.stg.internal.plusgrade.com',
           'REQUEST_SERVICE': 'https://ws-request.stg.internal.plusgrade.com',
           'REQUEST_SERVICE_AUTH_KEY': 'e8sRg75754218a6uuJ52vv7W',
           'TOKEN_GENERATOR': 'https://ws-testing.mgmt.internal.plusgrade.com/ws-testing',
           'TOKEN_GENERATOR_ENV': 'STAGE'}
}

CUSTOM_FARE_CLASS = {
  13: {
    'config': {
      'default': 'Y',
      'priority': [
        'International',
        'Neighbor Island',
      ],
    },
    'values': {
      'Neighbor Island': {
        "F": "BUSINESS",
        "J": "BUSINESS",
        "P": "BUSINESS",
        "A": "BUSINESS",
        "C": "BUSINESS",
        "D": "BUSINESS",
        "B": "ECONOMY",
        "E": "ECONOMY",
        "G": "ECONOMY",
        "H": "ECONOMY",
        "I": "ECONOMY",
        "K": "ECONOMY",
        "L": "ECONOMY",
        "M": "ECONOMY",
        "N": "ECONOMY",
        "O": "ECONOMY",
        "Q": "ECONOMY",
        "R": "ECONOMY",
        "S": "ECONOMY",
        "T": "ECONOMY",
        "U": "ECONOMY",
        "V": "ECONOMY",
        "W": "ECONOMY",
        "X": "ECONOMY",
        "Y": "ECONOMY",
        "Z": "ECONOMY",
      },
      'International': {
        "F": "REGIONAL_BUSINESS",
        "J": "REGIONAL_BUSINESS",
        "P": "REGIONAL_BUSINESS",
        "A": "REGIONAL_BUSINESS",
        "C": "REGIONAL_BUSINESS",
        "D": "REGIONAL_BUSINESS",
        "B": "ECONOMY",
        "E": "ECONOMY",
        "G": "ECONOMY",
        "H": "ECONOMY",
        "I": "ECONOMY",
        "K": "ECONOMY",
        "L": "ECONOMY",
        "M": "ECONOMY",
        "N": "ECONOMY",
        "O": "ECONOMY",
        "Q": "ECONOMY",
        "R": "ECONOMY",
        "S": "ECONOMY",
        "T": "ECONOMY",
        "U": "ECONOMY",
        "V": "ECONOMY",
        "W": "ECONOMY",
        "X": "ECONOMY",
        "Y": "ECONOMY",
        "Z": "ECONOMY",
      },
    },
  },
  28: {
    'config': {
      'default': 'H',
      'priority': [
        'long-haul',
        'short-haul',
      ],
    },
    'values': {
      'short-haul': {
        "J": "REGIONAL_BUSINESS",
        "C": "REGIONAL_BUSINESS",
        "D": "REGIONAL_BUSINESS",
        "Z": "REGIONAL_BUSINESS",
        "P": "REGIONAL_BUSINESS",
        "I": "REGIONAL_BUSINESS",
        "Y": "PREMIUM_ECONOMY",
        "B": "PREMIUM_ECONOMY",
        "M": "PREMIUM_ECONOMY",
        "X": "PREMIUM_ECONOMY",
        "U": "ECONOMY",
        "H": "ECONOMY",
        "Q": "ECONOMY",
        "V": "ECONOMY",
        "W": "ECONOMY",
        "S": "ECONOMY",
        "T": "ECONOMY",
        "L": "ECONOMY",
        "K": "ECONOMY",
        "A": "ECONOMY",
        "O": "ECONOMY",
        "F": "ECONOMY",
      },
      'long-haul': {
        "J": "BUSINESS",
        "C": "BUSINESS",
        "D": "BUSINESS",
        "Z": "BUSINESS",
        "P": "BUSINESS",
        "I": "BUSINESS",
        "E": "LONGHAUL_PREMIUM_ECONOMY",
        "N": "LONGHAUL_PREMIUM_ECONOMY",
        "G": "LONGHAUL_PREMIUM_ECONOMY",
        "R": "LONGHAUL_PREMIUM_ECONOMY",
        "Y": "ECONOMY",
        "B": "ECONOMY",
        "M": "ECONOMY",
        "X": "ECONOMY",
        "U": "ECONOMY",
        "H": "ECONOMY",
        "Q": "ECONOMY",
        "V": "ECONOMY",
        "W": "ECONOMY",
        "S": "ECONOMY",
        "T": "ECONOMY",
        "L": "ECONOMY",
        "K": "ECONOMY",
        "A": "ECONOMY",
        "O": "ECONOMY",
      },
    },
  },
  566: {
    'config': {
      'default': 'Y',
      'priority': [
        'Asie',
        'AU NZ',
        'PPT',
        'Southpac',

      ],
    },
    'values': {
      'Asie': {
        "J": "BUSINESS",
        "C": "BUSINESS",
        "D": "BUSINESS",
        "Z": "BUSINESS",
        "I": "BUSINESS",
        "O": "BUSINESS",
        "W": "PREMIUM_ECONOMY",
        "P": "PREMIUM_ECONOMY",
        "F": "PREMIUM_ECONOMY",
        "Y": "ECONOMY",
        "B": "ECONOMY",
        "X": "ECONOMY",
        "E": "ECONOMY",
        "S": "ECONOMY",
        "H": "ECONOMY",
        "K": "ECONOMY",
        "Q": "ECONOMY",
        "R": "ECONOMY",
        "U": "ECONOMY",
        "V": "ECONOMY",
        "M": "ECONOMY",
        "A": "ECONOMY",
        "N": "ECONOMY",
        "L": "ECONOMY",
        "T": "ECONOMY",
      },
      'AU NZ': {
        "J": "BUSINESS",
        "D": "BUSINESS",
        "I": "BUSINESS",
        "O": "BUSINESS",
        "W": "PREMIUM_ECONOMY",
        "P": "PREMIUM_ECONOMY",
        "F": "PREMIUM_ECONOMY",
        "Y": "ECONOMY",
        "B": "ECONOMY",
        "X": "ECONOMY",
        "E": "ECONOMY",
        "S": "ECONOMY",
        "H": "ECONOMY",
        "K": "ECONOMY",
        "L": "ECONOMY",
        "R": "ECONOMY",
        "U": "ECONOMY",
        "Q": "ECONOMY",
        "V": "ECONOMY",
        "M": "ECONOMY",
        "T": "ECONOMY",
        "N": "ECONOMY",
        "A": "ECONOMY",
        "C": "ECONOMY",
        "G": "ECONOMY",
      },
      'PPT': {
        "J": "BUSINESS",
        "D": "BUSINESS",
        "Z": "BUSINESS",
        "I": "BUSINESS",
        "O": "BUSINESS",
        "W": "PREMIUM_ECONOMY",
        "P": "PREMIUM_ECONOMY",
        "F": "PREMIUM_ECONOMY",
        "Y": "ECONOMY",
        "B": "ECONOMY",
        "X": "ECONOMY",
        "E": "ECONOMY",
        "S": "ECONOMY",
        "H": "ECONOMY",
        "K": "ECONOMY",
        "L": "ECONOMY",
        "R": "ECONOMY",
        "U": "ECONOMY",
        "Q": "ECONOMY",
        "V": "ECONOMY",
        "M": "ECONOMY",
        "T": "ECONOMY",
        "N": "ECONOMY",
        "A": "ECONOMY",
        "G": "ECONOMY",
      },
      'Southpac': {
        "J": "BUSINESS",
        "D": "BUSINESS",
        "I": "BUSINESS",
        "O": "BUSINESS",
        "W": "PREMIUM_ECONOMY",
        "P": "PREMIUM_ECONOMY",
        "F": "PREMIUM_ECONOMY",
        "Y": "ECONOMY",
        "B": "ECONOMY",
        "X": "ECONOMY",
        "E": "ECONOMY",
        "S": "ECONOMY",
        "H": "ECONOMY",
        "K": "ECONOMY",
        "L": "ECONOMY",
        "R": "ECONOMY",
        "U": "ECONOMY",
        "Q": "ECONOMY",
        "V": "ECONOMY",
        "M": "ECONOMY",
        "T": "ECONOMY",
        "N": "ECONOMY",
        "A": "ECONOMY",
        "G": "ECONOMY",
      },
    },
  },
  573: {
    'config': {
      'default': 'X',
      'priority': [
        'long-haul',
        'short-haul',
      ],
    },
    'values': {
      'short-haul': {
        "C": "PREMIUM_ECONOMY",
        "J": "PREMIUM_ECONOMY",
        "Y": "PREMIUM_ECONOMY",
        "S": "PREMIUM_ECONOMY",
        "B": "PREMIUM_ECONOMY",
        "P": "PREMIUM_ECONOMY",
        "A": "PREMIUM_ECONOMY",
        "E": "ECONOMY",
        "M": "ECONOMY",
        "H": "ECONOMY",
        "Q": "ECONOMY",
        "W": "ECONOMY",
        "U": "ECONOMY",
        "K": "ECONOMY",
        "L": "ECONOMY",
        "T": "ECONOMY",
        "O": "ECONOMY",
        "V": "ECONOMY",
        "G": "ECONOMY",
        "X": "ECONOMY",
      },
      'long-haul': {
        "C": "BUSINESS",
        "D": "BUSINESS",
        "Z": "BUSINESS",
        "Y": "PREMIUM_ECONOMY",
        "S": "PREMIUM_ECONOMY",
        "B": "PREMIUM_ECONOMY",
        "P": "PREMIUM_ECONOMY",
        "A": "PREMIUM_ECONOMY",
        "F": "PREMIUM_ECONOMY",
        "E": "ECONOMY",
        "M": "ECONOMY",
        "H": "ECONOMY",
        "Q": "ECONOMY",
        "W": "ECONOMY",
        "U": "ECONOMY",
        "K": "ECONOMY",
        "L": "ECONOMY",
        "T": "ECONOMY",
        "O": "ECONOMY",
        "V": "ECONOMY",
        "G": "ECONOMY",
        "X": "ECONOMY",
      },
    },
  },
}

GRAPHICAL_MODULE = None
GRAPHICAL_SAVE_PICKLE_PATH = os.path.expanduser('~/.offerutil.py.pickle')
GRAPHICAL_THEME_CONFIG_PATH = os.path.expanduser('~/.offerutil.conf')
GRAPHICAL_WINDOW = None
GRAPHICAL_POPUP = None
GRAPHICAL_CONFIG = {}

#pragma mark -

def process_filter(s):
	info('Processing filter %s' % s)
	try:
		if len(s) < 1: s = None
		f = [x.strip() for x in s.split(',')]
	except (AttributeError, TypeError) as e:
		debug('Filter processing failed %s' % e)
		f = []
	if len(f) < 1:
		f = ['*']
		debug('No filter found, setting catch-all')
	info('Filter returned as %s' % str(f))
	return f

def reverse_filter(arr):
	return ','.join(arr) if arr != None and '*' not in arr else ''

def retrieve_args():
	parser = argparse.ArgumentParser('An assistant for handling offers', usage=argparse.SUPPRESS)
	# Required or prompted options
	parser.add_argument('--partner-id', '-p', help='The partner id or code', metavar="ID")
	
	# Actions
	group = parser.add_argument_group('Actions')
	group.add_argument('--list', '-l', help='List the eligible routes with flight numbers and fare classes', action='store_true')
	group.add_argument('--generate-token', '--token', '-t', help='Generate token', action='store_true')
	group.add_argument('--troubleshoot', help='Ignore all options, troubleshoot instant upgrade tables', action='store_true')
	
	# Filter options
	group = parser.add_argument_group('Filters')
	group.add_argument('--fare-class', help="Filter for FROM fare classes (Ex. Y,PY or *)", metavar="FILTER")
	group.add_argument('--cabin-type', help='Filter for FROM cabin type', metavar="TYPE")
	group.add_argument('--equipment', '-e', help='Filter for equipment type (e.g. 338,388 or ~738 or *)', metavar="EQUIP")
	group.add_argument('--origin', '-o', help='Filter for origin (e.g. YUL,YYZ or *)', metavar="FILTER")
	group.add_argument('--destination', '-d', help='Filter for destination (e.g. LGA,YVR or *)', metavar="FILTER")
	
	# Token generator options
	group = parser.add_argument_group('Token generator options')
	group.add_argument('--experimental', help='Bypass token create endpoint (EXPERIMENTAL)', action='store_true')
	group.add_argument('--hours-before', help='Select hours before departure', metavar="HOURS")
	group.add_argument('--extra-seat', help='Further check for extra seat eligibility', action='store_true')
	group.add_argument('--instant', '-i', help='Further check for instant upgrade eligibility', action='store_true')
	group.add_argument('--show-raw-token', help='Show raw generated token', action='store_true')
	group.add_argument('--suppress-raw-payload', help='Suppress raw generated payload', action='store_true')
	group.add_argument('--departure-date', help='Specify the departure date')
	
	# Cruise options
	group = parser.add_argument_group('Cruise options')
	group.add_argument('--duration', help='Select duration in days for cruise', metavar="DAYS")
	group.add_argument('--from-cabin', help='Select from cabin for cruise', metavar="CABIN")
	#group.add_argument('--upgrade-type', help='Select upgrade-type for cruise', metavar="TYPE")
	group.add_argument('--after', help='Restrict to cruises after date (e.g. 2030-01-01)', metavar="DATE")
	group.add_argument('--show-all', help='Show all cruises instead of one per duration', action='store_true')
	group.add_argument('--suppress-cruise-details', help='Suppress information about fare classes for ships', action='store_true')
	
	# Email options
	group = parser.add_argument_group('Email options')
	group.add_argument('--show-email-links', '-a', help='Show all email links', action='store_true')
	
	# Override generator options
	group = parser.add_argument_group('Override generator options')
	group.add_argument('--suppress-key', '-s', help='Strip out key from payload, regardless of other options', action='append', metavar="KEY")
	group.add_argument('--force-key', '-f', help='Set key value pair in payload, regardless of other options', action='append', metavar=("KEY", "VALUE"), nargs=2)
	
	# Live auction options
	group = parser.add_argument_group('Live auction options')
	group.add_argument('--live-auction', help='Some stuff', action='store_true')

	register_environment_args(parser, include_prod_confirm=False)
	register_verbose_args(parser)
	register_version_args(parser)
	register_database_args(parser)
	
	args = parser.parse_args()
	
	handle_database_args(args)
	handle_version_args(args)
	handle_verbose_args(args)
	handle_environment_args(args, default = 'STAGE')
	
	return args

def retrieve_partners(env):
	# Partners restrictions
	partner_query = "SELECT id, code FROM offers.Partners;"
	partners = env['db'].execute(partner_query)[0]['result']
	info("Executed Partners query")
	
	out = {}
	for row in partners:
		out[row[1]] = row[0]
	env['partners'] = out

def retrieve_partner_id(env):
	partner_id = env['partner_id']
	env['partner_id'] = None
	while not env['partner_id']:
		try:
			env['partner_id'] = int(partner_id)
			debug('Partner id set to %s' % str(env['partner_id']))
		except (TypeError, ValueError):
			pass
		if isinstance(partner_id, str) and partner_id.upper() in env['partners'].keys():
			env['partner_id'] = env['partners'][partner_id.upper()]
			debug('Partner id set to %s' % str(env['partner_id']))
		found_partner_id = False
		for partner in env['partners']:
			if env['partners'][partner] == env['partner_id']:
				found_partner_id = True
		if not found_partner_id:
			env['partner_id'] = None
		if not env['partner_id']:
			debug('Partner id specified was not found in DB')
			partner_id = input("Enter the partner id or code? ")
	info("Partner id set to: %d" % env['partner_id'])
	
	env['config'] = retrieve_partner_configuration(env)
	env['ticket-prefix'] = ''
	try:
		if env['config']['ticketStock']['requiredForTokenGeneration']:
			env['ticket-prefix'] = env['config']['ticketStock']['numbers'][0]
	except KeyError:
		pass

def retrieve_parameters(env, args):
	set_env_from_args(env, args)
	
	if env['experimental']:
		env['suppress_raw_payload'] = True
	
	if env['troubleshoot'] or env['show_email_links'] or env['list'] or env['generate_token'] or env['live_auction']:
		retrieve_partner_id(env)
		return
	
	graphical_menu(env)

def graphical_menu(env, default_values = None):
	global FLAG_ENVIRONMENT, GRAPHICAL_SAVE_PICKLE_PATH, GRAPHICAL_THEME_CONFIG_PATH, GRAPHICAL_CONFIG, GRAPHICAL_WINDOW, GRAPHICAL_MODULE
	os.environ["TK_SILENCE_DEPRECATION"] = "1"
	try:
		import tkinter
	except ModuleNotFoundError:
		return interactive_menu(env)
	try:
		import PySimpleGUI as sg
	except ModuleNotFoundError as e:
		warning(e)
		warning('Run `sudo pip3 install pysimplegui` to use the graphical menu')
		return interactive_menu(env)
	GRAPHICAL_MODULE = sg
	env['graphical'] = True
	
	sg.LOOK_AND_FEEL_TABLE['LightPink1'] =  {
		'BACKGROUND': '#F3CAE7',
		'TEXT': '#BE23C8',
		'INPUT': '#F1F3F4',
		'TEXT_INPUT': '#202123',
		'SCROLL': '#EEA5F6',
		'BUTTON': ('#FFFFFF', '#DF42D1'),
		'PROGRESS': sg.DEFAULT_PROGRESS_BAR_COLOR,
		'BORDER': 1,
		'SLIDER_DEPTH': 0,
		'PROGRESS_DEPTH': 0,
		'COLOR_LIST': ['#F3CAE7', '#DF42D1', '#EEA5F6', '#BE23C8'], 
	}
	
	GRAPHICAL_CONFIG = {
		'theme': 'DarkBlack',
		'font': 'Menlo 11',
		'alpha': 0.9,
	}
	try:
		conf = json.load(open(GRAPHICAL_THEME_CONFIG_PATH))
		GRAPHICAL_CONFIG['theme'] = conf['theme']
		GRAPHICAL_CONFIG['font'] = conf['font']
		GRAPHICAL_CONFIG['alpha'] = conf['alpha']
	except (KeyError, ValueError, FileNotFoundError):
		pass
				
	
	sg.ChangeLookAndFeel(GRAPHICAL_CONFIG['theme'])
	sg.SetOptions(font=GRAPHICAL_CONFIG['font'])
	
	coalesce_str = lambda x: x if x != None else ''
	coalesce_filter = lambda x: ','.join(x) if x != None else ''
	coalesce_bool = lambda x: x if x != None else False
	
	values = default_values
	if values == None:
		values = {}
		set_graphical_values_from_env(env, values)
		values['environment'] = FLAG_ENVIRONMENT
		values['verbose'] = verbose_level()
	
	layout = [
		[sg.Menu([['File', ['&Quit']], ['View', ['Theme', ['Light Appearance', 'Dark Appearance', 'Pink Appearance', 'Opaque', 'Translucent']]]], tearoff=True)],
		[sg.Column([
			[sg.Frame('', [
				[sg.Column([
					[sg.Text('Partner ID')],
					[sg.Text('Environment')],
					[sg.Text('Instant', pad=((5, 5),(8,5)))],
					[sg.Text('Extra Seat', pad=((5, 5),(8,5)))],
				], size=(120, 110), element_justification='right'),
				sg.Column([
					[sg.InputText(coalesce_str(values['partner_id']), key='partner_id')],
					[sg.Combo(('STAGE', 'PRODUCTION'),	default_value=values['environment'], key='environment')],
					[sg.Checkbox('', default=coalesce_bool(values['instant']), key='instant')],
					[sg.Checkbox('', default=coalesce_bool(values['extra_seat']), key='extra_seat')],
				], size=(120, 110)),
				],
			], pad=((5, 5),(0, 157)), border_width=0)],
			[sg.Frame('Filters', [
				[sg.Column([
					[sg.Text('Fare Class')],
					[sg.Text('Cabin Type')],
					[sg.Text('Origin')],
					[sg.Text('Destination')],
					[sg.Text('Hours Before')],
					[sg.Text('Departure Date')],
					[sg.Text('Equipment')],
					[sg.Text('From Cabin')],
					[sg.Text('Duration')],
					[sg.Text('After')],
				], element_justification='right'),
				sg.Column([
					[sg.InputText(coalesce_filter(values['fare_class']), key='fare_class', tooltip='e.g. Y,PY or *')],
					[sg.InputText(coalesce_filter(values['cabin_type']), key='cabin_type', tooltip='e.g. BUSINESS')],
					[sg.InputText(coalesce_filter(values['origin']), key='origin', tooltip='e.g. YUL,YYZ or *')],
					[sg.InputText(coalesce_filter(values['destination']), key='destination', tooltip='e.g. LGA,YVR or *')],
					[sg.InputText(coalesce_filter(values['hours_before']), key='hours_before', tooltip='e.g. 24')],
					[sg.InputText(coalesce_filter(values['departure_date']), key='departure_date', tooltip='e.g. 1972-01-01')],
					[sg.InputText(coalesce_filter(values['equipment']), key='equipment', tooltip='e.g. 338,388 or ~738 or SY2 or *')],
					[sg.InputText(coalesce_filter(values['from_cabin']), key='from_cabin', tooltip='e.g. BALCONY')],
					[sg.InputText(coalesce_filter(values['duration']), key='duration', tooltip='e.g. 7')],
					[sg.InputText(coalesce_filter(values['after']), key='after', tooltip='e.g. 1972-01-01')],
				], pad=((5, 5),(0, 0)), size=(170, 230))],
			], size=(240,300))],
		], size=(315,540)),
		sg.Column([
			[sg.Frame('Advanced', [
				[sg.Column([
					[sg.Text('Verbose:')],
					[sg.Text('Experimental', pad=((5, 5),(8,5)))],
					[sg.Text('Show Raw Token', pad=((5, 5),(8,5)))],
					[sg.Text('Suppress Payload', pad=((5, 5),(8,5)))],
					[sg.Text('Show All', pad=((5, 5),(8,5)))],
					[sg.Text('Suppress Cruise Details', pad=((5, 5),(8,5)))],
				], element_justification='right'),
				sg.Column([
					[sg.Combo(('TRACE', 'DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'), default_value=values['verbose'], key='verbose')],
					[sg.Checkbox('', default=coalesce_bool(values['experimental']), key='experimental')],
					[sg.Checkbox('', default=coalesce_bool(values['show_raw_token']), key='show_raw_token')],
					[sg.Checkbox('', default=coalesce_bool(values['suppress_raw_payload']), key='suppress_raw_payload')],
					[sg.Checkbox('', default=coalesce_bool(env['show_all']), key='show_all')],
					[sg.Checkbox('', default=coalesce_bool(values['suppress_cruise_details']), key='suppress_cruise_details')],
				])
				],
				[sg.Column([
					[sg.Button('Live Auction', key='live_auction')],
					#[sg.Button('Web Check-in', key='web_check_in')],
				], pad=((10,5),(0, 0))),
				sg.Column([
					[sg.Button('Troubleshoot', key='troubleshoot')],
					[sg.Button('Email Assist', key='show_email_links')],
				], pad=((70,5),(0, 0)))],
				[sg.Text('_'*40,)],
				[sg.Column([
					[sg.Text('Suppress Key:', pad=((5,5),(0, 10)))],
					[sg.Text('Force Key:')],
					[sg.Text('Value:')],
				], size=(100, 80), element_justification='right'),
				sg.Column([
					[sg.InputText('', key='suppress_key', pad=((5,5),(0, 10)))],
					[sg.InputText('', key='force_key')],
					[sg.InputText('', key='force_key_value')],
				], size=(140, 80)),
				sg.Column([
					[sg.Button('+', key='suppress_key_button', pad=((5,5),(0, 10)))],
					[sg.Button('+', key='force_key_button')],
				], size=(30, 80)),
				],
				[sg.Listbox([], key='force_key_list', select_mode=sg.LISTBOX_SELECT_MODE_MULTIPLE, size=(38, 9))],
				[sg.Button('Remove', key='force_key_list_remove', pad=((235,0),(5,5)))],
			], size=(330, 500))],
		], size=(320,540)),
		],
		[sg.Column([
			[sg.Button('Reset', key='reset'), sg.Button('Save', key='save')],
		], size=(495, 45)),
		sg.Column([
			[sg.Button('List', key='list'), sg.Button('Generate', key='generate_token')],
		], size=(130, 45))],
	]
	
	
	window = sg.Window('offerutil.py Ver. %s' % FLAG_VERSION, layout, default_element_size=(30, 1), alpha_channel=GRAPHICAL_CONFIG['alpha'], grab_anywhere=False)
	window.Finalize()
	GRAPHICAL_WINDOW = window

	if os.path.exists(GRAPHICAL_SAVE_PICKLE_PATH) and default_values == None:
		window.LoadFromDisk(GRAPHICAL_SAVE_PICKLE_PATH)
	
	graphical_menu_main_loop(env)

def graphical_menu_main_loop(env):
	global FLAG_ENVIRONMENT, GRAPHICAL_SAVE_PICKLE_PATH, GRAPHICAL_THEME_CONFIG_PATH, GRAPHICAL_CONFIG, GRAPHICAL_WINDOW, GRAPHICAL_POPUP, GRAPHICAL_MODULE
	if not GRAPHICAL_MODULE: return
	sg = GRAPHICAL_MODULE
	window = GRAPHICAL_WINDOW
	
	if GRAPHICAL_POPUP != None:
		GRAPHICAL_POPUP.close()
		GRAPHICAL_POPUP = None
	
	done = False
	while not done:
		event, values = window.read()
		if event == 'reset':
			values = {0: None, 'partner_id': '', 'environment': 'STAGE', 'instant': False, 'extra_seat': False, 'fare_class': '', 'cabin_type': '', 'origin': '', 'destination': '', 'hours_before': '', 'departure_date': '', 'equipment': '', 'from_cabin': '', 'duration': '', 'after': '', 'verbose': 'WARNING', 'experimental': False, 'show_raw_token': False, 'suppress_raw_payload': False, 'show_all': False, 'suppress_cruise_details': False, 'suppress_key': '', 'force_key': '', 'force_key_value': '', 'force_key_list': []}
			window.Fill(values)
		elif event == 'save':
			window.SaveToDisk(GRAPHICAL_SAVE_PICKLE_PATH)
		elif event in ['suppress_key_button', 'force_key_button', 'force_key_list_remove']:
			key = ''
			value = ''
			if event == 'suppress_key_button' and values['suppress_key'] != '':
				key = values['suppress_key']
				value = 'suppressed'
				window.FindElement('suppress_key').update(value='')
			elif event == 'force_key_button' and values['force_key'] != '':
				key = values['force_key']
				value = values['force_key_value']
				window.FindElement('force_key').update(value='')
				window.FindElement('force_key_value').update(value='')
			elif event == 'force_key_list_remove':
				keylist = window.FindElement('force_key_list')
				new_arr = []
				arr = keylist.GetListValues()
				for value in arr:
					if value not in values['force_key_list']:
						new_arr.append(value)
				keylist.Update(values=new_arr)
			if key != '':
				keylist = window.FindElement('force_key_list')
				arr = keylist.GetListValues()
				v = f'{key}: {value}'
				if v not in arr: arr.append(v)
				keylist.Update(values=arr)
		elif event in ['Light Appearance', 'Dark Appearance', 'Pink Appearance', 'Opaque', 'Translucent']:
			theme = GRAPHICAL_CONFIG['theme']
			if event == 'Light Appearance':
				GRAPHICAL_CONFIG['theme'] = 'LightGrey1'
				GRAPHICAL_CONFIG['alpha'] = 1.0
			elif event == 'Dark Appearance':
				GRAPHICAL_CONFIG['theme'] = 'DarkBlack'
				GRAPHICAL_CONFIG['alpha'] = 0.9
			elif event == 'Pink Appearance':
				GRAPHICAL_CONFIG['theme'] = 'LightPink1'
				GRAPHICAL_CONFIG['alpha'] = 1.0
			elif event == 'Opaque':
				GRAPHICAL_CONFIG['alpha'] = 1.0
			elif event == 'Translucent':
				GRAPHICAL_CONFIG['alpha'] = 0.9
			with open(GRAPHICAL_THEME_CONFIG_PATH, 'w') as f:
				json.dump(GRAPHICAL_CONFIG, f)
			window.SetAlpha(GRAPHICAL_CONFIG['alpha'])
			if theme != GRAPHICAL_CONFIG['theme']:
				window.close()
				return graphical_menu(env, values)
				
		else:
			done = True
	
	if event == None:
		window.close()
		exit()
	
	# Process events
	for k in ['generate_token', 'list', 'troubleshoot', 'show_email_links', 'live_auction', 'web_check_in']:
		values[k] = True if event == k else False
	
	values['suppress_key'] = []
	values['force_key'] = []
	for v in window.FindElement('force_key_list').GetListValues():
		key, value = v.split(': ')
		if value == 'suppressed': values['suppress_key'].append(key)
		else: values['force_key'].append([key, value])
	
	set_env_from_graphical_values(env, values)
	GRAPHICAL_POPUP = sg.Window('', [[sg.Text("Processing...")]], alpha_channel=GRAPHICAL_CONFIG['alpha'], grab_anywhere=False)
	GRAPHICAL_POPUP.read(timeout=0)

def set_env_from_args(env, args):
	args_values = vars(args)
	for k in ['partner_id', 'suppress_raw_payload', 'show_raw_token', 'extra_seat', 'after', 'show_all', 'live_auction', 'departure_date', 'hours_before', 'instant', 'experimental', 'suppress_cruise_details', 'generate_token', 'list', 'troubleshoot', 'show_email_links', 'suppress_key', 'force_key']:
		env[k] = args_values[k]
	
	for k in ['fare_class', 'equipment', 'cabin_type', 'origin', 'destination', 'duration', 'from_cabin']:
		env[k] = process_filter(args_values[k])

	try: env['hours_before'] = int(env['hours_before'])
	except (ValueError, TypeError): pass

def set_graphical_values_from_env(env, values):
	for k in ['partner_id', 'suppress_raw_payload', 'show_raw_token', 'extra_seat', 'after', 'show_all', 'departure_date', 'instant', 'experimental', 'suppress_cruise_details', 'generate_token', 'list', 'troubleshoot', 'show_email_links', 'hours_before']:
		values[k] = env[k]
	
	for k in ['fare_class', 'equipment', 'cabin_type', 'origin', 'destination', 'duration', 'from_cabin']:
		values[k] = reverse_filter(env[k])
	

def set_env_from_graphical_values(env, values):
	global FLAG_ENVIRONMENT
	for k in ['partner_id', 'suppress_raw_payload', 'show_raw_token', 'extra_seat', 'after', 'show_all', 'departure_date', 'instant', 'experimental', 'suppress_cruise_details', 'generate_token', 'list', 'troubleshoot', 'show_email_links', 'live_auction']:
		env[k] = values[k]
	
	FLAG_ENVIRONMENT = values['environment']
	set_verbose_level(values['verbose'])
	
	retrieve_partner_id(env)
	
	for k in ['fare_class', 'equipment', 'cabin_type', 'origin', 'destination', 'duration', 'from_cabin']:
		env[k] = process_filter(values[k])
	
	if values['hours_before']:
		try:
			env['hours_before'] = int(values['hours_before'])
		except ValueError:
			warning('Invalid value for hours-before argument')
	

def interactive_menu(env):
	global FLAG_ENVIRONMENT
	
	retrieve_partner_id(env)
	
	if 'partnerType' not in env['config'] or env['config']['partnerType'] == 'airline':
		while not env['list'] and not env['generate_token']:
			print('Current Environment: %s' % FLAG_ENVIRONMENT)
			print('Available actions:')
			print('\t1. List instant upgrade eligible routes')
			print('\t2. List NFS eligible routes')
			print('\t3. Generate instant upgrade token')
			print('\t4. Generate NFS token')
			print('\t5. Generate bidding token')
			print('\t6. Specify options')
			try:
				x = int(input('Select an action? '))
			except ValueError:
				continue
			if x == 1:
				env['list'] = True
				env['instant'] = True
			if x == 2:
				env['list'] = True
				env['instant'] = True
				env['extra_seat'] = True
			if x == 3:
				env['instant'] = True
				env['generate_token'] = True
			if x == 4:
				env['instant'] = True
				env['generate_token'] = True
				env['extra_seat'] = True
			if x == 5:
				env['generate_token'] = True
			if x == 6:
				options_done = False
				while not options_done:
					print('Current Environment: %s' % FLAG_ENVIRONMENT)
					if '*' not in env['fare_class'] and len(env['fare_class']) > 0:
						print('Fare class filter: %s' % ', '.join(env['fare_class']))
					if '*' not in env['cabin_type'] and len(env['cabin_type']) > 0:
						print('Cabin Type: %s' % ', '.join(env['cabin_type']))
					if '*' not in env['equipment'] and len(env['equipment']) > 0:
						print('Equipment: %s' % ', '.join(env['equipment']))
					if '*' not in env['origin'] and len(env['origin']) > 0:
						print('Origin: %s' % ', '.join(env['origin']))
					if '*' not in env['destination'] and len(env['destination']) > 0:
						print('Destination: %s' % ', '.join(env['destination']))
					if env['segment-count'] != 1:
						print('Segment Count: %s' % str(env['segment_count']))
					if env['pax'] != 1:
						print('Pax: %s' % str(env['pax']))
					if env['hours_before'] != 24:
						print('Hours before: %s' % env['hours_before'])
					if env['segment_count'] != 1:
						print('Segment count: %s' % env['segment_count'])
					print('Available options:')
					print('\t1. Toggle environment')
					print('\t2. Filter for fare classes (Ex. Y,PY or *)')
					print('\t3. Filter for cabin type (Ex. ECONOMY)')
					print('\t4. Filter for equipment type (Ex. 338,388 or *)')
					print('\t5. Specify origin (e.g. YUL,YYZ or *)')
					print('\t6. Specify destination (e.g. LGA,YVR or *)')
					print('\t7. Select hours before departure')
					print('\t8. Specify the number of segments')
					print('\t9. Specify the number of pax')
					print('\t0. Return to previous menu')
					try:
						x = int(input('Select an action? '))
					except ValueError:
						continue
					
					if x == 1:
						if FLAG_ENVIRONMENT == 'STAGE':
							FLAG_ENVIRONMENT = 'PRODUCTION'
						else:
							FLAG_ENVIRONMENT = 'STAGE'
					if x == 2:
						s = input('Enter a fare class filter? ')
						env['fare_class'] = process_filter(s)
					if x == 3:
						s = input('Enter a cabin type? ')
						env['cabin_type'] = process_filter(s)
					if x == 4:
						s = input('Enter a equipment type? ')
						env['equipment'] = process_filter(s)
					if x == 5:
						s = input('Enter origin(s)? ')
						env['origin'] = process_filter(s)
					if x == 6:
						s = input('Enter destination? ')
						env['destination'] = process_filter(s)
					if x == 7:
						env['hours_before'] = input('Enter hours before departure? ')
					if x == 8:
						env['segment_count'] = input('Enter number of segments? ')
					if x == 9:
						env['pax'] = input('Enter number of pax? ')
					if x == 0:
						options_done = True
	elif env['config']['partnerType'] == 'cruise':
		while not env['list'] and not env['generate_token']:
			print('Current Environment: %s' % FLAG_ENVIRONMENT)
			print('Available actions:')
			print('\t1. List cruises')
			print('\t2. Generate token')
			print('\t3. Specify options')
			try:
				x = int(input('Select an action? '))
			except ValueError:
				continue
			if x == 1:
				env['list'] = True
			if x == 2:
				env['generate_token'] = True
			if x == 3:
				options_done = False
				while not options_done:
					print('Current Environment: %s' % FLAG_ENVIRONMENT)
					if '*' not in env['duration'] and len(env['duration']) > 0:
						print('Duration: %s' % ', '.join(env['duration']))
					if '*' not in env['from_cabin'] and len(env['from_cabin']) > 0:
						print('From Cabin: %s' % ', '.join(env['from_cabin']))
					if '*' not in env['equipment'] and len(env['equipment']) > 0:
						print('Equipment: %s' % ', '.join(env['equipment']))
					print('Available options:')
					print('\t1. Toggle environment')
					print('\t2. Select duration in days for cruise (Ex. 7,14)')
					print('\t3. Select from cabin for cruise (Ex. BALCONY)')
					print('\t4. Filter for equipment (Ex. SUN,EXP or *)')
					print('\t5. Restrict to cruises after date (e.g. 2030-01-01)')
					print('\t6. Return to previous menu')
					try:
						x = int(input('Select an action? '))
					except ValueError:
						continue
					if x == 1:
						if FLAG_ENVIRONMENT == 'STAGE':
							FLAG_ENVIRONMENT = 'PRODUCTION'
						else:
							FLAG_ENVIRONMENT = 'STAGE'
					if x == 2:
						s = input('Enter duration in days? ')
						env['duration'] = process_filter(s)
					if x == 3:
						s = input('Enter from cabin? ')
						env['from_cabin'] = process_filter(s)
					if x == 4:
						s = input('Enter a equipment? ')
						env['equipment'] = process_filter(s)
					if x == 5:
						env['after'] = input('Enter after date? ')
					if x == 6:
						options_done = True

def retrieve_prices(env):
	table = 'ps'
	instant_upgrade_string = ''
	extra_seat_string = ''
	if env['instant']:
		table = 'ups'
		instant_upgrade_string = ' AND fcr.instantupgrade_eligible = 1'
	
	route_query_departure_date = ' AND fi.last_flight > NOW() AND fi.first_flight < DATE_ADD(NOW(), INTERVAL 6 MONTH)'
	if env['departure_date']:
		
		route_query_departure_date = ' AND fi.last_flight > %s AND fi.first_flight < %s' % (repr(env['departure_date']), repr(env['departure_date']))
	
	fare_class_query = "SELECT DISTINCT fcr.fare_class, fcd.cabin_type, fcr.upgrade_type FROM offers.FareClassRules AS fcr LEFT JOIN offers.FareClassDef AS fcd ON fcr.fare_class = fcd.fare_class AND fcr.partner_id = fcd.partner_id WHERE fcr.partner_id = %d AND fcr.eligible = 1%s;"
	
	if env['instant']:
		departure_date_string = ' AND ups.end_date > NOW() AND ups.start_date < DATE_ADD(NOW(), INTERVAL 6 MONTH)'
		if env['departure_date']:
		
			departure_date_string = ' AND ups.end_date > %s AND ups.start_date < %s' % (repr(env['departure_date']), repr(env['departure_date']))
		
		extra_seat_string = ' AND ups.upgrade_type != "EXTRA_SEAT"'
		if env['extra_seat']:
		
			extra_seat_string = ' AND ups.upgrade_type = "EXTRA_SEAT"'

		price_query = "SELECT DISTINCT ups.from_cabin, ups.upgrade_type, ups.fare_class, ups.origin, ups.dest FROM offers.UpgradePriceSettings AS ups LEFT JOIN offers.ProductSettings AS ps ON ups.partner_id = ps.partner_id AND ups.from_cabin = ps.from_cabin AND ups.upgrade_type = ps.upgrade_type AND ups.origin = ps.origin AND ups.dest = ps.dest WHERE ups.partner_id = %d AND ups.enabled = 1%s%s%s%s;"
	else:
		departure_date_string = ' AND end_date > NOW() AND start_date < DATE_ADD(NOW(), INTERVAL 6 MONTH)'
		if env['departure_date']:
		
			departure_date_string = ' AND end_date > %s AND start_date < %s' % (repr(env['departure_date']), repr(env['departure_date']))
		price_query = "SELECT DISTINCT from_cabin, upgrade_type, fare_class, origin, dest FROM offers.ProductSettings AS ps WHERE partner_id = %d AND product_id = 0%s%s%s%s;"
	
	origin_string = ''
	if '*' not in env['origin'] and len(env['origin']) > 0:
		origin_string = ' AND %s.origin IN (%s)' % (table, ','.join([repr(x) for x in env['origin']]))
		debug('retrieve_prices: Origin string set to %s' % origin_string)
	
	destination_string = ''
	if '*' not in env['destination'] and len(env['destination']) > 0:
		destination_string = ' AND %s.dest IN (%s)' % (table, ','.join([repr(x) for x in env['destination']]))
	
	fare_classes = env['db'].execute(fare_class_query % (env['partner_id'], instant_upgrade_string))[0]['result']
	info("Executed FareClass query")
	
	# Custom Fare Class Def
	fare_classes = retrieve_custom_fare_classes(env, fare_classes)
	
	prices = env['db'].execute(price_query % (env['partner_id'], departure_date_string, origin_string, destination_string, extra_seat_string))[0]['result']
	info("Executed Prices query")

	filtered_prices = []
	
	if len(fare_classes) < 1:
		warning('No eligible fare classes for upgrade')
		return []
	
	if len(prices) < 1:
		warning('No eligible prices for upgrade')
		return []
	
	info("Retriving prices eligible for upgrade")
	for fare_class in fare_classes:
		for price in prices:
			if (price[2] == '*' or price[2] == fare_class[0]) and (price[0] == '*' or price[0] == fare_class[1]) and (price[1] == '*' or price[1] == fare_class[2]):
				filtered_prices.append([fare_class[0], fare_class[1], price[3], price[4]])
	
	if len(filtered_prices) < 1:
		warning('No eligible prices after filtering for fare classes')
		return []
	
	return filtered_prices

def retrieve_custom_fare_classes(env, fare_classes):
	global CUSTOM_FARE_CLASS
	if env['partner_id'] not in CUSTOM_FARE_CLASS.keys():
		return fare_classes
	
	warning('Custom Fare Classes are not fully implemented')
	
	config = CUSTOM_FARE_CLASS[env['partner_id']]['config']
	values = CUSTOM_FARE_CLASS[env['partner_id']]['values']
	
	if '*' in env['fare_class']:
		env['fare_class'] = [config['default']]
	
	for i in range(len(fare_classes)):
		cabin_type = None
		for x in config['priority']:
			try:
				cabin_type = values[x][fare_classes[i][0]]
			except KeyError:
				pass
		fare_classes[i] = (fare_classes[i][0], cabin_type, fare_classes[i][2])
	info('Loaded custom fare classes: %s' % str(fare_classes))
	return fare_classes
	

def filter_eligible_routes(env, filtered_prices):
	instant_upgrade_string = ''
	if env['instant']:
		instant_upgrade_string = " AND (r.instantupgrade_eligibility='ALL' OR (r.instantupgrade_eligibility='PARTIAL' AND fi.instantupgrade_eligibility='T'))"
		
	departure_date_string = ' AND fi.last_flight > NOW() AND fi.first_flight < DATE_ADD(NOW(), INTERVAL 6 MONTH)'
	if env['departure_date']:
		
		departure_date_string = ' AND fi.last_flight > %s AND fi.first_flight < %s' % (repr(env['departure_date']), repr(env['departure_date']))
	
	route_query = "SELECT DISTINCT fi.flight_num, fi.origin, fi.dest, fi.airline FROM offers.FlightInventory AS fi LEFT JOIN offers.Route AS r ON fi.partner_id = r.partner_id AND fi.origin = r.origin AND fi.dest = r.dest WHERE fi.partner_id = %d AND fi.cancelled = 0 AND (r.eligibility = 'ALL' OR (r.eligibility = 'PARTIAL' AND fi.eligibility = 'T'))%s%s;"

	routes = env['db'].execute(route_query % (env['partner_id'], instant_upgrade_string, departure_date_string))[0]['result']
	info("Executed Route query")
	
	filtered_routes = {}
	excluded_routes = {}
	
	if len(routes) < 1:
		warning('No eligible routes for upgrade')
		return []
	
	info("Filtering prices with eligible routes for upgrade")
	for price in filtered_prices:
		price_added = False
		if price[1] not in env['cabin_type'] and '*' not in env['cabin_type']:
			continue
		if price[1] not in filtered_routes.keys():
			filtered_routes[price[1]] = {}
		for route in routes:
			if (price[2] == '*' or route[1] == price[2]) and (price[3] == '*' or route[2] == price[3]):
				s = '%s-%s' % (route[1], route[2])
				if route[0] not in filtered_routes[price[1]].keys():
					filtered_routes[price[1]][route[0]] = {}
				
				if s not in filtered_routes[price[1]][route[0]].keys():
					filtered_routes[price[1]][route[0]][s] = []
				
				if price[0] in env['fare_class'] or '*' in env['fare_class']:
					if price[0] not in filtered_routes[price[1]][route[0]][s]:
						filtered_routes[price[1]][route[0]][s].append(price[0])
						price_added = True
		s = '%s-%s' % (price[2], price[3])
		if not price_added and '*' not in s:
			if price[1] not in excluded_routes.keys():
				excluded_routes[price[1]] = {}
			if s not in excluded_routes[price[1]].keys():
				excluded_routes[price[1]][s] = []
			if price[0] not in excluded_routes[price[1]][s]:
				excluded_routes[price[1]][s].append(price[0])
			if s in excluded_routes[price[1]].keys():
				for flight in filtered_routes[price[1]]:
					if s in filtered_routes[price[1]][flight].keys():
						try:
							del excluded_routes[price[1]][s]
						except KeyError:
							pass
	
	info("Prices excluded due to no eligible flight")
	for cabin_type in excluded_routes:
		info('%s:' % cabin_type)
		info('\t\tRoute\tFare Classes')
		routes = list(excluded_routes[cabin_type].keys())
		routes.sort()
		for route in routes:
			info('\t\t%s\t%s' % (route, ','.join(excluded_routes[cabin_type][route])))
	
	if len(filtered_routes) < 1:
		warning('No eligible routes after filtering for prices')
		return []
	
	return filtered_routes

def retrieve_flights(env):
	departure_date_string = ' AND last_flight > NOW() AND first_flight < DATE_ADD(NOW(), INTERVAL 6 MONTH)'
	if env['departure_date']:
		
		departure_date_string = ' AND fi.last_flight > %s AND fi.first_flight < %s' % (repr(env['departure_date']), repr(env['departure_date']))
		
	flight_query = "SELECT DISTINCT fi.flight_num, fv.equip FROM offers.FlightInventory AS fi LEFT JOIN offers.FlightInvVariation AS fv ON fi.ID = fv.product_id WHERE fi.partner_id = %s AND fi.cancelled = 0%s ORDER BY fi.flight_num;"
	results = env['db'].execute(flight_query % (env['partner_id'], departure_date_string))[0]['result']
	info("Executed Flight query")
	
	flights = {}
	for row in results:
		if row[0] not in flights.keys():
			flights[row[0]] = []
		flights[row[0]].append(row[1])
	return flights

def expand_restrictions(restrictions, routes):
	expanded_restrictions = []
	cabin_type_catch_all = False
	for row in restrictions:
		if row[1] == '*':
			for cabin_type in routes:
				expanded_restrictions.append((row[0], cabin_type, row[2], row[3], row[4], row[5]))
		else:
			expanded_restrictions.append(row)
	if cabin_type_catch_all:
		info('Expanding catch-all restriction for cabin type')
	restrictions = expanded_restrictions
	expanded_restrictions = []
	upgrade_type_catch_all = False
	for row in restrictions:
		if row[2] == '*':
			for cabin_type in routes:
				expanded_restrictions.append((row[0], row[1], cabin_type, row[3], row[4], row[5]))
				upgrade_type_catch_all = True
		else:
			expanded_restrictions.append(row)
	if upgrade_type_catch_all:
		info('Expanding catch-all restriction for upgrade type')
	restrictions = expanded_restrictions
	expanded_restrictions = []
	origin_catch_all = False
	for row in restrictions:
		if row[4] == '*':
			for flight in routes[row[1]]:
				for route in routes[row[1]][flight]:
					origin, dest = route.split('-')
					expanded_restrictions.append((row[0], row[1], row[2], row[3], origin, row[5]))
					origin_catch_all = True
		else:
			expanded_restrictions.append(row)
	if origin_catch_all:
		info('Expanding catch-all restriction for origin')
	restrictions = expanded_restrictions
	expanded_restrictions = []
	destination_catch_all = False
	for row in restrictions:
		if row[5] == '*':
			for flight in routes[row[1]]:
				for route in routes[row[1]][flight]:
					origin, dest = route.split('-')
					expanded_restrictions.append((row[0], row[1], row[2], row[3], row[4], dest))
					destination_catch_all = True
		else:
			expanded_restrictions.append(row)
	if destination_catch_all:
		info('Expanding catch-all restriction for destination')
	return expanded_restrictions
		
def filter_restrictions(env, filtered_routes):
	# Fare class restrictions
	restriction_query = "SELECT DISTINCT exclusive,from_cabin,upgrade_type,fare_class,origin,dest FROM offers.FareClassEligibilityRestrictions WHERE partner_id = %s AND end_date > NOW() AND start_date < DATE_ADD(NOW(), INTERVAL 6 MONTH);"

	restrictions = env['db'].execute(restriction_query % env['partner_id'])[0]['result']
	info("Executed Restrictions query")
	
	routes = filtered_routes
	restriction_filter = {}
	info("Filtering routes with restrictions for upgrade")
	
	restrictions = expand_restrictions(restrictions, routes)
	
	for row in restrictions:
		route = '%s-%s' % (row[4], row[5])
		fare_class = row[3]
		if row[0] == 1:
			for cabin_type in filtered_routes:
				if cabin_type == row[1] or row[1] == '*':
					for flight in filtered_routes[cabin_type]:
						if route not in filtered_routes[cabin_type][flight]:
							continue
						if fare_class not in filtered_routes[cabin_type][flight][route]:
							continue
						routes[cabin_type][flight][route].remove(fare_class)
						info("Route %s (flight %s), fare class %s (cabin_type '%s'): Excluded (with exclusive=1)" % (route, flight, fare_class, cabin_type))
		elif row[0] == 0:
			if row[2] not in restriction_filter.keys():
				restriction_filter[row[2]] = []
			restriction_filter[row[2]].append(route)
	
	for cabin_type in filtered_routes:
		for flight in filtered_routes[cabin_type]:
			for route in filtered_routes[cabin_type][flight]:
				for fare_class in filtered_routes[cabin_type][flight][route]:
					if fare_class not in restriction_filter.keys():
						continue
					if route not in restriction_filter[fare_class]:
						routes[cabin_type][flight][route].remove(fare_class)
						info("Route %s (flight %s), fare class %s (cabin_type '%s'): Excluded (with exclusive=0)" % (route, flight, fare_class, cabin_type))
	
	if len(routes) < 1:
		warning('No eligible routes after filtering for restrictions for upgrade')
		return []
	
	return routes

def filter_equipments(env, routes):
	env['flights'] = retrieve_flights(env)

	info("Filtering routes by equipment")
	for equip in env['equipment']:
		if equip == '*':
			info('No equipment filter set')
			return routes

	filtered_routes = {}
	for cabin_type in routes:
		for flight in routes[cabin_type]:
			flight_added = False
			for equip in env['equipment']:
				if equip[0] == '~':
					if equip[1:] not in env['flights'][flight]:
						if cabin_type not in filtered_routes.keys():
							filtered_routes[cabin_type] = {}
						filtered_routes[cabin_type][flight] = routes[cabin_type][flight]
						flight_added = True
						break
				else:
					if equip in env['flights'][flight]:
						if cabin_type not in filtered_routes.keys():
							filtered_routes[cabin_type] = {}
						filtered_routes[cabin_type][flight] = routes[cabin_type][flight]
						flight_added = True
						break
			if not flight_added:
				info("Flight %s (cabin_type '%s'): Excluded due to equipment restriction" % (flight, cabin_type))
	return filtered_routes
		
	

def filter_extra_seat(env, routes):
	combination_query = "SELECT 1 FROM offers.UpgradeCombinations WHERE partner_id = %s AND from_cabin = 'ECONOMY' AND to_cabin = 'EXTRA_SEAT';"
	combinations = env['db'].execute(combination_query % env['partner_id'])[0]['result']
	info("Executed Combinations query")
		
	if len(combinations) < 1:
		warning("The combination ECONOMY to EXTRA_SEAT is missing from UpgradeCombinations")
		return []
	
	extra_seat_eligibility_query = "SELECT DISTINCT origin, dest FROM offers.CabinTypeUpgradeEligibility WHERE partner_id = %s AND from_cabin='ECONOMY' AND to_cabin='EXTRA_SEAT' AND eligible = 1;"
	extra_seat_routes = env['db'].execute(extra_seat_eligibility_query % env['partner_id'])[0]['result']
	info("Executed Extra Seat Eligibility query")
	
	filtered_routes = {}
	info("Filtering routes with extra seat")
	for cabin_type in routes:
		for flight in routes[cabin_type]:
			flight_added = False
			for route in routes[cabin_type][flight]:
				catch_all_filter = False
				for row in extra_seat_routes:
					s = '%s-%s' % (row[0], row[1])
					if s == '*-*':
						catch_all_filter = True
						continue
					if route != s:
						continue
					if cabin_type not in filtered_routes.keys():
						filtered_routes[cabin_type] = {}
					if flight not in filtered_routes[cabin_type].keys():
						filtered_routes[cabin_type][flight] = {}
					filtered_routes[cabin_type][flight][route] = routes[cabin_type][flight][route]
					flight_added = True
					#info("Route %s (flight %s) cabin_type %s: Eligible Extra Seat" % (route, flight, cabin_type))
				if catch_all_filter:
					if cabin_type not in filtered_routes.keys():
						filtered_routes[cabin_type] = {}
					if flight not in filtered_routes[cabin_type].keys():
						filtered_routes[cabin_type][flight] = {}
					filtered_routes[cabin_type][flight][route] = routes[cabin_type][flight][route]
					flight_added = True
					#info("Route %s (flight %s) cabin_type %s: Catch-all Eligible Extra Seat" % (route, flight, cabin_type))
			if not flight_added:
				info("Route %s (flight %s) cabin_type %s: Excluded due to extra seat eligibility" % (route, flight, cabin_type))
	
	return filtered_routes

def check_rules_cutoff_v2(env):
	if 'offerCutoffRulesV2' not in env['config'].keys() or not env['config']['offerCutoffRulesV2']:
		return True
	rules_cutoff_query = "SELECT rd.hours_before, ra.attr_type, ra.attr_value FROM offers.RuleData AS rd LEFT JOIN offers.RuleAttribute AS ra ON rd.id = ra.rule_id WHERE rd.partner_id = %s AND rd.rule_type = 'OFFER_SUBMISSION';"
	
	rules_cutoff = env['db'].execute(rules_cutoff_query % env['partner_id'])[0]['result']
	info("Executed Rules Cutoff v2 query")
	
	default_cutoff = None
	extra_seat_cutoff = None
	
	for rule in rules_cutoff:
		if rule[1] == 'DEFAULT':
			default_cutoff = rule[0]
		if rule[1] == 'UPGRADE_TYPE' and rule[2] == 'EXTRA_SEAT':
			extra_seat_cutoff = rule[0]
	
	if not extra_seat_cutoff:
		extra_seat_cutoff = default_cutoff
	
	cutoff = default_cutoff
	if env['extra_seat']:
		cutoff = extra_seat_cutoff
	
	if not cutoff:
		return True
	
	while cutoff > env['hours_before']:
		try:
			env['hours_before'] = env['config']['products']['instantUpgrade']['eligibleDaysBeforeStart']*24 - 1
			if cutoff > env['hours_before']:
				warning('Incoherent cutoff v2 rules')
				return True
		except KeyError:
			env['hours_before'] = float(cutoff) + 1
	
	return True

def retrieve_cruises(env):
	after_string = ''
	if env['after']:
		after_string = ' AND eff_dt > %s' % repr(env['after'])
	duration_string = ''
	if '*' not in env['duration'] and len(env['duration']) > 0:
		duration_string = ' AND fv.duration IN (%s)' % (','.join([repr(str(int(x)*24*60)) for x in env['duration']]))
	equip_string = ''
	if '*' not in env['equipment'] and len(env['equipment']) > 0:
		equip_string = ' AND equip IN (%s)' % (','.join([repr(x) for x in env['equipment']]))
	origin_string = ''
	if '*' not in env['origin'] and len(env['origin']) > 0:
		origin_string = ' AND origin IN (%s)' % (','.join([repr(x) for x in env['origin']]))
	destination_string = ''
	if '*' not in env['destination'] and len(env['destination']) > 0:
		origin_string = ' AND dest IN (%s)' % (','.join([repr(x) for x in env['destination']]))
	
	cruise_query = "SELECT flight_num AS sail_id, origin, dest, eff_dt, end_dt, equip, fv.duration FROM offers.FlightInventory AS fi JOIN offers.FlightInvVariation AS fv ON fi.id=fv.product_id WHERE partner_id = %s AND eligibility = 'T' AND end_dt > NOW() AND eff_dt < DATE_ADD(NOW(), INTERVAL 2 YEAR)%s%s%s%s%s;"
	results = env['db'].execute(cruise_query % (env['partner_id'], after_string, duration_string, equip_string, origin_string, destination_string))[0]['result']
	info("Executed Cruises query")
	
	if len(results) < 1:
		warning('No cruises match specified criteria')
		return {}
	
	ship_code_string = ''
	if '*' not in env['equipment'] and len(env['equipment']) > 0:
		f = []
		f_ex = []
		ship_code_string = ''
		for x in env['equipment']:
			if x[0] == '~':
				f_ex.append(repr(x[1:]))
			else:
				f.append(repr(x))
		if len(f) > 0:
			ship_code_string += ' AND ship_code IN (%s)' % (','.join(f))
		if len(f_ex) > 0:
			ship_code_string += ' AND ship_code NOT IN (%s)' % (','.join(f_ex))
	cabin_type_string = ''
	if '*' not in env['from_cabin'] and len(env['from_cabin']) > 0:
		cabin_type_string = ' AND cabin_type IN (%s)' % (','.join([repr(x) for x in env['from_cabin']]))
	
	mapping_query = "SELECT DISTINCT ship_code, fare_class, cabin_type FROM offers.CruiseShipFareClassMappings WHERE partner_id = %s%s%s;"
	mapping_results = env['db'].execute(mapping_query % (env['partner_id'], ship_code_string, cabin_type_string))[0]['result']
	info("Executed Mappings query")
	
	cruises = {}
	for row in results:
		if row[5] not in cruises.keys():
			cruises[row[5]] = {
				'mappings': {},
				'durations': {},
				'combinations': {}
			}
		if row[3] != row[4]:
			warning('Detected anomaly in Cruise Inventory eff_dt != end_dt')
			continue
		departureDate = row[3].isoformat()
		# Duration is stored in minutes, convert to days
		duration = int(int(row[6])/24/60)
		if duration not in cruises[row[5]]['durations'].keys():
			cruises[row[5]]['durations'][duration]  = []
		cruises[row[5]]['durations'][duration].append([departureDate, row[0]])
	
	for row in mapping_results:
		if row[0] not in cruises:
			continue
		if row[2] not in cruises[row[0]]['mappings']:
			cruises[row[0]]['mappings'][row[2]] = []
		if row[2] == env['from_cabin']:
			found_from_cabin = True
		cruises[row[0]]['mappings'][row[2]].append(row[1])
	
	from_cabin_string = ''
	if '*' not in env['from_cabin'] and len(env['from_cabin']) > 0:
		from_cabin_string = ' AND from_cabin IN (%s)' % (','.join([repr(x) for x in env['from_cabin']]))
	
	combination_query = "SELECT DISTINCT ship_code, from_cabin, to_cabin FROM offers.CruiseShipUpgradeCombinations WHERE partner_id = %s%s%s;"
	combination_results = env['db'].execute(combination_query % (env['partner_id'], ship_code_string, from_cabin_string))[0]['result']
	info("Executed Cruise combinations query")

	for row in combination_results:
		if row[0] not in cruises:
			continue
		if row[1] not in cruises[row[0]]['combinations']:
			cruises[row[0]]['combinations'][row[1]] = []
		cruises[row[0]]['combinations'][row[1]].append(row[2])
		
	return cruises

def process_forced_payload_values(env, payload):
	if env['force_key'] != None:
		for x in env['force_key']:
			payload[x[0]] = x[1]
	if env['suppress_key']:
		for x in env['suppress_key']:
			if x in payload.keys():
				del payload[x]
	return payload

def retrieve_minimal_payload(env, apiKeys = ['automation_tests', 'pre_travel_email']):
	global REQUESTS_ENV, FLAG_ENVIRONMENT
	base_payload = {}
	base_payload['partnerId'] = env['partner_id']
	base_payload['environment'] = REQUESTS_ENV[FLAG_ENVIRONMENT]['TOKEN_GENERATOR_ENV']
	while 'apiKey' not in base_payload.keys():
		if len(apiKeys) < 1:
			error('No matching apiKey was not found.')
			return
		x = apiKeys.pop(0)
		
		for apiKey in env['config']['request']['profile']:
			if env['config']['request']['profile'][apiKey]['profileName'] == x:
				base_payload['apiKey'] = apiKey
	if 'ticket-prefix' in env.keys() and len(env['ticket-prefix']) > 0:
		base_payload['ticketPrefix'] = env['ticket-prefix']
	return base_payload

def call_token_generator(env, payload = None):
	global REQUESTS_ENV, FLAG_ENVIRONMENT
	
	if not payload:
		payload = retrieve_minimal_payload(env)
	
	url = '%s/token/createTestCase' % REQUESTS_ENV[FLAG_ENVIRONMENT]['TOKEN_GENERATOR']
	info("Sending POST request")
	debug("url = %s" % url)
	debug("payload = %s" % json.dumps(payload, indent=2))
	response = requests.post(url, headers={'Content-Type': 'application/json;charset=UTF-8'}, data=json.dumps(payload))
	info("Received response")
	debug("response = %s" % response)
	json_response = None
	try:
		json_response = response.json()
	except ValueError:
		pass
	return json_response
	


def generate_cruise_token(env, cruises):
	global REQUESTS_ENV, FLAG_ENVIRONMENT
	
	print('='*32, 'TOKEN GENERATOR', '='*31)
	
	info("Starting to generate token")
	base_payload = retrieve_minimal_payload(env, ['pre_travel_email'])
	if 'currencyCode' in env['config'].keys():
		base_payload['currency'] = env['config']['currencyCode']
	base_payload['language'] = 'en'
	base_payload['pointOfSale'] = env['config']['defaultPointOfSale']
	base_payload['segmentCount'] = env['segment_count']
	base_payload['pax'] = env['pax']
	
	equip_list = list(cruises.keys())

	done = False
	while not done:
		equip_list = list(cruises.keys())
		if len(equip_list) < 1:
			error('There is no eligible equipment to choose from')
			done = True
			continue
		equip = equip_list[randint(0, len(equip_list)-1)]
		if '*' in env['from_cabin']:
			from_cabin = 'BALCONY'
		else:
			from_cabin_list = env['from_cabin']
			from_cabin = from_cabin_list[randint(0, len(from_cabin_list)-1)]
		base_payload['equipment'] = equip
		if from_cabin and from_cabin not in cruises[equip]['combinations'].keys():
			error('Specified `%s` has no eligible upgrade type for equipment `%s`' % (from_cabin, equip) )
			return
		if '*' not in env['fare_class']:
			fare_class = env['fare_class'][randint(0, len(env['fare_class'])-1)]
			base_payload['fareClass'] = fare_class
		elif from_cabin in cruises[equip]['mappings'].keys():
			cabin_list = cruises[equip]['mappings'][from_cabin]
			fare_class = cabin_list[randint(0, len(cabin_list)-1)]
			base_payload['fareClass'] = fare_class
		else:
			base_payload['fareClass'] = cruises[equip]['mappings']['BALCONY'][0]
		
		durations_list = list(cruises[equip]['durations'].keys())
		duration = durations_list[randint(0, len(durations_list)-1)]
		flight = cruises[equip]['durations'][duration][0]
		base_payload['departureDate'] = flight[0]
		base_payload['flightNumber'] = flight[1]
		
		base_payload = process_forced_payload_values(env, base_payload)
		
		
		if not env['suppress_raw_payload']:
			print('\n', '='*21, 'PAYLOAD', '='*20)
			print(json.dumps(base_payload, sort_keys=True, indent=2))
		
		print('\n', '='*20, 'RESPONSE', '='*20)
		json_response = call_token_generator(env, base_payload)
		if isinstance(json_response, list):
			if env['show_raw_token']:
				print('='*22, 'TOKEN', '='*21)
				pprint(json_response)
			print('\n', '='*15, 'OFFER URL', '='*14)
			print(json_response[0]['offerFlowRequestTokenResponse']['offerUrl'])
			done = True
		elif not json_response:
			error('No response from POST request. Retrying...')
		else:
			print('\n', '='*17, 'ERROR', '='*16)
			pprint(json_response)
	print('')

# curl -X POST https://ws-request.stg.internal.plusgrade.com:443/request/41a1e4cdFZ/offer/create \
#--header 'Content-Type: application/json' \
#--header 'Accept: application/json' \
#--data-raw '{"flights":[{"carrierCode":"FZ","farePaid":"100.10","origination":"HBE","destination":"DXB","flightNumber":"176","departureTime":"20:55","departureDate":"2020-05-21","fareClass":"Y","upgradeType":"business"}],"tickets":[{"ticketNumber":"12345678901111","firstName":"Sam","lastName":"Test","primaryTraveller":true}],"partnerId":"41a1e4cdFZ","firstName":"Sam","lastName":"Test","pax":"1","currency":"USD","pnr":"A1A1A1","email":"astro.thedog@plusgrade.com","language":"en","origPayType":"CC","pointOfSale":"US","totalFarePaid":"200.05","infant":false,"apiKey":"Y7npwIHEBVEdaIeutyntqKGw"}'

def experimental_call_request_service(env, payload = None):
	global REQUESTS_ENV, FLAG_ENVIRONMENT
	
	if not payload:
		payload = experimental_retrieve_minimal_payload(env)
	
	url = '%s/request/%s/offer/create' % (REQUESTS_ENV[FLAG_ENVIRONMENT]['REQUEST_SERVICE'], env['config']['pid'])
	info("Sending POST request")
	debug("url = %s" % url)
	debug("payload = %s" % json.dumps(payload, indent=2))
	response = requests.post(url, headers={'Content-Type': 'application/json;charset=UTF-8', 'Accept': 'application/json'}, data=json.dumps(payload))
	info("Received response")
	debug("response = %s" % response)
	json_response = None
	try:
		json_response = response.json()
	except ValueError:
		pass
	return json_response

def experimental_retrieve_minimal_payload(env, apiKeys = ['automation_tests', 'pre_travel_email']):
	global REQUESTS_ENV, FLAG_ENVIRONMENT
	
	pnr = ''.join([choice('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') for i in range(6)])
	base_payload = {
		"flights": [],
		"tickets": [],
		"partnerId": env['config']['pid'],
		"firstName": "PLUSGRADEFIRST",
		"lastName": "PLUSGRADELAST",
		"pax": "1",
		"currency": "USD",
		"pnr": pnr,
		"email": "astro.thedog@plusgrade.com",
		"language": "en",
		"origPayType": "CC",
		"pointOfSale": "US",
		"totalFarePaid": "200.05",
		"infant": False,
	}
	while 'apiKey' not in base_payload.keys():
		if len(apiKeys) < 1:
			error('No matching apiKey was not found.')
			return
		x = apiKeys.pop(0)
		
		for apiKey in env['config']['request']['profile']:
			if env['config']['request']['profile'][apiKey]['profileName'] == x:
				base_payload['apiKey'] = apiKey
	return base_payload


def experimental_generate_token(env, filtered_routes):
	if len(filtered_routes) < 1:
		return
	
	print('='*32, 'REQUEST SERVICE', '='*31)
	
	info("Starting to generate token")
	cabin_type = env['cabin_type']
	partner_id = env['partner_id']
	if '*' in cabin_type:
		cabin_type = ['ECONOMY']
	elif len(cabin_type) > 1:
		cabin_type = [cabin_type[0]]
	cabin_type = cabin_type[0]
	info("Selected Cabin Type %s" % cabin_type)
	base_payload = experimental_retrieve_minimal_payload(env, ['web_check_in', 'pre_travel_email'])
	if 'currencyCode' in env['config'].keys():
		base_payload['currency'] = env['config']['currencyCode']
	else:
		base_payload['currency'] = 'USD'
	base_payload['language'] = 'en'
	base_payload['pointOfSale'] = env['config']['defaultPointOfSale']

	
	pax = 1
	try:
		pax = int(env['pax'])
	except ValueError:
		pass
	for i in range(pax):
		primary = True
		if i > 0: primary = False
		ticket_num = '%s%s' % (env['ticket-prefix'], str(randint(100000000, 999999999)))
		ticket = {
			"ticketNumber": ticket_num,
			"firstName": "PLUSGRADEFIRST",
			"lastName": "PLUSGRADELAST",
			"primaryTraveller": primary
		}
		base_payload['tickets'].append(ticket)
	
	done = False
	while not done:
		if env['departure_date']:
			departureDate = env['departure_date']
		elif env['instant']:
			departureDate = (datetime.date.today() + datetime.timedelta(hours=env['hours_before'])).isoformat()
		else:
			departureDate = (datetime.date.today() + datetime.timedelta(days=randint(2, 160))).isoformat()
		random_flight = None
		while not random_flight:
			flights = list(filtered_routes[cabin_type].keys())
			info("Selecting random flight %s" % flights)
			if len(flights) < 1:
				error('No flights were eligible for upgrade.')
				return
			i = randint(0, len(flights)-1)
			info("Testing with flight %s" % str(flights[i]))
			routes = list(filtered_routes[cabin_type][flights[i]].keys())
			j = randint(0, len(routes)-1)
			filtered_fare_class = []
			fc = filtered_routes[cabin_type][flights[i]][routes[j]]
			fc2 = env['fare_class']
			if '*' in fc2:
				filtered_fare_class = fc
			else:
				filtered_fare_class = [x for x in fc if x in fc2]
			if len(filtered_fare_class) < 1:
				info("No eligible route for fare class %s on flight %s" % (fare_class, str(flights[i])))
				continue
			else:
				if 'Y' in filtered_fare_class:
					fare_class = 'Y'
				else:
					k = randint(0, len(filtered_fare_class)-1)
					fare_class = filtered_fare_class[k]
				info("Testing with fare class %s" % fare_class)
				info("Found route %s" % routes[j])
				random_flight = [flights[i], routes[j]]
		
		origin, dest = random_flight[1].split('-')
		base_payload['flights'] = [{
			"carrierCode": env['config']['companyCode'],
			"farePaid": "100.10",
			"origination": origin,
			"destination": dest,
			"flightNumber": random_flight[0],
			"departureDate": departureDate,
			"fareClass": fare_class
		}]
		
		base_payload = process_forced_payload_values(env, base_payload)
		
		if not env['suppress_raw_payload']:
			print('\n', '='*21, 'PAYLOAD', '='*20)
			print(json.dumps(base_payload, sort_keys=True, indent=2))
	
		json_response = experimental_call_request_service(env, base_payload)
		if not json_response:
			error('No response from POST request. Retrying...')
		elif 'offerUrl' in json_response.keys():
			if env['show_raw_token']:
				print('='*22, 'TOKEN', '='*21)
				pprint(json_response)
			print('\n', '='*15, 'OFFER URL', '='*14)
			url = json_response['offerUrl']
			print(url)
			if env['graphical'] and not env['extra_seat']:
				open_new_tab(url)
			if env['extra_seat']:
				print('\n', '='*13, 'OFFER NFS URL', '='*12)
				url = json_response['offerUrl'].replace('email/upgrade', 'vertical')
				print(url)
				if env['graphical']:
					open_new_tab(url)
			done = True
		else:
			print('\n', '='*17, 'ERROR', '='*16)
			pprint(json_response)
	print('')
	
	if env['instant']:
		flightNumber = base_payload['flights'][0]['flightNumber']
		warning("In ws-service-inventory:")
		warning("Eligibility may be restricted in custom-eligibility.xml")
		info("Stage: ws-service-inventory/src/main/resources/environment/staging-docker/custom-eligibility.xml")
		info("Prod: ws-service-inventory/src/main/resources/environment/production-docker/custom-eligibility.xml")
		try:
			flight = env['flights'][flightNumber]
			if len(flight) == 1:
				s = 'is %s' % flight[0]
			elif len(flight) > 1:
				s = 'can be %s' % ', '.join(flight)
			else:
				raise KeyError
		except KeyError:
			s = 'is unknown'
		warning('Equipment type for flight %s %s' % (flightNumber, s))


def generate_token(env, filtered_routes):
	if len(filtered_routes) < 1:
		return
	
	print('='*32, 'TOKEN GENERATOR', '='*31)
	
	info("Starting to generate token")
	cabin_type = env['cabin_type']
	partner_id = env['partner_id']
	if '*' in cabin_type:
		cabin_type = ['ECONOMY']
	elif len(cabin_type) > 1:
		cabin_type = [cabin_type[0]]
	cabin_type = cabin_type[0]
	info("Selected Cabin Type %s" % cabin_type)
	base_payload = retrieve_minimal_payload(env, ['web_check_in', 'pre_travel_email'])
	base_payload['segmentCount'] = env['segment_count']
	if 'currencyCode' in env['config'].keys():
		base_payload['currency'] = env['config']['currencyCode']
	base_payload['language'] = 'en'
	if env['instant']:
		base_payload['pointOfSale'] = env['config']['defaultPointOfSale']
		tomorrow = datetime.date.today() + datetime.timedelta(hours=env['hours_before'])
		base_payload['departureDate'] = tomorrow.isoformat()
	if env['departure_date']:
		base_payload['departureDate'] = env['departure_date']
	base_payload['pax'] = env['pax']
	if env['createPnr'] and FLAG_ENVIRONMENT != 'PRODUCTION':
		base_payload['createPnr'] = True
	
	retries = 10
	done = False
	while not done:
		if env['instant']:
			random_flight = None
			while not random_flight:
				flights = list(filtered_routes[cabin_type].keys())
				info("Selecting random flight %s" % flights)
				if len(flights) < 1:
					error('No flights were eligible for upgrade.')
					return
				i = randint(0, len(flights)-1)
				info("Testing with flight %s" % str(flights[i]))
				routes = list(filtered_routes[cabin_type][flights[i]].keys())
				j = randint(0, len(routes)-1)
				filtered_fare_class = []
				fc = filtered_routes[cabin_type][flights[i]][routes[j]]
				fc2 = env['fare_class']
				if '*' in fc2:
					filtered_fare_class = fc
				else:
					filtered_fare_class = [x for x in fc if x in fc2]
				if len(filtered_fare_class) < 1:
					info("No eligible route for fare class %s on flight %s" % (fare_class, str(flights[i])))
					continue
				else:
					if 'Y' in filtered_fare_class:
						fare_class = 'Y'
					else:
						k = randint(0, len(filtered_fare_class)-1)
						fare_class = filtered_fare_class[k]
					info("Testing with fare class %s" % fare_class)
					info("Found route %s" % routes[j])
					random_flight = [flights[i], routes[j]]
			base_payload['fareClass'] = fare_class
			base_payload['flightNumber'] = random_flight[0]
			base_payload['origination'], base_payload['destination'] = random_flight[1].split('-')
		else:
			if '*' not in env['origin']:
				base_payload['origination'] = ''.join(env['origin'])
			if '*' not in env['destination']:
				base_payload['destination'] = ''.join(env['destination'])
		
		base_payload = process_forced_payload_values(env, base_payload)
		
		if not env['suppress_raw_payload']:
			print('\n', '='*21, 'PAYLOAD', '='*20)
			print(json.dumps(base_payload, sort_keys=True, indent=2))
	
		print('\n', '='*20, 'RESPONSE', '='*20)
		json_response = call_token_generator(env, base_payload)
		if isinstance(json_response, list):
			if env['show_raw_token']:
				print('='*22, 'TOKEN', '='*21)
				pprint(json_response)
			print('\n', '='*15, 'OFFER URL', '='*14)
			url = json_response[0]['offerFlowRequestTokenResponse']['offerUrl']
			print(url)
			if env['graphical'] and not env['extra_seat']:
				open_new_tab(url)
			if env['extra_seat']:
				print('\n', '='*13, 'OFFER NFS URL', '='*12)
				url = json_response[0]['offerFlowRequestTokenResponse']['offerUrl'].replace('email/upgrade', 'vertical')
				print(url)
				if env['graphical']:
					open_new_tab(url)
			done = True
		elif not json_response:
			error('No response from POST request. Retrying...')
		else:
			print('\n', '='*17, 'ERROR', '='*16)
			pprint(json_response)
		retries -= 1
		if retries < 0: done = True
	print('')
	
	if env['instant']:
		warning("In ws-service-inventory:")
		warning("Eligibility may be restricted in custom-eligibility.xml")
		info("Stage: ws-service-inventory/src/main/resources/environment/staging-docker/custom-eligibility.xml")
		info("Prod: ws-service-inventory/src/main/resources/environment/production-docker/custom-eligibility.xml")
		try:
			flight = env['flights'][base_payload['flightNumber']]
			if len(flight) == 1:
				s = 'is %s' % flight[0]
			elif len(flight) > 1:
				s = 'can be %s' % ', '.join(flight)
			else:
				raise KeyError
		except KeyError:
			s = 'is unknown'
		warning('Equipment type for flight %s %s' % (base_payload['flightNumber'], s))
	

def retrieve_partner_configuration(env, alternate = 0):
	global REQUESTS_ENV, FLAG_ENVIRONMENT
	info("Loading partner configuration")
	
	
	json_response = None
	try:
		url = '%s/partner/id/%s' % (REQUESTS_ENV[FLAG_ENVIRONMENT]['CONFIGURATION_SERVICE'][alternate], env['partner_id'])
		info("Sending GET request to %s" % url)
		response = requests.get(url, headers={'Content-Type': 'application/json;charset=UTF-8'})
		info("Received response")
		json_response = response.json()
	except ValueError:
		next = (alternate+1)%len(REQUESTS_ENV[FLAG_ENVIRONMENT]['CONFIGURATION_SERVICE'])
		if next == 0:
			error('No response from GET request. Retrying...')
			exit()
		return retrieve_partner_configuration(env, next)
	return json_response

def maximize_hours_before(env):
	debug('Trying to maximize hours-before for token')
	try:
		if env['extra_seat']:
			debug('Extra Seat is requested, setting 24h for now')
			env['hours_before'] = 24
		else:
			debug('Partner config has eligibleDaysBeforeStart set to %s' % str(env['config']['products']['instantUpgrade']['eligibleDaysBeforeStart']))
			env['hours_before'] = (env['config']['products']['instantUpgrade']['eligibleDaysBeforeStart'] - 2)*24
			debug('Setting hours-before to %s' % str(env['hours_before']))
		if env['config']['products']['instantUpgrade']['eligibleDaysBeforeEnd']*24 > env['hours_before']:
			debug('Partner config has eligibleDaysBeforeEnd set to %s' % str(env['config']['products']['instantUpgrade']['eligibleDaysBeforeEnd']))
			env['hours_before'] = env['config']['products']['instantUpgrade']['eligibleDaysBeforeEnd']*24+23
			debug('Setting hours-before to %s' % str(env['hours_before']))
		if env['config']['products']['instantUpgrade']['eligibleDaysBeforeStart']*24 < env['hours_before']:
			warning('products.instantUpgrade.eligibleDaysBeforeStart has incoherent value')
	except KeyError:
		pass

def check_partner_config(env):
	c = env['config']
	if 'Error' in c.keys():
		error('Configuration Service -- %s' % str(c['Error']))
		return False
	
	if env['config']['partnerType'] == 'cruise':
		return True
	
	if not env['instant']:
		return True
	
	if 'asyncUpgradeProcess' not in c.keys():
			warning('Key is missing from configuration file: %s' % 'asyncUpgradeProcess')
			warning('Email triggering may not function correctly')
			
	try:
		if not c['products']['instantUpgrade']['version2']:
			raise ValueError('products.instantUpgrade.version2')
	except KeyError as e:
		warning('Key is missing from configuration file: %s' % e)
		return False
	except ValueError as e:
		warning('Key is not true: %s' % e)
		return False
	
	try:
		if not c['products']['instantUpgrade']['skipAvailabilityCheck']:
			info('products.instantUpgrade.skipAvailabilityCheck is false')
			env['createPnr'] = True
	except KeyError as e:
		pass
	
	return True

def present_results(filtered_routes):
	cabin_types = list(filtered_routes.keys())
	out = {}
	routes = {}
	max_len = {'flights':0}
	for cabin_type in filtered_routes:
		max_len[cabin_type] = 0
		flights = list(filtered_routes[cabin_type].keys())
		flights.sort()
		for flight in flights:
			for route in filtered_routes[cabin_type][flight]:
				flight_s = str(flight)
				if flight_s not in out.keys():
					out[flight_s] = {}
					routes[flight_s] = route
					if len(flight_s) > max_len['flights']:
						max_len['flights'] = len(flight_s)
				out[flight_s][cabin_type] = ','.join(filtered_routes[cabin_type][flight][route])
				if cabin_type not in max_len.keys() or len(out[flight_s][cabin_type]) > max_len[cabin_type]:
					max_len[cabin_type] = len(out[flight_s][cabin_type])

	print('Flight  Route     %s' % ('   ').join(['%s%s' % (x, ' '*(max_len[x]-len(x))) for x in cabin_types]))
	flights = list(out.keys())
	flights.sort(key = int)
	for flight in flights:
		flight_s = str(flight)
		print('%s%s%s   %s' % (flight, ' '*(8-len(flight_s)), routes[flight_s], ('   ').join(['%s%s' % (out[flight_s][x], ' '*(max_len[x]-len(out[flight_s][x]))) for x in out[flight_s]])))

def present_cruises(env, cruises):
	cruises_list = list(cruises.keys())
	cruises_list.sort()
	for equipment in cruises_list:
		print('\n%s:' % equipment)
		if not env['suppress_cruise_details']:
			mappings_list = list(cruises[equipment]['mappings'].keys())
			mappings_list.sort()
			max_len = 0
			for category in mappings_list:
				if max_len < len(category):
					max_len = len(category)
			max_len = 8 * round(max_len/8) + 8
			print('\tCategory%sFare Classes' % (' '*(max_len-8)))
			for category in cruises[equipment]['mappings']:
				print('\t%s%s%s' % (category, ' '*(max_len-len(category)),' '.join(cruises[equipment]['mappings'][category])))
			
			combinations_list = list(cruises[equipment]['combinations'].keys())
			combinations_list.sort()
			max_len = 0
			for from_cabin in combinations_list:
				if max_len < len(from_cabin):
					max_len = len(from_cabin)
			max_len = 8 * round(max_len/8) + 8
			print('\n\tFrom Cabin%sTo Cabin' % (' '*(max_len-10)))
			for from_cabin in cruises[equipment]['combinations']:
				print('\t%s%s%s' % (from_cabin, ' '*(max_len-len(from_cabin)),', '.join(cruises[equipment]['combinations'][from_cabin])))
			
		print('\n\tDays\tDeparture Date\tFlight Number')
		durations = list(cruises[equipment]['durations'].keys())
		durations.sort()
		for duration in durations:
			dates = cruises[equipment]['durations'][duration]
			if not env['show_all']:
				dates = [dates[0]]
			else:
				dates.sort(key=lambda x: x[0])
			for date in dates:
				print('\t%s\t%s\t%s' % (duration, date[0], date[1]))

def troubleshoot_instant_upgrade(env):

#	SELECT fcr.fare_class, fcd.cabin_type, fcr.upgrade_type FROM offers.FareClassRules AS fcr LEFT JOIN offers.FareClassDef AS fcd ON fcr.fare_class = fcd.fare_class AND fcr.partner_id = fcd.partner_id WHERE fcr.partner_id = %d AND fcr.eligible = 1 AND fcr.instantupgrade_eligible = 1;

#	SELECT fcr.fare_class, fcd.cabin_type, fcr.upgrade_type, fcr.eligible, fcr.instantupgrade_eligible FROM offers.FareClassRules AS fcr LEFT JOIN offers.FareClassDef AS fcd ON fcr.fare_class = fcd.fare_class AND fcr.partner_id = fcd.partner_id WHERE fcr.partner_id = %d;

#	SELECT ups.from_cabin, ups.upgrade_type, ups.fare_class, ups.origin, ups.dest FROM offers.UpgradePriceSettings AS ups LEFT JOIN offers.ProductSettings AS ps ON ups.partner_id = ps.partner_id AND ups.from_cabin = ps.from_cabin AND ups.upgrade_type = ps.upgrade_type AND ups.origin = ps.origin AND ups.dest = ps.dest WHERE ups.partner_id = %d AND ups.enabled = 1 AND ups.end_date > NOW() AND ups.start_date < DATE_ADD(NOW(), INTERVAL 6 MONTH)%s%s;

#	SELECT fi.flight_num, fi.origin, fi.dest FROM offers.FlightInventory AS fi LEFT JOIN offers.Route AS r ON fi.partner_id = r.partner_id AND fi.origin = r.origin AND fi.dest = r.dest WHERE fi.partner_id = %d AND fi.last_flight > NOW() AND fi.first_flight < DATE_ADD(NOW(), INTERVAL 6 MONTH) AND fi.cancelled = 0 AND (r.eligibility = 'ALL' OR (r.eligibility = 'PARTIAL' AND fi.eligibility = 'T')) AND (r.instantupgrade_eligibility='ALL' OR (r.instantupgrade_eligibility='PARTIAL' AND fi.instantupgrade_eligibility='T'));

#	SELECT exclusive,from_cabin,upgrade_type,fare_class,origin,dest FROM offers.FareClassEligibilityRestrictions WHERE partner_id = %s AND end_date > NOW() AND start_date < DATE_ADD(NOW(), INTERVAL 6 MONTH);

	today = datetime.date.today()

	fcr_query = "SELECT DISTINCT fare_class, eligible, instantupgrade_eligible FROM offers.FareClassRules WHERE partner_id = %d;" % env['partner_id']
	
	fcr = env['db'].execute(fcr_query)[0]['result']
	info("Executed FareClassRules query")
	
	fcr_total = len(fcr)
	fcr_excluded_no_eligible = 0
	fcr_excluded_no_instanteligible = 0
	for x in fcr:
		if str(x[1]) != '1':
			fcr_excluded_no_eligible += 1
			info('Excluded FareClassRules row due to (eligible != 1):\n\t %s' % x[0])
			continue
		
		if str(x[2]) != '1':
			fcr_excluded_no_instanteligible += 1
			info('Excluded FareClassRules row due to (instantupgrade_eligible != 1):\n\t %s' % x[0])
			continue
	print("\n```%s```\n" % fcr_query)
	print('FareClassRules: %d/%d rows eligible\n\t=> %d rows excl. (eligible != 1)\n\t=> %d rows excl. (instantupgrade_eligible != 1)' % (
		fcr_total-fcr_excluded_no_eligible-fcr_excluded_no_instanteligible,
		fcr_total,
		fcr_excluded_no_eligible,
		fcr_excluded_no_instanteligible))
	
	ups_query = "SELECT DISTINCT ups.id, ups.origin, ups.dest, ups.start_date, ups.end_date, ps.id, ps.start_date, ps.end_date, ups.enabled, fi.id FROM offers.UpgradePriceSettings AS ups LEFT JOIN offers.ProductSettings AS ps ON ups.partner_id = ps.partner_id AND ups.from_cabin = ps.from_cabin AND ups.upgrade_type = ps.upgrade_type AND ups.origin = ps.origin AND ups.dest = ps.dest LEFT JOIN offers.FlightInventory AS fi ON ups.partner_id = fi.partner_id AND ups.origin = fi.origin AND ups.dest = fi.dest WHERE ups.partner_id = %d;" % env['partner_id']
		
	ups = env['db'].execute(ups_query)[0]['result']
	info("Executed UpgradePriceSettings query")
	
	ups_total = len(ups)
	ups_excluded_no_ps = 0
	ups_excluded_date_range = 0
	ups_excluded_not_enabled = 0
	ups_excluded_no_flight = 0
	for x in ups:
		if x[5] == None:
			ups_excluded_no_ps += 1
			info('Excluded UpgradePriceSettings row due to no matching ProductSettings row:\n\t[id = %d] %s-%s' % (x[0], x[1], x[2]))
			continue
		if x[3] == None or x[3] > today:
			ups_excluded_date_range += 1
			info('Excluded UpgradePriceSettings row due to UpgradePriceSettings.start_date after now:\n\t[id = %d] %s-%s' % (x[0], x[1], x[2]))
			continue
		if x[4] == None or x[4] < today:
			ups_excluded_date_range += 1
			info('Excluded UpgradePriceSettings row due to UpgradePriceSettings.end_date before now:\n\t[id = %d] %s-%s' % (x[0], x[1], x[2]))
			continue
		if x[6] == None or x[6] > today:
			ups_excluded_date_range += 1
			info('Excluded UpgradePriceSettings row due to ProductSettings.start_date after now:\n\t[id = %d] %s-%s' % (x[0], x[1], x[2]))
			continue
		if x[7] == None or x[7] < today:
			ups_excluded_date_range += 1
			info('Excluded UpgradePriceSettings row due to ProductSettings.end_date before now:\n\t[id = %d] %s-%s' % (x[0], x[1], x[2]))
			continue
		if str(x[8]) != '1':
			ups_excluded_not_enabled += 1
			info('Excluded UpgradePriceSettings row due to (enabled != 1):\n\t[id = %d] %s-%s' % (x[0], x[1], x[2]))
			continue
		if x[9] == None:
			ups_excluded_no_flight += 1
			info('Excluded UpgradePriceSettings row due to (no flight in inventory):\n\t[id = %d] %s-%s' % (x[0], x[1], x[1]))
			continue
	
	print("\n```%s```\n" % ups_query)
	print('UpgradePriceSettings: %d/%d rows eligible\n\t=> %d rows excl. (no ProductSettings row)\n\t=> %d rows excl. (outside date range)\n\t=> %d rows excl. (not enabled)\n\t=> %d rows excl. (no flight in inventory)' % (
		ups_total-ups_excluded_no_ps-ups_excluded_date_range-ups_excluded_not_enabled-ups_excluded_no_flight,
		ups_total,
		ups_excluded_no_ps,
		ups_excluded_date_range,
		ups_excluded_not_enabled,
		ups_excluded_no_flight))
	
	route_query = "SELECT DISTINCT origin, dest, eligibility, instantupgrade_eligibility FROM offers.Route WHERE partner_id = %d;" % env['partner_id']
	
	routes = env['db'].execute(route_query)[0]['result']
	info("Executed Route query")
	
	routes_total = len(routes)
	routes_excluded_no_eligibility = 0
	routes_excluded_no_instanteligibility = 0
	for x in routes:
		if x[2] == 'NONE':
			routes_excluded_no_eligibility += 1
			info('Excluded Routes row due to (eligibility = NONE):\n\t %s-%s' % (x[0], x[1]))
			continue
		if x[3] == 'NONE':
			routes_excluded_no_instanteligibility += 1
			info('Excluded Routes row due to (instantupgrade_eligibility = NONE):\n\t %s-%s' % (x[0], x[1]))
			continue
	
	print("\n```%s```\n" % route_query)
	print('Routes: %d/%d rows eligible\n\t=> %d rows excl. (eligibility is NONE)\n\t=> %d rows excl. (instantupgrade_eligibility is NONE)' % (
		routes_total-routes_excluded_no_eligibility-routes_excluded_no_instanteligibility,
		routes_total, 
		routes_excluded_no_eligibility, 
		routes_excluded_no_instanteligibility))

	
	fi_query = "SELECT DISTINCT fi.id, fi.flight_num, fi.origin, fi.dest, fi.eligibility, fi.instantupgrade_eligibility, fi.first_flight, fi.last_flight, fi.cancelled, r.eligibility, r.instantupgrade_eligibility FROM offers.FlightInventory AS fi LEFT JOIN offers.Route AS r ON fi.partner_id = r.partner_id AND fi.origin = r.origin AND fi.dest = r.dest WHERE fi.partner_id = %d;" % env['partner_id']
	inventory = env['db'].execute(fi_query)[0]['result']
	info("Executed FlightInventory query")
	
	inventory_total = len(inventory)
	inventory_excluded_no_route = 0
	inventory_excluded_no_eligibility = 0
	inventory_excluded_no_instanteligibility = 0
	inventory_excluded_no_route_eligibility = 0
	inventory_excluded_no_route_instanteligibility = 0
	inventory_date_range = 0
	inventory_excluded_cancelled = 0
	for x in inventory:
		if x[4] != 'T':
			inventory_excluded_no_eligibility += 1
			info('Excluded Inventory row due to (eligibility != T):\n\t[id = %d] Flight #%s (%s-%s)' % (x[0], x[1], x[2], x[3]))
			continue
		if x[5] != 'T':
			inventory_excluded_no_instanteligibility += 1
			info('Excluded Inventory row due to (instantupgrade_eligibility != T):\n\t[id = %d] Flight #%s (%s-%s)' % (x[0], x[1], x[2], x[3]))
			continue
		if x[6] == None or x[6] > today:
			inventory_date_range += 1
			info('Excluded Inventory row due to (first_flight after now):\n\t[id = %d] Flight #%s (%s-%s)' % (x[0], x[1], x[2], x[3]))
			continue
		if x[7] == None or x[7] < today:
			inventory_date_range += 1
			info('Excluded Inventory row due to (last_flight before now):\n\t[id = %d] Flight #%s (%s-%s)' % (x[0], x[1], x[2], x[3]))
			continue
		if str(x[8]) == '1':
			inventory_excluded_cancelled += 1
			info('Excluded Inventory row due to (cancelled = 1):\n\t[id = %d] Flight #%s (%s-%s)' % (x[0], x[1], x[2], x[3]))
			continue
		if x[9] == None:
			inventory_excluded_no_route += 1
			info('Excluded Inventory row due to (no route in Route):\n\t[id = %d] Flight #%s (%s-%s)' % (x[0], x[1], x[2], x[3]))
			continue
		if x[9] == 'NONE':
			inventory_excluded_no_route_eligibility += 1
			info('Excluded Inventory row due to (Route.eligibility = NONE):\n\t[id = %d] Flight #%s (%s-%s)' % (x[0], x[1], x[2], x[3]))
			continue
		if x[10] == 'NONE':
			inventory_excluded_no_route_instanteligibility += 1
			info('Excluded Inventory row due to (Route.instantupgrade_eligibility = NONE):\n\t[id = %d] Flight #%s (%s-%s)' % (x[0], x[1], x[2], x[3]))
			continue
	
	print("\n```%s```\n" % fi_query)
	print('FlightInventory: %d/%d rows eligible\n\t=> %d rows excl. (eligibility != T)\n\t=> %d rows excl. (instantupgrade_eligibility != T)\n\t=> %d rows excl. (outside date range)\n\t=> %d rows excl. (cancelled = 1)\n\t=> %d rows excl. (no route in Route)\n\t=> %d rows excl. (Route.eligibility = NONE)\n\t=> %d rows excl. (Route.instantupgrade_eligibility = NONE)' % (
		inventory_total-inventory_excluded_no_route-inventory_excluded_no_eligibility-inventory_excluded_no_instanteligibility-inventory_excluded_no_route_eligibility-inventory_excluded_no_route_instanteligibility-inventory_date_range-inventory_excluded_cancelled,
		inventory_total,
		inventory_excluded_no_eligibility,
		inventory_excluded_no_instanteligibility,
		inventory_date_range,
		inventory_excluded_cancelled,
		inventory_excluded_no_route,
		inventory_excluded_no_route_eligibility,
		inventory_excluded_no_route_instanteligibility,
	))

def show_email_links(env, language = 'en'):
	global FLAG_ENVIRONMENT
	
	partner_code = None
	for x in env['partners']:
		if env['partners'][x] == env['partner_id']:
			partner_code = x.lower()
	if not partner_code:
		error('Could not find partner id %d' % (env['partner_id']))
		return
	
	done = False
	json_response = None
	while not done:
		payload = retrieve_minimal_payload(env)
		payload['language'] = language
		json_response = call_token_generator(env, payload)
		if isinstance(json_response, list):
			done = True
		elif not json_response:
			error('No response from POST request. Retrying...')
		else:
			print('\n', '='*17, 'ERROR', '='*16)
			pprint(json_response)
	offer_url = ''
	request_id = ''
	try:
		offer_url = json_response[0]['offerFlowRequestTokenResponse']['offerUrl']
		request_id = json_response[0]['offerFlowRequestTokenResponse']['requestId']
	except KeyError:
		pass
	
	if not env['db'].check_permission_list([
		'offers.Offers.SELECT',
	]):
		return
	
	offer_query = "SELECT id, request_id FROM offers.Offers WHERE partner_id = %d AND offer_language_code = %s ORDER BY created DESC LIMIT 1;" % (env['partner_id'], repr(language))
	offer_id = env['db'].execute(offer_query)[0]['result'][0][0]
	if len(request_id) < 1:
		request_id = env['db'].execute(offer_query)[0]['result'][0][1]
	info("Executed Offers query")
	
	encrypted_offer_id = None
	long_partner_id = offer_url.split('/offer/')[1].split('/')[0]
	
	services = {
		'STAGE': {
			'partner-app': 'https://partner-stage.plusgrade.com/partner',
			'consumer-app': 'https://upgrade-stage.plusgrade.com',
			'tester': 'https://upgrade-stage.plusgrade.com/offer/tester',
		},
		'PRODUCTION': {
			'partner-app': 'https://partner.plusgrade.com/partner',
			'consumer-app': 'https://upgrade.plusgrade.com',
			'tester': 'https://upgrade.plusgrade.com/offer/tester',
		}
	}
	
	partner_app = services[FLAG_ENVIRONMENT]['partner-app']
	consumer_app = services[FLAG_ENVIRONMENT]['consumer-app']
	tester = services[FLAG_ENVIRONMENT]['tester']
	
	
	url = '%s/offerIdEncrypt' % (tester)
	encrypted_offer_id = None
	info('Sending POST request for Offer ID encryption')
	while not encrypted_offer_id:
		encrypted_offer_id = requests.post(url, auth=('pgteam','comeFlyWithMe-N0W'), data={'offerId': offer_id}).text
		if not encrypted_offer_id:
			error('No response from POST request. Retrying...')

	print('Offer ID:', offer_id)
	print('Encrypted Offer ID:', encrypted_offer_id)
	print('Request ID:', request_id)
	
	return
	
	params = '&'.join([
		'partnerId=%s' % env['partner_id'],
		'authKey=4BseTY4CnUv9',
		'overrideEmailAddress=%s' % 'pierre.corcoran@plusgrade.com',
		'testerEmail=true',
		'bypassSpamStopper=true',
	])
	
	out = {}
	
	# Passenger-facing Views
	
	# View aop
	out['View AOP'] = offer_url.replace('email/upgrade', 'aop/display')
	# View booster
	out['View Booster'] = offer_url.replace('email/upgrade', 'booster/email')
	# View campaign
	out['View Campaign'] = offer_url.replace('email/upgrade', 'campaign/email')
	# View chaser
	out['View Chaser'] = offer_url.replace('email/upgrade', 'chaser/email')
	# View make-an-offer
	out['View Make-An-Offer'] = offer_url.replace('email/upgrade', 'pretravel')
	# View NFS
	out['View NFS'] = offer_url.replace('email/upgrade', 'vertical')
	
	out['View Offer Submit'] = '%s/offer/bid/email/debug/%s/CEeaFMojotrmrG013eYb-w?&template=/partner/%s/mail/_offer_submit&lang=%s' % (consumer_app, long_partner_id, partner_code, language)
	

	
	# USING REQUEST ID
	
	# Send make-an-offer
	out['Send Make-An-Offer'] = '%s/services/make-an-offer/sendByRequestId?%s&requestId=%s' % (partner_app, params, request_id)
	
	# Send chaser
	out['Send Chaser'] = '%s/services/chaser-email/sendByRequestId?%s&requestId=%s' % (partner_app, params, request_id)
	
	# USING OFFER ID
	
	# Send aop -- Doesn't work
	out['Send AOP'] = '%s/services/offer/aop?%s&offerId=%s' % (consumer_app, params, offer_id)
	
	# Send campaign
	out['Send Campaign'] = '%s/services/offer/campaign?%s&offerId=%s' % (partner_app, params, offer_id)
	
	pprint(out)
	
def live_auction(env):
	live_auction_enabled = True
	try:
		if not env['config']['liveAuction']['enabled']: live_auction_enabled = False
	except KeyError:
		live_auction_enabled = False
	
	if not live_auction_enabled:
		warning('Live Auction is not enabled in the partner config for this partner')

	las_query = 'SELECT fi.flight_num, fi.origin, fi.dest, las.travel_dt, o.request_id, o.from_cabin, o.upgrade_type, o.equip FROM `offers`.`LiveAuctionStatus` as las LEFT JOIN `offers`.`FlightInventory` as fi ON las.product_id = fi.id LEFT JOIN `offers`.`Offers` as o ON o.product_id = fi.id LEFT JOIN `offers`.`OfferLiveAuction` AS ola ON o.id = ola.offer_id WHERE las.partner_id = %s AND las.live_auction_open = "1" AND fi.eligibility = "T" AND fi.cancelled = 0 AND las.travel_dt > DATE_ADD(NOW(), INTERVAL 2 DAY) AND ola.in_live_auction = 1 AND o.offer_status = "SUBMITTED" GROUP BY fi.flight_num ORDER BY las.travel_dt;'
	las = env['db'].execute(las_query % env['partner_id'])[0]['result']
	info("Executed LiveAuctionStatus query")
	
	if len(las) < 1:
		error('No active Live Auctions were found')
		return

	print('Flight\tRoute\tTravel Date\tFrom Cabin --> To Cabin\t Equipment')
	for x in las:
		print('%s\t%s-%s\t%s\t%s --> %s\t%s'%(x[0],x[1],x[2],x[3],x[5],x[6],x[7]))
		print('\thttps://upg.stage.plusgrade.com/live/splash/%s/%s' % (env['config']['pid'],x[4]))


def verify_vpc_connection():
	# Errorcode is 58 if LDAP server is existant
	# otherwise, errorcode is 28 for timeout
	try:
		check_output(['curl', 'ldaps://ldap-01.plusgrade.com', '-s', '--connect-timeout', '1'])
	except CalledProcessError as e:
		return e.returncode == 58

def main():
	if not verify_vpc_connection():
		error('VPC connection is not active')
		exit()
	
	args = retrieve_args()
	
	env = {
		'partner_id': None,
		'fare_class': None,
		'cabin_type': None,
		'equipment': None,
		'hours_before': None,
		'createPnr': False,
		'segment_count': 1,
		'pax': 1,
		'graphical': False,
	}
	
	db = DatabaseController()
	db.select_stage()
	env['db'] = db
	
	retrieve_partners(env)
	retrieve_parameters(env, args)
	
	if not check_partner_config(env):
		warning('Partner is not set up correctly for instant upgrade')
	
	if not args.hours_before:
		maximize_hours_before(env)
	
	if FLAG_ENVIRONMENT == 'PRODUCTION':
		env['db'].select_prod_readonly()
		info("Selected production read-only database")
	else:
		env['db'].select_stage()
		info("Selected stage database")
	
	if not env['db'].check_permission_list([
		'offers.CabinTypeUpgradeEligibility.SELECT',
		'offers.CruiseShipFareClassMappings.SELECT',
		'offers.CruiseShipUpgradeCombinations.SELECT',
		'offers.FareClassDef.SELECT',
		'offers.FareClassEligibilityRestrictions.SELECT',
		'offers.FareClassRules.SELECT',
		'offers.FlightInvVariation.SELECT',
		'offers.FlightInventory.SELECT',
		'offers.Partners.SELECT',
		'offers.ProductSettings.SELECT',
		'offers.Route.SELECT',
		'offers.RuleAttribute.SELECT',
		'offers.RuleData.SELECT',
		'offers.UpgradeCombinations.SELECT',
		'offers.UpgradePriceSettings.SELECT',
	]):
		return
	
	done = False
	while not done:
		if env['troubleshoot']:
			troubleshoot_instant_upgrade(env)
		
		if env['show_email_links']:
			show_email_links(env)

		if env['live_auction']:
			live_auction(env)
		
		if 'partnerType' in env['config'] and env['config']['partnerType'] == 'cruise':
				cruises = retrieve_cruises(env)
				if env['generate_token']:
					generate_cruise_token(env, cruises)
				if env['list']:
					present_cruises(env, cruises)
		else:
			if check_rules_cutoff_v2(env) and (env['list'] or env['generate_token']):
				filtered_prices = retrieve_prices(env)		
				filtered_routes = filter_eligible_routes(env, filtered_prices)
				filtered_routes = filter_restrictions(env, filtered_routes)
				filtered_routes = filter_equipments(env, filtered_routes)
				
				if env['extra_seat']:
					filtered_routes = filter_extra_seat(env, filtered_routes)	
				
				if env['list']:
					info("Presenting results")
					present_results(filtered_routes)
				if env['generate_token']:
					if env['experimental']:
						experimental_generate_token(env, filtered_routes)
					else:
						generate_token(env, filtered_routes)
		
		if env['graphical']:
			print('-'*60)
			graphical_menu_main_loop(env)
		else:
			done = True

if __name__ == '__main__':
	main()
