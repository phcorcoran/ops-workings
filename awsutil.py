#!/usr/bin/env python3
import os, sys

#pragma mark Version Handling

FLAG_VERSION = '0.1.2'

def register_version_args(parser):
	parser.add_argument('--version', help='Output script version', action='store_true')
	
def handle_version_args(args):
	global FLAG_VERSION
	try:
		if args.version:
			print('%s Ver' % os.path.basename(__file__), FLAG_VERSION)
			exit()
	except AttributeError:
		pass

#pragma mark -

#pragma mark Import Handling

import argparse
import json
from subprocess import run, check_output
from urllib.parse import urlparse
from pprint import pprint

#pragma mark -

#pragma mark Verbose Handling

FLAG_VERBOSE = 2

def verbose(*args, level = 'INFO'):
	global FLAG_VERBOSE
	levels = [
		['FATAL', '\033[31m'],
		['ERROR', '\033[31m'],
		['WARNING', '\033[33m'],
		['INFO', '\033[32m'],
		['DEBUG', '\033[36m'],
		['TRACE', '\033[90m'],
	]
	color_end = '\033[0m'
	for i in range(FLAG_VERBOSE+1):
		if levels[i][0] == level:
			print("[%s%s%s]" % (levels[i][1], level, color_end), *args)

def debug(*args):
	verbose(*args, level='DEBUG')

def register_verbose_args(parser):
	group = parser.add_argument_group('Verbose')
	group.add_argument('--verbose', '-v', help='Output more information (-vv debug, -vvv trace)', action='count', default=0)
	group.add_argument('--quiet', '-q', help='Suppress warnings (-qq errors, -qqq fatal)', action='count', default=0)

def trace_calls(frame, event, arg):
	if event != 'call' or frame.f_code.co_name == 'write': return
	try:
		print('[%sTRACE%s] Called `%s` (line %s of `%s`) from line %s of `%s`' %  ('\033[90m', '\033[0m', frame.f_code.co_name, frame.f_lineno, frame.f_code.co_filename, frame.f_back.f_lineno, frame.f_back.f_code.co_filename))
	except AttributeError:
		pass
	return

def handle_verbose_args(args):
	global FLAG_VERBOSE
	try:
		FLAG_VERBOSE += args.verbose - args.quiet
	except AttributeError:
		pass
	if FLAG_VERBOSE >= 5:
		try:
			sys.settrace(trace_calls)
		except NameError:
			pass

#pragma mark -

def load_distributions():
	distributionIDs = {}
	s = check_output(['aws', 'cloudfront', 'list-distributions'])
	try:
		out = json.loads(s)
	except ValueError:
		verbose('Unable to load distributions from AWS:\n%s' % s, level='ERROR')
		exit()
	for x in out['DistributionList']['Items']:
		if x['Status'] != 'Deployed': continue
		d = {x['DomainName']: True}
		
		try:
			for y in x['Aliases']['Items']:
				d[y] = True
		except KeyError:
			pass
		try:
			for y in x['Origins']['Items']:
				d[y['DomainName']] = True
		except KeyError:
			pass
		try:
			for y in x['AliasICPRecordals']:
				d[y['CNAME']] = True
		except KeyError:
			pass
		
		distributionIDs[x['Id']] = list(d.keys())
	return distributionIDs


def main():	
	distributionIDs = load_distributions()
	
	parser = argparse.ArgumentParser()
	
	parser.add_argument('URLs', nargs='*', help='Image URLs')

	register_verbose_args(parser)
	register_version_args(parser)
	
	args = parser.parse_args()
	
	handle_version_args(args)
	handle_verbose_args(args)
	
	urls = []
	
	for x in args.URLs:
		verbose('Adding %s' % x)
		urls.append(urlparse(x))

	if len(urls) < 1:
		while True:
			x = input('Enter image URL (leave empty when done)? ').strip()
			if len(x) < 1: break
			verbose('Adding %s' % x)
			urls.append(urlparse(x))
	
	to_run = []
	
	for url in urls:
		netloc = url.netloc
		path = url.path
		
		distribution = []
		
		for k in distributionIDs:
			for url in distributionIDs[k]:
				if url in netloc:
					verbose('Found distribution %s for netloc %s' % (k, netloc))
					distribution.append(k)
		
		if len(distribution) < 1:
			verbose('Could not find the correct distribution ID for provided URL `%s`' % netloc, level='ERROR')
		else:
			for d in distribution:
				to_run.append([d, path])
	
	for x in to_run:
		print('Running invalidation: distribution `%s`, path `%s`' % (x[0], x[1]))
		try:
			cmd = ['aws', 'cloudfront', 'create-invalidation', '--distribution-id', x[0], '--paths', x[1]]
			verbose('Calling: %s' % cmd)
			s = check_output(cmd).decode('utf-8')
			out = json.loads(s)
			print('Response: `%s`' % out['Invalidation']['Status'])
		except (AttributeError, ValueError, KeyError):
			verbose('AWS command failed:\n%s' % s, level='ERROR')
		


if __name__ == '__main__':
	main()
