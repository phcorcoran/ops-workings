
all:
	docker build -f offerutil.py.dockerfile -t offerutil .

run:
	docker run --rm -it -v ~/.gnupg:/root/.gnupg offerutil
