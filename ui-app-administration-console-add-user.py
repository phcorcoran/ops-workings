#!/usr/bin/env python3
import os, sys

#pragma mark Version Handling

FLAG_VERSION = '0.1.3'

def register_version_args(parser):
	parser.add_argument('--version', help='Output script version', action='store_true')
	
def handle_version_args(args):
	global FLAG_VERSION
	try:
		if args.version:
			print('%s Ver' % os.path.basename(__file__), FLAG_VERSION)
			exit()
	except AttributeError:
		pass

#pragma mark -

#pragma mark Import Handling

ERROR_MSG = []

try:
	import requests
except ModuleNotFoundError as e:
	ERROR_MSG.append('[ERROR] %s' % e)
	ERROR_MSG.append('[ERROR] Run `sudo pip3 install requests`')

try:
	import pwgen
except ModuleNotFoundError as e:
	ERROR_MSG.append('[ERROR] %s' % e)
	ERROR_MSG.append('[ERROR] Run `sudo pip3 install pwgen`')

if len(ERROR_MSG) > 0:
	print('\n'.join(ERROR_MSG))
	exit()

import json
import argparse
from getpass import getpass

#pragma mark -

#pragma mark Verbose Handling

FLAG_VERBOSE = 2

def verbose(*args, level = 'INFO'):
	global FLAG_VERBOSE
	levels = [
		['FATAL', '\033[31m'],
		['ERROR', '\033[31m'],
		['WARNING', '\033[33m'],
		['INFO', '\033[32m'],
		['DEBUG', '\033[36m'],
		['TRACE', '\033[90m'],
	]
	color_end = '\033[0m'
	for i in range(FLAG_VERBOSE+1):
		if levels[i][0] == level:
			print("[%s%s%s]" % (levels[i][1], level, color_end), *args)

def debug(*args):
	verbose(*args, level='DEBUG')

def register_verbose_args(parser):
	group = parser.add_argument_group('Verbose')
	group.add_argument('--verbose', '-v', help='Output more information (-vv debug, -vvv trace)', action='count', default=0)
	group.add_argument('--quiet', '-q', help='Suppress warnings (-qq errors, -qqq fatal)', action='count', default=0)

def trace_calls(frame, event, arg):
	if event != 'call' or frame.f_code.co_name == 'write': return
	try:
		print('[%sTRACE%s] Called `%s` (line %s of `%s`) from line %s of `%s`' %  ('\033[90m', '\033[0m', frame.f_code.co_name, frame.f_lineno, frame.f_code.co_filename, frame.f_back.f_lineno, frame.f_back.f_code.co_filename))
	except AttributeError:
		pass
	return

def handle_verbose_args(args):
	global FLAG_VERBOSE
	try:
		FLAG_VERBOSE += args.verbose - args.quiet
	except AttributeError:
		pass
	if FLAG_VERBOSE >= 5:
		try:
			sys.settrace(trace_calls)
		except NameError:
			pass

#pragma mark -

#pragma mark Environment Handling

FLAG_ENVIRONMENT = None

def register_environment_args(parser, include_prod_readonly = False, include_prod_confirm = True):
	verbose('Environment: Setting up argument group', level='DEBUG')
	group = parser.add_argument_group('Environment')
	verbose('Environment: Adding STAGE argument', level='DEBUG')
	group.add_argument('--stage', help='Select STAGE database', action='store_true')
	verbose('Environment: Adding PRODUCTION argument', level='DEBUG')
	group.add_argument('--prod', help='Select PRODUCTION database', action='store_true')
	if include_prod_readonly:
		verbose('Environment: Adding PRODUCTION READ-ONLY argument', level='DEBUG')
		parser.add_argument('--prod-readonly', help='Select PRODUCTION READ-ONLY database', action='store_true')
	if include_prod_confirm:
		verbose('Environment: Adding skip-prod-confirm argument', level='DEBUG')
		parser.add_argument('--skip-prod-confirm', help='Disable PRODUCTION confirmation prompt', action='store_true')
	
def handle_environment_args(args, default = None):
	global FLAG_ENVIRONMENT
	include_prod_readonly = True
	include_prod_confirm = True
	
	try: 
		if args.skip_prod_confirm: include_prod_confirm = False
	except AttributeError: include_prod_confirm = False
	
	verbose('Environment: include_prod_confirm set to %s' % str(include_prod_confirm), level='DEBUG')
	
	env_select = None
	
	if default == 'STAGE':
		env_select = 1
	elif default == 'PRODUCTION':
		env_select = 2
	elif default == 'PRODUCTION READ-ONLY':
		env_select = 3
	
	try:
		if args.prod: env_select = 2
	except AttributeError: pass
	
	try:
		if args.prod_readonly: env_select = 3
	except AttributeError: include_prod_readonly = False
	
	try:
		if args.stage: env_select = 1
	except AttributeError: pass
	
	verbose('Environment: env_select set to %s' % str(env_select), level='DEBUG')
	
	if not env_select:
		if include_prod_readonly:
			print("1. STAGE\n2. PRODUCTION\n3. PRODUCTION READ-ONLY")
		else:
			print("1. STAGE\n2. PRODUCTION")
	
		max_option = 2
		if include_prod_readonly: max_option = 3
	
		while not env_select:
			try:
				env_select = int(input("Select the desired environment? "))
				if env_select < 1 or env_select > max_option:
					env_select = None
					raise ValueError
			except ValueError:
				print("Invalid selection")
	
	verbose('Environment: env_select set to %s' % str(env_select), level='DEBUG')
	
	if env_select == 1:
		FLAG_ENVIRONMENT = 'STAGE'
	elif env_select == 2:
		FLAG_ENVIRONMENT = 'PRODUCTION'
		if include_prod_confirm:
			print("===== CONFIRM CHANGES TO PRODUCTION =====")
			x = input("Confirm (y/n)? ").strip()
			if x != 'y':
				exit()
	elif env_select == 3:
		FLAG_ENVIRONMENT = 'PRODUCTION READ-ONLY'
	
	verbose('Environment: FLAG_ENVIRONMENT set to %s' % str(FLAG_ENVIRONMENT), level='DEBUG')

#pragma mark -

#pragma mark -

#pragma mark DatabaseController

def register_database_args(parser, include_superuser = False):
	group = parser.add_argument_group('Database')
	group.add_argument('--db-user', help='Database user')
	group.add_argument('--db-password', help='Database password')
	group.add_argument('--db-email', help='Database user email')
	group.add_argument('--db-capture-credentials', help='Force capture of database credentials for encrypted local storage', action='store_true')
	if include_superuser:
		parser.add_argument('--db-superuser', help='Use superuser credentials', action='store_true')

def handle_database_args(args):
	DatabaseController.override_user = args.db_user
	DatabaseController.override_password = args.db_password
	DatabaseController.override_email = args.db_email
	DatabaseController.override_capture_credentials = args.db_capture_credentials
	try:
		DatabaseController.superuser = args.db_superuser
	except AttributeError:
		pass

class DatabaseController:
	version = '0.3.0'
	override_user = ''
	override_password = ''
	override_email = ''
	override_capture_credentials = False
	superuser = False
	
	def __init__(self):
		try:
			import mysql.connector
		except ModuleNotFoundError as e:
			verbose(e, level='ERROR')
			verbose('Run `brew ls --versions mysql && brew upgrade mysql || brew install mysql`', level='ERROR')
			verbose('Run `brew cask install mysql-connector-python`', level='ERROR')
			verbose('Run `sudo pip3 install mysql-connector-python`', level='ERROR')
			exit()
		
		self.mysql_connector = mysql.connector
		
		self.db = None
		self.dry_run = False
		self.override_user = DatabaseController.override_user
		self.override_password = DatabaseController.override_password
		self.override_email = DatabaseController.override_email
		self.override_credentials_capture = DatabaseController.override_capture_credentials
		if not self.override_credentials_capture:
			if self.override_user or self.override_password:
				if not self.override_user: self.override_user = 'plusgrade'
				self.credentials = self.minimal_credentials()
				return
		self.permissions = {}

		self.load_gpg_credentials()
	
	def load_gpg_credentials(self):
		try:
			import gnupg
		except ModuleNotFoundError as e:
			verbose('DB credentials can be speficied as command line arguments (--db-user & --db-password), or can be stored locally using GnuPG encryption', level='ERROR')
			verbose(e, level='ERROR')
			verbose('For the latter, follow these instructions:', level='ERROR')
			verbose('Run `brew install gnupg`', level='ERROR')
			verbose('Run `sudo pip3 install python-gnupg`', level='ERROR')
			verbose('Adjust the next command with your own shell profile location (could be .bash_profile, .zshrc, .bashrc, etc)', level='ERROR')
			verbose('Run `echo -e "\nGPG_TTY=\$(tty);\nexport GPG_TTY;\ngpgconf --launch gpg-agent;\necho UPDATESTARTUPTTY | gpg-connect-agent &> /dev/null;\n" >> ~/.profile`')
			verbose('Run `gpg --full-generate-key`', level='ERROR')
			exit()
		
		self.gpgpath = os.path.expanduser("~/.gnupg/")
		verbose('DatabaseController -- GPG path is set to %s' % self.gpgpath, level='DEBUG')
		if DatabaseController.superuser:
			self.credentials_path = os.path.expanduser("~/.gnupg/credentials-superuser.json.gpg")
		else:
			self.credentials_path = os.path.expanduser("~/.gnupg/credentials.json.gpg")
		verbose('DatabaseController -- GPG credentials path is set to %s' % self.credentials_path, level='DEBUG')
		self.update_start_tty()
		self.gpg = gnupg.GPG(gnupghome=self.gpgpath)
		self.gpg.encoding = 'utf-8'
		self.retrieve_credentials()
	
	def retrieve_email(self):
		if self.override_email:
			return self.override_email
		email = ''
		try:
			gpg_identity = self.gpg.list_keys()[0]['uids'][0]
			email = gpg_identity.split('@')[0].split('<')[-1]
		except (IndexError, KeyError) as e:
			pass
		return email
	
	def update_start_tty(self):
		from subprocess import run, DEVNULL
		run(['gpg-connect-agent', 'UPDATESTARTUPTTY', '/bye'], stdout=DEVNULL, stderr=DEVNULL)
	
	def minimal_credentials(self):
		return {
			'STAGE': {
				'host': 'db.stage.plusgrade.com',
				'port': '3306',
				'user': self.override_user,
				'passwd': self.override_password
			},
			'PRODUCTION READ-ONLY': {
				'host': 'db-rr.plusgrade.com',
				'port': '3306',
				'user': self.override_user,
				'passwd': self.override_password
			},
			'PRODUCTION': {
				'host': 'db.plusgrade.com',
				'port': '3306',
				'user': self.override_user,
				'passwd': self.override_password
			}
		}
			
	def generate_db_credentials(self):
		from getpass import getpass
		verbose('DB credentials can be speficied as command line arguments (--db-user & --db-password), or can be stored locally using GnuPG encryption', level='WARNING')
		user = input('Enter DB username (e.g. john.D) ? ')
		stage_passwd = getpass(prompt='Enter stage password? ')
		prod_passwd = getpass(prompt='Enter prod password? ')
		
		credentials = self.minimal_credentials()
		credentials['STAGE']['user'] = user
		credentials['PRODUCTION READ-ONLY']['user'] = user
		credentials['PRODUCTION']['user'] = user
		credentials['STAGE']['passwd'] = stage_passwd
		credentials['PRODUCTION READ-ONLY']['passwd'] = prod_passwd
		credentials['PRODUCTION']['passwd'] = prod_passwd
		
		verbose('DatabaseController -- Creating credentials file')
		recipient = self.gpg.list_keys()[0]['keyid']
		verbose('DatabaseController -- GPG Recipient: %s' % recipient, level='DEBUG')
		s = self.gpg.encrypt(json.dumps(credentials), [recipient])
		verbose('DatabaseController -- GPG Response: %s' % s, level='DEBUG')
		if s.ok:
			with open(self.credentials_path, 'w') as f:
				f.write(str(s))
		else:
			verbose('DatabaseController -- GPG Status: %s' % s.status, level='ERROR')
			verbose('DatabaseController -- GPG Error: %s' % s.stderr, level='ERROR')
			exit()
	
	def retrieve_credentials(self):
		if not os.path.exists(self.credentials_path):
			verbose('DatabaseController -- Credentials file not found.', level='DEBUG')
			self.generate_db_credentials()
		if self.override_credentials_capture:
			verbose('DatabaseController -- Forcing capture of credentials', level='DEBUG')
			self.override_credentials_capture = False
			self.generate_db_credentials()
		with open(self.credentials_path, 'rb') as f:
			s = self.gpg.decrypt_file(f)
		if s.ok:
			verbose('Credentials file loaded correctly.', level='DEBUG')
			self.credentials = json.loads(str(s))
		else:
			verbose('DatabaseController -- GPG Status: %s' % s.status)
			verbose('DatabaseController -- GPG Error: %s' % s.stderr)
			verbose('DatabaseController -- Could not retrieve DB credentials', level='WARNING')
			self.generate_db_credentials()
			return self.retrieve_credentials()
			
			
	
	def enable_dry_run(self):
		self.dry_run = True
	def disable_dry_run(self):
		self.dry_run = False

	def select_stage(self):
		if self.db: self.close()
		self.load_db("STAGE")
	def select_prod_readonly(self):
		if self.db: self.close()
		self.load_db("PRODUCTION READ-ONLY")
	def select_prod(self):
		if self.db: self.close()
		self.load_db("PRODUCTION")
	
	def load_db(self, name):
		if self.dry_run:
			self.db = self.credentials[name]
			self.permissions = {'*': {'*': 'ALL PRIVILEGES'}}
			return
		verbose('DatabaseController -- Opening MySQL connection', level='DEBUG')
		self.db = self.mysql_connector.connect(
			host=self.credentials[name]['host'],
			port=self.credentials[name]['port'],
			user=self.credentials[name]['user'],
			passwd=self.credentials[name]['passwd']
		)
		self.retrieve_all_permissions()
	
	def cursor(self):
		if not self.db:
			verbose("DatabaseController -- No database selected.", level='ERROR')
			return
		if self.dry_run:
			return None
		return self.db.cursor()
	
	def commit(self):
		if not self.db: return
		self.db.commit()
	
	def close(self):
		if not self.db: return
		self.db.close()
		self.db = None
	
	def retrieve_all_permissions(self):
		if not self.db:
			verbose("DatabaseController -- No database selected.", level='ERROR')
			return
		grant_query = 'SHOW GRANTS;'
		response = self.execute(grant_query)[0]['result']
		verbose('DatabaseController -- Executed permissions query', level='DEBUG')
		self.permissions = {}
		for row in response:
			p1, p2 = row[0].split(' ON ')
			p1 = p1.split('GRANT ')[-1]
			p2 = p2.split(' TO ')[0]
			row_permission = p1.split(', ')
			database, table = p2.split('.')
			database = database.replace('`', '').strip().lower()
			table = table.replace('`', '').strip().lower()
			
			if database not in self.permissions:
				self.permissions[database] = {}
			self.permissions[database][table] = row_permission
		verbose('DatabaseController -- Loaded permissions: %s' % self.permissions, level='DEBUG')
	
	def check_permission_list(self, permission_list):
		if not self.db:
			verbose("DatabaseController -- No database selected.", level='ERROR')
			return False
		verbose('DatabaseController -- Checking permission list: %s' % permission_list, level='DEBUG')
		for line in permission_list:
			try:
				database, table, action = line.split('.')
			except ValueError:
				verbose('DatabaseController -- Invalid permission list format (format is `database.table.action`)', level='ERROR')
				return False
			database = database.replace('`', '')
			table = table.replace('`', '')
			if not self.check_permission(database, table, action):
				verbose('DatabaseController -- Permission check failed for `%s`' % line, level='ERROR')
				return False
		verbose('DatabaseController -- All permission checks succeeded', level='DEBUG')
		return True

	def check_permission(self, database, table, action):
		if not self.db:
			verbose("DatabaseController -- No database selected.", level='ERROR')
			return False
		database = database.replace('`', '').strip().lower()
		table = table.replace('`', '').strip().lower()

		key1 = database
		if database not in self.permissions.keys():
			if '*' in self.permissions.keys():
				key1 = '*'
			else:
				return False
		key2 = table
		if table not in self.permissions[key1].keys():
			if '*' in self.permissions[key1].keys():
				key2 = '*'
			else:
				return False
		if action in self.permissions[key1][key2] or 'ALL PRIVILEGES' in self.permissions[key1][key2]:
			return True
		return False
	
	def execute(self, queries):
		if self.dry_run:
			verbose('DatabaseController -- Dry-Run MySQL query: %s' % queries, level='WARNING')
			return [{
				'statement': queries,
				'result': [],
				'rowcount': 0
			}]
		out = []
		cursor = self.cursor()
		try:
			verbose('DatabaseController -- Executing MySQL query: %s' % queries, level='DEBUG')
			for result in cursor.execute(queries, multi=True):
				if result.with_rows:
					verbose('DatabaseController -- MySQL result with rows: %s' % result, level='DEBUG')
					out.append({
						'statement': result.statement,
						'result': result.fetchall()
					})
				else:
					verbose('DatabaseController -- MySQL result: %s' % result, level='DEBUG')
					out.append({
						'statement': result.statement,
						'rowcount': result.rowcount
					})
		except (RuntimeError, StopIteration) as e:
			pass
		
		self.commit()
		verbose('DatabaseController -- Execute value out: %s' % str(out), level='DEBUG')
		return out

#pragma mark -

def retrieve_partners(env):
	# Partners restrictions
	partner_query = "SELECT id, code FROM offers.Partners;"
	partners = env['db'].execute(partner_query)[0]['result']
	verbose("Executed Partners query")
	
	out = {}
	out_code = {}
	for row in partners:
		out[row[1]] = row[0]
		out_code[row[0]] = row[1]
	env['partners'] = out
	env['partners_code'] = out_code

BASE_URL = 'https://partner-stage.plusgrade.com/partner/services/user/encryptPassword'
EXCLUDE_LIST = [
	'PT',
	'AMEX',
	'XX'
]

def password_encode(username, password):
	data = {
		'authKey': '4BseTY4CnUv9',
		'username': username,
		'password': password
		
	}
	retry = 10
	while retry > 0:
		verbose('Encoding password for username: %s' % username)
		r = requests.post(BASE_URL, auth=('pgteam', 'comeFlyWithMe-N0W'), data=data)
		try:
			resp = json.loads(r.text)
			return resp['encryptedPassword']
		except (json.JSONDecodeError, KeyError):
			verbose('Requests: %s' % r, level='WARNING')
			retry -= 1

def main():
	global FLAG_ENVIRONMENT, EXCLUDE_LIST
	
	parser = argparse.ArgumentParser()
	# Required or prompted options
	parser.add_argument('--partner-id', '-p', help='The partner id or code', metavar="ID")
	parser.add_argument('--email', help='The user\'s plusgrade email')
	parser.add_argument('--firstname', help='The user\'s first name')
	parser.add_argument('--lastname', help='The user\'s last name')
	parser.add_argument('--role-id', help='The user\'s role (default: 4)')
	parser.add_argument('--SQL', help='Outputs SQL queries instead of running them', action='store_true')

	register_environment_args(parser)
	register_verbose_args(parser)
	register_version_args(parser)
	register_database_args(parser)
	
	args = parser.parse_args()
	
	handle_database_args(args)
	handle_version_args(args)
	handle_verbose_args(args)
	handle_environment_args(args)
	
	firstname = None
	lastname = None
	email = None
	role_id = None
	
	if args.firstname:
		firstname = args.firstname
	if args.lastname:
		lastname = args.lastname
	if args.email:
		email = args.email
	if args.role_id:
		role_id = args.role_id
	
	if not role_id:
		role_id = 4
	
	db = DatabaseController()
	db.select_stage()
	
	env = {'partner_id': None}
	env['db'] = db
	retrieve_partners(env)
	
	if FLAG_ENVIRONMENT == 'STAGE':
		db.select_stage()
	elif FLAG_ENVIRONMENT == 'PRODUCTION':
		db.select_prod()
	
	if not db.check_permission_list([
		'offers.Partners.SELECT',
		'users.Users.SELECT',
		'users.Users.INSERT',
	]):
		return

	
	while not email:
		email = input('Enter plusgrade email? ')
	
	try:
		firstname, lastname = email.split('@')[0].split('.')
	except ValueError:
		pass
	
	if firstname:
		firstname = '%s%s' % (firstname[0].upper(), firstname[1:])
	if lastname:
		lastname = '%s%s' % (lastname[0].upper(), lastname[1:])
	
	while not firstname:
		firstname = input('Enter first name? ')
		
	while not lastname:
		lastname = input('Enter last name? ')
	
	verbose('Firstname: %s' % firstname)
	verbose('Lastname: %s' % lastname)
	
	should_apply = False
	list_partner_id = []
	# Get partner IDs to process
	while True:
		while not env['partner_id']:
			try:
				env['partner_id'] = int(args.partner_id)
			except (TypeError, ValueError):
				pass
			if isinstance(args.partner_id, str) and args.partner_id.upper() in env['partners'].keys():
				env['partner_id'] = env['partners'][args.partner_id.upper()]
			found_partner_id = False
			for partner in env['partners']:
				if env['partners'][partner] == env['partner_id']:
					found_partner_id = True
			if not found_partner_id:
				env['partner_id'] = None
			if not env['partner_id']:
				args.partner_id = input("Enter the partner id or code (leave empty to continue, * for all)? ")
				if args.partner_id == '*':
					list_partner_id = []
					for x in env['partners_code']:
						list_partner_id.append(x)
					should_apply = True
					break
				if len(args.partner_id) < 1:
					should_apply = True
					break
		
		if should_apply:
			break
		
		if env['partner_id'] not in env['partners_code'].keys():
			verbose('Partner code not found for id %s' % env['partner_id'], level='WARNING')
		else:
			list_partner_id.append(env['partner_id'])
		args.partner_id, env['partner_id'] = None, None
	
	first = email.split('@')[0]
	
	user_query = "SELECT * FROM users.Users WHERE email LIKE '%s%%' AND email LIKE '%%@plusgrade.com';"
	users = env['db'].execute(user_query % first)[0]['result']
	verbose("Executed Users query")
	
	new_partner_list = list(list_partner_id)
	for partner_id in list_partner_id:
		code = env['partners_code'][partner_id]
		user = email.replace('@', '+%s@' % code)
		for x in users:
			if x[3].lower() == user.lower():
				verbose('User `%s` exists already' % user, level='WARNING')
				should_skip = True
				if partner_id in new_partner_list:
					new_partner_list.remove(partner_id)
	
	out = []
	all_random = False
	for partner_id in new_partner_list:
		code = env['partners_code'][partner_id]
		user = email.replace('@', '+%s@' % code)
		
		if code in EXCLUDE_LIST:
			verbose('Partner `%s` is part of EXCLUDE_LIST' % code)
			continue
		
		passwd = ''
		if not all_random:
			passwd = getpass(prompt='Enter desired password for %s (leave empty for random password, * for all)? ' % code)
			if passwd == '*':
				all_random = True
				passwd = ''
				print('Encoding all passwords...')
		disp_passwd = ''
		if len(passwd) < 1:
			x = pwgen.pwgen(16,symbols=False,no_capitalize=True, no_ambiguous=True)
			passwd = "-".join(["".join(x[i:i+4]) for i in range(0,len(x), 4)])
			disp_passwd = passwd
		encoded_pass = password_encode(user, passwd)
		out.append([user,encoded_pass,disp_passwd,partner_id])
	
	if len(out) > 0:
		print('New Users:\n', end='')
		print('\n'.join(['Partner App %s %s\t%s\t%s' % (env['partners_code'][x[3]], FLAG_ENVIRONMENT, x[0], x[2]) for x in out]))
		if not args.SQL:
			x = input('Apply (y/n)? ')
		else:
			x = 'y'
	else:
		verbose('No changes to apply.', level='WARNING')
		x = 'n'
	
	if x == 'y':
		out_query = []
		for row in out:
			out_query.append('INSERT INTO `users`.`Users` (firstname, lastname, email, lang, airport, currency, password, role_id, partner_id, employee_id, security_img_id, account_status) VALUES (%s, %s, %s, NULL, NULL, NULL, %s, %s, %s, NULL, 12, "PENDING");' % (repr(firstname), repr(lastname), repr(row[0]), repr(row[1]), repr(str(role_id)), repr(str(row[3]))))
		s = 'START TRANSACTION;\n'
		s += '\n'.join(out_query)
		s += '\nCOMMIT;'
		
		if not args.SQL:
			verbose('Executing generated INSERT query')
			db.execute(s)
			print('User(s) created.')
		else:
			print('='*76)
			print(s)
	db.close()


if __name__ == '__main__':
	main()
		
		