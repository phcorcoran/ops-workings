#!/usr/bin/env python3

import requests
import csv

x = input('Enter file to use (CSV, one column of domains as "@example.com")? ')
r = csv.reader(open(x))

env_select = None
print("1. STAGE\n2. PRODUCTION")
while not env_select:
	try:
		env_select = int(input("Select the desired environment? "))
		if env_select != 1 and env_select != 2:
			env_select = None
			raise ValueError
	except ValueError:
		print("Invalid selection")

if env_select == 2:
	print("="*23 + " CONFIRM CHANGES TO PRODUCTION " + "="*24)
	x = input("Confirm (y/n)? ").strip()
	if x != 'y':
		exit()

for row in r:
	if env_select == 1:
		query = 'https://ws-inventory.stg.internal.plusgrade.com/inventory-app/service/passengerBlacklist'
	elif env_select == 2:
		query = 'https://ws-inventory.prd.internal.plusgrade.com/inventory-app/service/passengerBlacklist'
	print('Adding blacklisted domain %s' % row[0])
	requests.post(query, headers={'Partner-Id': '26'}, data={'email': row[0]})