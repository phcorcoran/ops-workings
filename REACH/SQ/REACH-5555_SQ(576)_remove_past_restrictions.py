#!/usr/bin/env python3

import requests
import csv

# SELECT id FROM offers.FlightRestriction WHERE partner_id = 576 AND global_ind = 'F' AND source_products = '*' AND eligible_days IS NULL AND form_of_payment IS NULL AND point_of_sale IS NULL AND user_id = 1 AND blackout_end < DATE(NOW() - INTERVAL 1 DAY)

print("="*18 + " Retrieving restrictions for past flights " + "="*18)
print("""SELECT id FROM `offers`.`FlightRestriction` WHERE partner_id = 576 AND 
global_ind = 'F' AND source_products = '*' AND eligible_days IS NULL AND 
form_of_payment IS NULL AND point_of_sale IS NULL AND user_id = 1 AND 
blackout_end < DATE(NOW() - INTERVAL 1 DAY);""")
print("=" * 78)

x = input('Enter file to use (CSV, one column of IDs)? ')
r = csv.reader(open(x))

env_select = None
print("1. STAGE\n2. PRODUCTION")
while not env_select:
	try:
		env_select = int(input("Select the desired environment? "))
		if env_select != 1 and env_select != 2:
			env_select = None
			raise ValueError
	except ValueError:
		print("Invalid selection")

if env_select == 2:
	print("="*23 + " CONFIRM CHANGES TO PRODUCTION " + "="*24)
	x = input("Confirm (y/n)? ").strip()
	if x != 'y':
		exit()

for row in r:
	if env_select == 1:
		query = 'https://ws-inventory.stg.internal.plusgrade.com/inventory-app/service/restrictions/%s'
	elif env_select == 2:
		query = 'https://services.plusgrade.com/inventory-app/service/restrictions/%s'
	print('Removing restriction ID %s' % row[0])
	requests.delete(query % row[0], headers={'Partner-Id': '576'})