#!/usr/bin/env python3

import os
import json
import subprocess

path = '/Users/phcor/plusgrade/ui-partner-configuration/stage/'

path = input('Enter the path containing the partnerId-XX.json files? ')
out_file = input('Enter the output file? ')


no_pnrCheck = []
false_pnrCheck = []

for root, dirs, files in os.walk(path):
	for f in files:
		if '.json' not in f:
			continue
		config = json.load(open(os.path.join(path, f)))
		try:
			chaser_enabled = config['notifications']['chaserEmail']['enabled']
		except KeyError:
			continue
		
		try:
			chaser_pnrCheck = config['notifications']['chaserEmail']['pnrCheck']
		except KeyError:
			if chaser_enabled:
				no_pnrCheck.append(f)
		else:
			if chaser_enabled and not chaser_pnrCheck:
				false_pnrCheck.append(f)

out = []

print('SWAP VALUE')
for f in false_pnrCheck:
	s1 = '    "chaserEmail": {\n'
	s2 = '      "pnrCheck": false'
	x = open(os.path.join(path, f))
	i = 1
	found = None
	for line in x:
		if line == s1:
			found = i
		if line[:23] == s2 and found != None:
			print(i, '(%d lines later)' % (i - found), f)
			out.append("sed -i '' '%ds/false/true/' %s" % (i, f))
			break
		i += 1

print('ADD VALUE')
for f in no_pnrCheck:
	s1 = '    "chaserEmail": {\n'
	s2 = '      "enabled": true'
	x = open(os.path.join(path, f))
	i = 1
	found = None
	for line in x:
		if line == s1:
			found = i
		if line[:21] == s2 and found != None:
			print(i, '(%d lines later)' % (i - found), f)
			out.append("sed -i '' '%d a\\\n\ \ \ \ \ \ \"pnrCheck\": true,\\\n' %s" % (i, f))
			break
		i += 1

f = open(out_file, 'w')
f.write('\n'.join(out))
f.close()
