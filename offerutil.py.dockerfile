##
## USAGE
##
## Command to build the image
## docker build -f offerutil.py.dockerfile -t offerutil:offerutil .
##
## Next command to log into the image using bash
## docker run --rm -it -v ~/.gnupg:/root/.gnupg offerutil:offerutil
##
## Next command to execute the upgrade from within the docker
##
##  $ offerutil.py -h
##  optional arguments:
##    -h, --help            show this help message and exit
##    --partner-id ID, -p ID
##                          The partner id or code
##    --version             Output script version
##  
##  Actions:
##    --list, -l            List the eligible routes with flight numbers and fare
##                          classes
##    --generate-token, --token, -t
##                          Generate token
##    --troubleshoot        Ignore all options, troubleshoot instant upgrade
##                          tables
##  
##  Filters:
##    --fare-class FILTER   Filter for FROM fare classes (Ex. Y,PY or *)
##    --cabin-type TYPE     Filter for FROM cabin type
##    --equipment EQUIP, -e EQUIP
##                          Filter for equipment type (e.g. 338,388 or ~738 or *)
##    --origin FILTER, -o FILTER
##                          Filter for origin (e.g. YUL,YYZ or *)
##    --destination FILTER, -d FILTER
##                          Filter for destination (e.g. LGA,YVR or *)
##  
##  Token generator options:
##    --experimental        Bypass token create endpoint (EXPERIMENTAL)
##    --hours-before HOURS  Select hours before departure
##    --extra-seat          Further check for extra seat eligibility
##    --instant, -i         Further check for instant upgrade eligibility
##    --show-raw-token      Show raw generated token
##    --suppress-raw-payload
##                          Suppress raw generated payload
##    --segment-count COUNT
##                          Specify the number of segments
##    --pax COUNT           Specify the number of pax
##    --email EMAIL         Specify the email
##    --departure-date DEPARTURE_DATE
##                          Specify the departure date
##  
##  Cruise options:
##    --duration DAYS       Select duration in days for cruise
##    --from-cabin CABIN    Select from cabin for cruise
##    --after DATE          Restrict to cruises after date (e.g. 2030-01-01)
##    --show-all            Show all cruises instead of one per duration
##    --suppress-fare-class-details
##                          Suppress information about fare classes for ships
##  
##  Email options:
##    --show-email-links, -a
##                          Show all email links
##  
##  Override generator options:
##    --suppress-key KEY, -s KEY
##                          Strip out key from payload, regardless of other
##                          options
##    --force-key KEY VALUE, -f KEY VALUE
##                          Set key value pair in payload, regardless of other
##                          options
##    --edit-payload        Gives a chance to edit the payload before posting
##  
##  Environment:
##    --stage               Select STAGE database
##    --prod                Select PRODUCTION database
##  
##  Verbose:
##    --verbose, -v         Output more information (-vv debug, -vvv trace)
##    --quiet, -q           Suppress warnings (-qq errors, -qqq fatal)
##  
##  Database:
##    --db-user DB_USER     Database user
##    --db-password DB_PASSWORD
##                          Database password
##    --db-email DB_EMAIL   Database user email
##    --db-capture-credentials
##                          Force capture of database credentials for encrypted
##                          local storage
##

FROM alpine:3.7
ENV ENV="/etc/profile"
ENV PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:."

# Install packages
RUN apk add --no-cache mysql mysql-client python3 py3-pip gnupg
RUN pip3 install -q --upgrade pip
RUN pip3 install -qq mysql-connector-python requests python-gnupg

WORKDIR /root
COPY offerutil.py /usr/bin/offerutil.py
RUN chmod +x /usr/bin/offerutil.py
RUN ln -s /usr/bin/offerutil.py offerutil.py
RUN chmod +x offerutil.py
RUN echo "GPG_TTY=\$(tty); export GPG_TTY; gpgconf --launch gpg-agent; echo UPDATESTARTUPTTY | gpg-connect-agent &> /dev/null; echo -e 'offerutil.py\n'; offerutil.py -h" > /etc/profile

ENTRYPOINT ["/bin/sh"]

